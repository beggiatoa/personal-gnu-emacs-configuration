;;; mp-ui-modeline.el --- Modeline user interface (UI) settings  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marcop@p7560-f37>
;; Keywords: ui, modeline

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:
;;;; Use mood-line <https://gitlab.com/jessieh/mood-line>
(use-package mood-line
  :ensure t
  :config
  (mood-line-mode)
  (display-time)
  (setopt display-time-24hr-format t
          display-time-default-load-average nil))

;;;; Display minor modes in menu
(use-package minions
  :ensure t
  :hook (after-init-hook . minions-mode))

(provide 'mp-ui-modeline)
;;; mp-ui-modeline.el ends here
