;;; mp-common.el --- My personal common utilities for Emacs  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marcop@p7560-f37>
;; Keywords: convenience, extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Common utilities for my Emacs configuration and usage.

;;; Code:
;;;; Variables and constants

(defgroup mp-common ()
  "My personal common utilities for Emacs."
  :group 'convenience)

(defcustom mp-common-insert-pair-alist
  '(("' Single quote"        . (39 39))     ; ' '
    ("\" Double quotes"      . (34 34))     ; " "
    ("` Elisp quote"         . (96 39))     ; ` '
    ("‘ Single apostrophe"   . (8216 8217)) ; ‘ ’
    ("“ Double apostrophes"  . (8220 8221)) ; “ ”
    ("( Parentheses"         . (40 41))     ; ( )
    ("{ Curly brackets"      . (123 125))   ; { }
    ("[ Square brackets"     . (91 93))     ; [ ]
    ("< Angled brackets"     . (60 62))     ; < >
    ("« Εισαγωγικά Gr quote" . (171 187))   ; « »
    ("= Equals signs"        . (61 61))     ; = =
    ("~ Tilde"               . (126 126))   ; ~ ~
    ("* Asterisks"           . (42 42))     ; * *
    ("/ Forward Slash"       . (47 47))     ; / /
    ("_ underscores"         . (95 95)))    ; _ _
  "Alist of pairs for use with `prot-simple-insert-pair-completion'."
  :type 'alist
  :group 'mp-common)

(defcustom mp-common-date-specifier "%F %a"
  "Date specifier for `format-time-string'.
Used by `mp-common-inset-date'."
  :type 'string
  :group 'mp-common)

(defcustom mp-common-time-specifier "%R"
  "Date specifier for `format-time-string'.
Used by `mp-common-inset-date' with a `\\[universal-argument]'."
  :type 'string
  :group 'mp-common)

(defcustom mp-common-date-time-specifier-alt "%Y%m%dT%H%M%S"
  "Alternate specifier for `format-time-string'.
Used by `mp-common-inset-date' with a `\\[universal-argument] \\[universal-argument]'."
  :type 'string
  :group 'mp-common)

;;;; Functions and commands

(autoload 'symbol-at-point "thingatpt")

;;;;; Help
;;;###autoload
(defun mp-common-describe-symbol-dwim ()
  "Run `describe-symbol' for the `symbol-at-point'."
  (interactive)
  (describe-symbol (symbol-at-point)))

;;;;; Text motion and editing

;;;###autoload
(defun mp-common-untabify-buffer ()
  "Call `untabify' for the whole buffer."
  (interactive)
  (untabify (point-min) (point-max)))

;;;###autoload
(defun mp-common-kill-line-backward ()
  "Kill from point to the beginning of the line."
  (interactive)
  (kill-line 0))

;;;###autoload
(defun mp-common-kill-sentence-backward ()
  "Kill from point to the beginning of the sentence."
  (interactive)
  (kill-sentence -1))

;;;###autoload
(defun mp-common-kill-sexp-backward ()
  "Kill the sexp (balanced expression) before point."
  (interactive)
  (kill-sexp -1)
  (indent-according-to-mode))

;;;###autoload
(defun mp-common-zap-to-char-backward (char &optional arg)
  "Backward `zap-to-char' for CHAR.

Optional ARG is a numeric prefix to match ARGth occurance of
CHAR."
  (interactive
   (list
    (read-char-from-minibuffer "Zap to char: " nil 'read-char-history)
    (prefix-numeric-value current-prefix-arg)))
  (zap-to-char (- arg) char t))

;;;###autoload
(defun mp-common-zap-up-to-char-backward (char &optional arg)
  "Backward `zap-up-to-char' for CHAR.

Optional ARG is a numeric prefix to match ARGth occurance of
CHAR."
  (interactive
   (list
    (read-char-from-minibuffer "Zap to char: " nil 'read-char-history)
    (prefix-numeric-value current-prefix-arg)))
  (zap-up-to-char (- arg) char t))

;;;###autoload
(defun mp-common-new-line-below (&optional arg)
  "Create an empty line below the current one.
Move the point to the absolute beginning.  Adapt indentation by
passing optional prefix ARG (\\[universal-argument]).  Also see
`mp-common-new-line-above'."
  (interactive "P")
  (end-of-line)
  (if arg
      (newline-and-indent)
    (newline)))

;;;###autoload
(defun mp-common-new-line-above (&optional arg)
  "Create an empty line above the current one.
Move the point to the absolute beginning.  Adapt indentation by
passing optional prefix ARG (\\[universal-argument])."
  (interactive "P")
  (let ((indent (or arg nil)))
    (if (or (bobp)
            (line-number-at-pos (point-min)))
        (progn
          (beginning-of-line)
          (newline)
          (forward-line -1))
      (forward-line -1)
      (mp-common-new-line-below indent))))

(defun mp-common--character-prompt (chars)
  "Helper of `mp-common-insert-pair-completion' to read CHARS."
  (let ((def (car mp-common--character-hist)))
    (completing-read
     (format "Select character [%s]: " def)
     chars nil t nil 'mp-common--character-hist def)))

(defvar mp-common--character-hist '()
  "History of inputs for `mp-common-insert-pair-completion'.")

;;;###autoload
(defun mp-common-insert-date-time (&optional arg)
  "Insert the current date as `mp-common-date-specifier'.

When called interactively with a `\\[universal-argument]' prefix
argument ARG, also append the current time understood as
`mp-common-time-specifier'.

When called interactively with a `\\[universal-argument]
\\[universal-argument]' prefix argument ARG, insert the date/time
as `mp-common-date-time-specifier-alt'.

When region is active, delete the highlighted text and replace it
with the specified date."
  (interactive "P")
  (let* ((date mp-common-date-specifier)
         (time mp-common-time-specifier)
         (alt mp-common-date-time-specifier-alt)
         (format (cond
                  ((equal arg '(4)) (format "[%s %s]" date time))
                  ((equal arg '(16)) (format "%s" alt))
                  (t (format "[%s]" date)))))
    (when (use-region-p)
      (delete-region (region-beginning) (region-end)))
    (insert (format-time-string format))))

;;;###autoload
(defun mp-common-insert-pair (pair &optional count)
  "Insert PAIR around the symbol at point or active region.

The optional argument COUNT is an integer for the number of
delimiters to insert.

When called interactively, PAIR is selected with a prompt from
`mp-common-insert-pair-alist'.  If a `\\[universal-argument]'
prefix argument is used, prompt also for the number of delimiters
to insert COUNT."
  (interactive
   (list
    (mp-common--character-prompt mp-common-insert-pair-alist)
    current-prefix-arg))
  (let* ((data mp-common-insert-pair-alist)
         (left (cadr (assoc pair data)))
         (right (caddr (assoc pair data)))
         (n (cond
             ((and count (natnump count))
              count)
             (count
              (read-number "How many delimiters?" 2))
             (1)))
         (beg)
         (end))
    (cond
     ((region-active-p)
      (setq beg (region-beginning)
            end (region-end)))
     ((when (thing-at-point 'symbol)
        (let ((bounds (bounds-of-thing-at-point 'symbol)))
          (setq beg (car bounds)
                end (cdr bounds)))))
     (t (setq beg (point)
              end (point))))
    (save-excursion
      (goto-char end)
      (dotimes (_ n)
        (insert right))
      (goto-char beg)
      (dotimes (_ n)
        (insert left)))))

;;;###autoload
(defun mp-common-delete-pair-dwim ()
  "Delete pair following or preceding point.
For Emacs version 28 or higher, the feedback's delay is
controlled by `delete-pair-blink-delay'."
  (interactive)
  (if (eq (point) (cdr (bounds-of-thing-at-point 'sexp)))
      (delete-pair -1)
    (delete-pair 1)))

;;;###autoload
(defun mp-common-unfill-region-or-paragraph (&optional region)
  "Takes a multi-line paragraph and makes it into a single line of text."
  (interactive (progn (barf-if-buffer-read-only) '(t)))
  (let ((fill-column (point-max))
        ;; This would override `fill-column' if it's an integer.
        (emacs-lisp-docstring-fill-column t))
    (fill-paragraph nil region)))

;;;###autoload
(defun mp-common-transpose-words (arg)
  "Mofied version of `transpose-words'.

If region is active, swap the word at mark (region beginning)
with the one at point (region end).

Otherwise, and while inside a sentence, this behaves as the
built-in `transpose-words', dragging forward the word behind the
point.  The difference lies in its behaviour at the end or
beginning of a line, where it will always transpose the word at
point with the one behind or ahead of it (effectively the
last/first two words)."
  (interactive "p")
  (cond
   ((use-region-p)
    (transpose-words 0))
   ((eq (point) (point-at-eol))
    (transpose-words -1))
   ((eq (point) (point-at-bol))
    (forward-word 1)
    (transpose-words 1))
   (t
    (transpose-words arg))))

;;;###autoload
(defun mp-common-transpose-words-backward (&optional arg)
  "Backward `transpose-words'.

Optional ARG is a numeric prefix to drag the word backward past
ARG other words."
  (interactive "p")
  (let ((arg (or arg -1)))
    (mp-common-transpose-words (- arg))
    ))

;;;;; Windows and buffers
;;;;;; Better `split-window-*' commands

;;;###autoload
(defun mp-common-split-window-below-prompt-buffer (&optional size)
  "Call `window-split-below' and prompt for a buffer to place."
  (interactive `(,(when current-prefix-arg
                    (prefix-numeric-value current-prefix-arg))))
  (split-window-below size)
  (other-window 1)
  (if (fboundp 'consult-buffer)
      (consult-buffer)
    (switch-to-buffer)))

;;;###autoload
(defun mp-common-split-window-right-prompt-buffer (&optional size)
  "Call `window-split-right' and prompt for a buffer to place."
  (interactive `(,(when current-prefix-arg
                    (prefix-numeric-value current-prefix-arg))))
  (split-window-right size)
  (other-window 1)
  (if (fboundp 'consult-buffer)
      (consult-buffer)
    (switch-to-buffer)))

;;;###autoload
(defun mp-common-split-root-window-below-prompt-buffer (&optional size)
  "Call `window-root-split-below' and prompt for a buffer to place."
  (interactive `(,(when current-prefix-arg
                    (prefix-numeric-value current-prefix-arg))))
  (split-root-window-below size)
  (other-window 1)
  (if (fboundp 'consult-buffer)
      (consult-buffer)
    (switch-to-buffer)))

;;;###autoload
(defun mp-common-split-root-window-right-prompt-buffer (&optional size)
  "Call `window-root-split-right' and prompt for a buffer to place."
  (interactive `(,(when current-prefix-arg
                    (prefix-numeric-value current-prefix-arg))))
  (split-root-window-right size)
  (other-window 1)
  (if (fboundp 'consult-buffer)
      (consult-buffer)
    (switch-to-buffer)))

;;;###autoload
(define-minor-mode mp-window-buffer-focus-mode
  "Maximize the selected buffer in a single window.
The windows layout is restored when the minor mode is disabled."
  :ligther " Focus"
  :global nil
  (let ((win mp-window--windows-layout))
    (if (one-window-p)
        (when win
          (set-window-configuration win))
      (setq mp-window--windows-layout (current-window-configuration))
      (delete-other-windows))))

(defvar mp-window--windows-layout nil
  "Used by `mp-window-buffer-focus-mode' to store the windows layout.")

(defun mp-window--buffer-focus-off ()
  "Set variable `mp-window--windows-layout' to nil when appropriate.
To be hooked to `window-configuration-change-hook'."
  (when (and mp-window-buffer-focus-mode
             (not (one-window-p)))
    (delete-other-windows)
    (mp-window-buffer-focus-mode -1)
    (set-window-configuration mp-window--windows-layout)))

(add-hook 'window-configuration-change-hook #'mp-window--buffer-focus-off)

;;;###autoload
(defun mp-common-kill-buffer-current (&optional arg)
  "Kill current buffer or abort recursion when in minibuffer.
With optional prefix ARG (\\[universal-argument]) delete the
buffer's window as well."
  (interactive "P")
  (if (minibufferp)
      (abort-recursive-edit)
    (kill-buffer (current-buffer)))
  (when (and arg
             (not (one-window-p)))
    (delete-window)))

;;;###autoload
(defun mp-common-rename-file-and-buffer (name)
  "Apply NAME to current file and rename its buffer.
Do not try to make a new directory or anything fancy."
  (interactive
   (list (read-string "Rename current file: " (buffer-file-name))))
  (let ((file (buffer-file-name)))
    (if (vc-registered file)
        (vc-rename-file file name)
      (rename-file file name))
    (set-visited-file-name name t t)))


(provide 'mp-common)
;;;;; Others

;;;###autoload
(defun mp-keyboad-quit (&optional interactive)
  "A custom version of `keyboard-quit’ taken from doom-emacs."
  (interactive (list 'interactive))
  (let ((inhibit-quit t))
    (cond ((minibuffer-window-active-p (minibuffer-window))
           ;; quit the minibuffer if open.
           (when interactive
             (setq this-command 'abort-recursive-edit))
           (abort-recursive-edit))
          ;; don't abort macros
          ((or defining-kbd-macro executing-kbd-macro) nil)
          ;; Back to the default
          ((unwind-protect (keyboard-quit)
             (when interactive
               (setq this-command 'keyboard-quit)))))))

;;; mp-common.el ends here
