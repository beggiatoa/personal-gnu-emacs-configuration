;;; mp-app-org.el --- Stay organized with Emacs      -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marcop@p7560-f37>
;; Keywords: convenience, calendar

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:
;;;; Org-mode (personal information manager)
(use-package org
  :ensure t
;;;;; Hooks
  :hook
  ((org-mode-hook . mp-org-table--setup-table-highlighting)
   (orgtbl-mode-hook . mp-org-table--setup-table-highlighting)
   (org-agenda-finalize-hook . mp-org-agenda--delete-empty-blocks)
   (org-after-todo-statistics-hook . mp-org--summary-todo)
   (after-init-hook . org-clock-persistence-insinuate))
;;;;; Bindings
  :bind
  (("C-c A" . org-agenda)
   ("C-c a" . mp-org-agenda-weekly-agenda)
   ("C-c c" . org-capture)
   ("C-c l" . org-store-link)
   ("C-c o" . org-open-at-point-global)
   ("C-c C-x j" . org-clock-goto)
   ("C-c C-x i" . org-clock-in)
   ("C-c C-x o" . org-clock-out)
   ("C-c C-x C-o" . org-clock-out)
   ("C-c i" . mp-org-capture-inbox-todo)
   :map org-mode-map
   ("C-c M-l" . org-insert-last-stored-link)
   ("C-c v" . org-show-todo-tree)
   ("M-g o" . consult-org-heading)
   ("C-c C-v y" . org-fold-hide-block-all)
   ("C-c s" . mp-org-sort-entries-quick-todo)
   ([remap org-emphasize] . mp-org-emphasize)
   ("S-SPC" . org-table-blank-field)
   ("C-c T" . org-table-header-line-mode)
   ("C-c C-x i" . org-clock-in)
   ("C-," . nil)                        ; Disturbing and too importatn
   ("C-'" . nil)                        ; ↑
   :map orgtbl-mode-map
   ("M-;" . orgtbl-toggle-comment)
   :map prog-mode-map
   ("C-c M-v j" . org-babel-tangle-jump-to-org)
   ("C-c M-v C-j" . org-babel-tangle-jump-to-org))
  :config
  (setq org-directory (convert-standard-filename "~/Dropbox/Org"))
;;;;; Personal library `mp-org'
  (require 'mp-org)

;;;;; Org files location
  (setopt mp-org-inbox-file (expand-file-name "inbox.org" org-directory)
          mp-org-scheduled-file (expand-file-name "agenda.org" org-directory)
          org-default-notes-file mp-org-inbox-file)

  (add-to-list 'safe-local-variable-values
               '(org-archive-location . "archive.org::"))

;;;;; Org buffers UX
  (setopt org-adapt-indentation nil
          org-blank-before-new-entry
          '((heading . t) (plain-list-item . nil))
          org-imenu-depth 7
          org-use-speed-commands t
          org-special-ctrl-a/e t
          org-special-ctrl-k nil
          org-special-ctrl-o nil
          org-M-RET-may-split-line '((headline . nil)
                                     (item . t)
                                     (table . nil)
                                     (default . t))
          org-catch-invisible-edits 'show-and-error
          org-loop-over-headlines-in-active-region 'start-level
          org-insert-heading-respect-content t
          org-use-sub-superscripts '{}
          org-read-date-prefer-future 'time
          ;; Faster (?) LaTeX previews
          org-preview-latex-default-process 'imagemagick
          ;; Standardize lists
          org-list-demote-modify-bullet '(("+" . "+")
                                          ("-" . "+")
                                          ("*" . "-"))
          )

;;;;; Org buffers UI
  (setopt org-ellipsis "⤶"
          ;; Startup settings
          org-adapt-indentation nil
          org-startup-indented nil
          org-startup-folded 'fold
          org-startup-with-latex-preview nil
          org-startup-with-inline-images nil
          org-startup-shrink-all-tables nil
          org-startup-numerated nil
          org-hide-block-startup t
          ;; Timestapms
          org-timestamp-custom-formats
          '("%d/%m/%y %a" . "%d/%m/%y %a %H:%M")
          ;; Images
          org-image-actual-width 500
          ;; Markup
          org-fontify-done-headline nil
          org-fontify-whole-heading-line t
          org-num-skip-footnotes t
          org-num-skip-unnumbered t
          org-num-skip-tags '("ARCHIVE" "ignore")
          org-num-max-level 3
          org-pretty-entities nil
          org-hide-emphasis-markers t
          ;; Source and other blocks
          org-src-fontify-natively t
          org-fontify-quote-and-verse-blocks t
          org-fontify-whole-block-delimiter-line t)

  (set-face-attribute 'org-ellipsis nil :inherit 'default :box nil)

  ;; Allow multiple line Org emphasis markup.  Ref:
  ;; <http://emacs.stackexchange.com/a/13828/115>
  (setcar (nthcdr 4 org-emphasis-regexp-components) 20) ; Up to 20 lines
  (org-set-emph-re 'org-emphasis-regexp-components
                   org-emphasis-regexp-components)

;;;;; Org Links

  (add-to-list 'org-file-apps
               '("\\(?:xlsx?\\|ods\\)\\'" . "libreoffice %s"))

  ;; Define custom links for #+NAME’d items
  (defun org-link-complete-names ()
    "`completing-read' for org elements with #+NAME in the current buffer."
    (let ((candidates (org-element-map
                          (org-element-parse-buffer)
                          org-element-all-elements
                        (lambda (arg) (org-element-property :name arg)))))
      (completing-read "Name:" candidates)))

  (org-link-set-parameters
   "name"
   :complete #'org-link-complete-names)

  ;; Comment link inspired from
  ;; https://kitchingroup.cheme.cmu.edu/blog/2023/02/25/MS-Word-comments-from-org-mode/
  (org-link-set-parameters
   "comment"
   :export (lambda (path desc backend)
             (cond
              ((member backend '(md html))
               (format
                "<span class=\"comment-start\" author=\"%s\">%s</span>%s<span class=\"comment-end\"></span>"
                (user-full-name) path desc))
              ((member backend '(latex))
               (format
                "%s\\marginpar{{\\footnotesize %s\\n--%s}}"
                desc path (user-full-name)))
              ;; ignore for other backends and just use path
              (t desc)))
   :display 'org-link
   :face '(:foreground "orange"))

;;;;; Org tables
  (setopt org-table-export-default-format "orgtbl-to-csv"
          org-table-default-size "3x3"
          org-table-tab-jumps-over-hlines t)

;;;;; Footnotes
  (setopt org-footnote-auto-adjust t
          org-footnote-auto-label t)

;;;;; Org TODOs
  (setopt org-todo-keywords
          '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(x)")
            (sequence "STRT(s!)" "|" "DONE(x)")
            (sequence "WAIT(w@)" "HOLD(h@)" "|" "CANC(c@/@)")
            (sequence "DELG(D@)" "REVW(r@/@)" "|" "DONE(x)")
            (sequence "PROJ(p)" "|" "DNPR(P)"))
          org-enforce-todo-dependencies t
          org-enforce-todo-checkbox-dependencies t
          org-hierarchical-todo-statistics nil
          org-stuck-projects
          '("+project/-DONE" ("NEXT") nil "\\<IGNORE\\>"))

  (setopt org-todo-keyword-faces
          '(("NEXT" . mp-org-next)
            ("STRT" . mp-org-started)
            ("WAIT" . mp-org-wait)
            ("STOP" . mp-org-stop)
            ("CANC" . mp-org-cancelled)
            ("DELG" . mp-org-delegated)
            ("REVW" . mp-org-review)
            ("PROJ" . mp-org-project)
            ("DNPR" . mp-org-done-project)))

;;;;; Tags
  (setopt org-fast-tag-selection-single-key nil
          org-tags-sort-function 'string-collate-lessp
          org-auto-align-tags nil
          org-tags-column 0
          org-use-tag-inheritance t)

  (setq org-tag-alist
        '(("mail" . ?M)
          (:startgroup . "Location")
          ("@lab" . ?l) ("@home" . ?h) ("@office" . ?o)
          (:endgroup)
          (:startgroup . "Context")
          ("work" . ?w) ("personal" . ?p)
          (:endgroup)))

;;;;; Logging
  (setopt org-log-done 'time
          org-log-into-drawer t
          org-log-redeadline 'note
          org-log-reschedule 'time
          org-log-refile nil)

;;;;; Efforts and clocking
  (setopt org-effort-property "Effort"
          ;; work an average of 8 hours per day, 5 days a week, 4
          ;; weeks a month and 10 months a year.
          org-duration-units `(("min" . 1)
                               ("h" . 60)
                               ("d" . ,(* 60 8)) ; 1 day = 8 hours
                               ("w" . ,(* 60 8 5)) ; 1 week = 5 days
                               ("m" . ,(* 60 8 5 4)) ; 1 month = 4 weeks
                               ;; 1 y = 252 days in IE (247 in Italy)
                               ("y" . ,(* 60 8 252)))
          org-columns-default-format
          "%50ITEM(Task) %Effort(Effort){:} %CLOCKSUM")

  (add-to-list 'org-global-properties
               `("Effort_ALL" . ,(concat "10min 30min "
                                         "1h 1h30min 2h 2h30min 3h 4h 5h "
                                         "1d 2d 3d 4d 1w 2w 3w 1m 6w"))
               t)

  (setopt org-clock-in-switch-to-state "STRT"
          org-clock-idle-time 15        ; 30 mins
          org-clock-persist 'history
          org-clock-persist-file
          (expand-file-name "org-clock-save.el" mp-emacs--cache-dir)
          org-clock-history-length 7    ; Original is 5
          org-clock-goto-may-find-recent-task t
          org-clock-out-remove-zero-time-clocks nil
          org-clock-out-when-done t)

  (org-clock-auto-clockout-insinuate)
  (org-clock-persistence-insinuate)

;;;;; Habits
  (require 'org-habit)
  (setopt org-habit-show-habits-only-for-today t
          org-habit-preceding-days 4
          org-habit-following-days 1
          org-habit-graph-column 72
          org-habit-today-glyph 46 ;8224
          org-habit-completed-glyph 32 ;10003
          org-habit-show-done-always-green t)

;;;;; Babel (literate programming)
  (setopt org-confirm-babel-evaluate nil
          org-src-window-setup 'reorganize-frame
          org-edit-src-auto-save-idle-delay 15
          org-edit-src-persistent-message nil
          org-src-fontify-natively t
          org-src-preserve-indentation t
          org-src-tab-acts-natively t
          org-edit-src-content-indentation 0
          org-structure-template-alist
          '(("a" . "export ascii")
            ("c" . "center")
            ("C" . "comment")
            ("E" . "example")
            ("ee" . "export")
            ("eh" . "export html")
            ("el" . "export latex")
            ("q" . "quote")
            ("ss" . "src")
            ("sb" . "src bash")
            ("se" . "src emacs-lisp")
            ("sp" . "src python")
            ("sr" . "src R")
            ("v" . "verse")))

  (require 'ob-R)
  (require 'ob-shell)
  (require 'ob-python)
  (require 'ob-julia)
  (require 'ob-plantuml)
  (require 'ob-calc)
  (require 'ob-latex)

  (add-to-list 'org-src-lang-modes
               '("plantuml" . plantuml))
  (add-to-list 'org-src-lang-modes
               '("R" . R))
  (add-to-list 'org-src-lang-modes
               '("latex" . tex))
  (add-to-list 'org-src-lang-modes
               '("python" . python))
  (add-to-list 'org-src-lang-modes
               '("julia" . julia))

;;;;; Export

  (setopt org-export-with-toc t
          org-export-headline-levels 5  ; Increased from 3
          org-export-dispatch-use-expert-ui nil
          org-export-with-sub-superscripts '{})

  ;; Additional export backends
  (require 'ox-texinfo)
  (require 'ox-md)
  (require 'ox-koma-letter)

;;;;;; LaTeX Export

  (setopt org-latex-toc-command "\\tableofcontents \\clearpage"
          org-koma-letter-class-option-file "DIN"
          org-latex-listings 'minted
          org-latex-prefer-user-labels nil
          org-latex-bib-compiler "biber"
          org-latex-pdf-process
          '("%latex -interaction nonstopmode -shell-escape -output-directory %o %f"
            "%bib %b"
            "%latex -interaction nonstopmode -shell-escape -output-directory %o %f"
            "%latex -interaction nonstopmode -shell-escape -output-directory %o %f"))

  ;; Add koma srcartcl and srcrprt support
  (with-eval-after-load 'ox-latex
    (add-to-list 'org-latex-classes
                 '("scrartcl" "\\documentclass{scrartcl}"
                   ("\\section{%s}" . "\\section*{%s}")
                   ("\\subsection{%s}" . "\\subsection*{%s}")
                   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                   ("\\paragraph{%s}" . "\\paragraph*{%s}")
                   ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
    (add-to-list 'org-latex-classes
                 '("scrreprt" "\\documentclass{scrreprt}"
                   ("\\part{%s}" . "\\part*{%s}")
                   ("\\chapter{%s}" . "\\chapter*{%s}")
                   ("\\section{%s}" . "\\section*{%s}")
                   ("\\subsection{%s}" . "\\subsection*{%s}")
                   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")))
    (add-to-list 'org-latex-classes
                 '("scrbook" "\\documentclass{scrbook}"
                   ("\\part{%s}" . "\\part*{%s}")
                   ("\\chapter{%s}" . "\\chapter*{%s}")
                   ("\\section{%s}" . "\\section*{%s}")
                   ("\\subsection{%s}" . "\\subsection*{%s}")
                   ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

  ;; Additional LaTeX packages
  (add-to-list 'org-latex-packages-alist
               '("version=4" "mhchem" t))
  (add-to-list 'org-latex-packages-alist
               '("" "siunitx" t))
  (add-to-list 'org-latex-packages-alist
               '("" "minted" t))

;;;;; IDs
  (setopt org-id-method 'org
          ;; `org' is a new method, not yet documented.  It generates
          ;; the shortest link, seemingly based on the timestamp.
          org-id-locations-file
          (expand-file-name "org-id-locations" mp-emacs--cache-dir)
          org-id-link-to-org-use-id
          'create-if-interactive-and-no-custom-id)

;;;;; Org attachments
  (setopt org-attach-id-dir
          (expand-file-name "~/3-Resources/Org-mode-files/attachments/")
          org-attach-method 'cp
          org-attach-preferred-new-method 'id
          org-attach-auto-tag "ATTACH"
          org-attach-archive-delete 'query
          org-attach-use-inheritance t)

  (require 'org-attach-git)

;;;;; Org capture

  (require 'org-capture)

;;;;;; Inbox

  (add-to-list 'org-capture-templates
               '("i" "Inbox…")
               t)

  (add-to-list 'org-capture-templates
               `("it" "Inbox TODO" entry (file ,mp-org-inbox-file)
                 ,(concat "* TODO %^{Task} :draft:%^g\n"
                          ":PROPERTIES:\n"
                          ":CREATED: %U\n"
                          ":END:")
                 :prepend t
                 :empty-lines-after 1
                 :immediate-finish t
                 :kill-buffer t
                 :clock-keep t)
               t)

  ;; The above is the most common template, so I bind it to =C-c i=
  ;; for quick access
  (defun mp-org-capture-inbox-todo ()
    "Call `org-capture' with the \"Inbox TODO\" template."
    (interactive)
    (org-capture nil "it"))

  (add-to-list 'org-capture-templates
               `("ic" "Inbox clock" entry (file ,mp-org-inbox-file)
                 ,(concat "* STRT %^{Task} :draft:%^g\n"
                          ":PROPERTIES:\n"
                          ":CREATED: %U\n"
                          ":END:\n")
                 :prepend t
                 :empty-lines-after 1
                 :jump-to-captured t
                 :clock-in t)
               t)

  (add-to-list 'org-capture-templates
               `("in" "Inbox note" entry (file ,mp-org-inbox-file)
                 ,(concat "* %^{Title} :draft:%^g\n"
                          ":PROPERTIES:\n"
                          ":CREATED: %U\n"
                          ":END:\n"
                          "%?\n\n"
                          "#+begin_quote\n%i\n\n--\n[[file:%F][ref]]\n#+end_quote")
                 :prepend t
                 :empty-lines-after 1
                 :kill-buffer t
                 :clock-resume t)       ; Notes can be long
               t)

;;;;;; Scheduled events
  (add-to-list 'org-capture-templates
               '("s" "Scheduled…")
               t)

  (add-to-list 'org-capture-templates
               `("sm" "Meeting" entry
                 (file ,mp-org-scheduled-file)
                 ,(concat "* TODO %^{Who?|Florence|Ciaran|Raffaello|Kris} "
                          "about %^{3-words topic summary} :meeting:%^G\n"
                          "SCHEDULED: %^T%\n"
                          ":PROPERTIES:\n"
                          ":CREATED: %U\n"
                          ":LOCATION: %^{Where?|Florence's office|My office}\n"
                          ":END:\n"
                          "*Agenda:*\n"
                          "%?")
                 :prepend t
                 :empty-lines-after 1
                 :kill-buffer t
                 :clock-keep t)
               t)

  (add-to-list 'org-capture-templates
               `("sM" "Recurrent meeting" entry
                 (file ,mp-org-scheduled-file)
                 ,(concat "* TODO %^{Who?|Florence|Ciaran|Raffaello|Kris} "
                          "about %^{3-words topic summary} :meeting:%^G\n"
                          ;; FIXME: Kind of janky but works
                          "SCHEDULED: <%^{ISO date-time|"
                          (format-time-string "%F %a %R")
                          "} %^{Repeat|.+1w}>\n"
                          ":PROPERTIES:\n"
                          ":CREATED: %U\n"
                          ":LOCATION: %^{Where?|Florence's office|My office}\n"
                          ":END:\n"
                          "*Agenda:*\n"
                          "%?")
                 :prepend t
                 :empty-lines-after 1
                 :kill-buffer t
                 :clock-keep t)
               t)

  (add-to-list 'org-capture-templates
               `("ss" "Event" entry
                 (file ,mp-org-scheduled-file)
                 ,(concat "* %^{Title: } :event:%^G\n"
                          "SCHEDULED:%^T\n"
                          ":PROPERTIES:\n"
                          ":CREATED: %U\n"
                          ":LOCATION: %^{Where? }\n"
                          ":END:\n")
                 :prepend t
                 :empty-lines-after 1
                 :immediate-finish t
                 :kill-buffer t
                 :clock-keep t)
               t)

  (add-to-list 'org-capture-templates
               `("sa" "Anniversary" plain
                 (file+headline ,mp-org-scheduled-file "Anniversaries")
                 ,(concat "\%\%(org-anniversary "
                          "%^{YYYY MM DD date|"
                          (format-time-string "%Y %m %d")
                          "}"
                          ") %^{Anniversary title: } %d years")
                 :empty-lines-after 0
                 :immediate-finish t
                 :kill-buffer t
                 :clock-keep t)
               t)

;;;;; Org refile
  ;; BUG! `org-buffer-list' is not a recognized otion when using
  ;; `setopt'
  (setq org-refile-targets '((org-agenda-files :maxlevel . 5)
                             (nil :maxlevel . 5)
                             (org-buffer-list :maxlevel . 5)))

  (setopt org-refile-keep nil
          org-refile-use-cache t
          org-refile-use-outline-path 'file
          org-outline-path-complete-in-steps nil
          org-refile-allow-creating-parent-nodes t
          org-refile-active-region-within-subtree t
          org-reverse-note-order t)

;;;;; Org archive

  (setopt org-archive-location (expand-file-name
                                "archive.org::datetree/"
                                org-directory))

;;;;; Agenda
;;;;;; Agenda UI/UX

  (setopt org-agenda-files `(,org-directory)
          org-agenda-span 'week
          org-agenda-start-on-weekday 1 ; Monday
          org-agenda-confirm-kill t
          org-agenda-window-setup 'current-window
          org-agenda-restore-windows-after-quit t
          org-agenda-prefix-format '((agenda . " %e %?-12t% s")
                                     (todo . " %i %-12:c")
                                     (tags . " %i %-12:c")
                                     (search . " %i %-12:c"))
          org-agenda-sorting-strategy '((agenda habit-down
                                                time-up
                                                priority-down
                                                category-keep)
                                        (todo priority-down
                                              category-keep)
                                        (tags priority-down
                                              category-keep)
                                        (search category-keep))
          org-agenda-breadcrumbs-separator "->"
          org-agenda-todo-keyword-format "%-1s"
          org-agenda-category-icon-alist nil
          org-agenda-remove-times-when-in-prefix nil
          org-agenda-remove-timeranges-from-blocks nil
          org-agenda-compact-blocks nil
          org-agenda-block-separator ?—)

;;;;;; Agenda marks
  (setopt org-agenda-bulk-mark-char "#"
          org-agenda-persistent-marks nil)

;;;;;; Agenda follow mode
  (setopt org-agenda-start-with-follow-mode nil
          org-agenda-follow-indirect t)

;;;;;; Agenda items with deadline and scheduled timestamps
  (setopt org-agenda-include-deadlines t
          org-deadline-warning-days 5
          org-agenda-skip-scheduled-if-done t
          org-agenda-skip-scheduled-if-deadline-is-shown t
          org-agenda-skip-timestamp-if-deadline-is-shown t
          org-agenda-skip-deadline-if-done t
          org-agenda-skip-deadline-prewarning-if-scheduled 1
          org-agenda-skip-scheduled-delay-if-deadline nil
          org-agenda-skip-additional-timestamps-same-entry nil
          org-agenda-skip-timestamp-if-done nil
          org-agenda-search-headline-for-time nil
          org-scheduled-past-days 365
          org-deadline-past-days 365
          org-agenda-move-date-from-past-immediately-to-today t
          org-agenda-show-future-repeats t
          org-agenda-prefer-last-repeat nil
          org-agenda-timerange-leaders '("" "(%d/%d): ")
          org-agenda-scheduled-leaders '("" "%2d ago ")
          org-agenda-inactive-leader "["
          org-agenda-deadline-leaders '("!Today! " "! %2d days "
                                        "!!Past!! (%2d) ")
          ;; Time grid
          org-agenda-time-leading-zero t
          org-agenda-timegrid-use-ampm nil
          org-agenda-use-time-grid t
          org-agenda-show-current-time-in-grid t
          org-agenda-current-time-string (concat "Now "
                                                 (make-string 70 ?-))
          org-agenda-time-grid '((daily today require-timed)
                                 (0600 0700 0800 0900 1000 1100
                                       1200 1300 1400 1500 1600
                                       1700 1800 1900 2000 2100)
                                 " …" "-----------------")
          org-agenda-default-appointment-duration nil)

;;;;;; Agenda global to-do list
  (setopt org-agenda-todo-ignore-with-date nil
          org-agenda-todo-ignore-timestamp nil
          org-agenda-todo-ignore-scheduled nil
          org-agenda-todo-ignore-deadlines nil
          org-agenda-todo-ignore-time-comparison-use-seconds nil
          org-agenda-tags-todo-honor-ignore-options nil)

;;;;;; Agenda habits
  (require 'org-habit)
  (setopt org-habit-graph-column 50
          org-habit-preceding-days 9
          org-habit-following-days 1
          org-habit-show-habits-only-for-today t
          org-habit-today-glyph 46      ; 8224
          org-habit-completed-glyph 32  ; 10003
          org-habit-show-done-always-green t)

;;;;;; Custom agenda definitions
  (add-to-list 'org-agenda-custom-commands
               '("d" "Daily agenda and top priority tasks"
                 ((agenda nil
                          ((org-agenda-skip-function
                            '(org-agenda-skip-entry-if
                              'todo '("PROJ")))
                           (org-agenda-start-on-weekday nil) ; Start from today
                           (org-agenda-span 1)
                           (org-agenda-show-all-dates nil)
                           (org-agenda-use-time-grid nil)))
                  (alltodo nil
                           ((org-agenda-skip-function
                             '(org-agenda-skip-entry-if 'timestamp
                                                        'todo '("PROJ")))
                            (org-agenda-overriding-header "Unscheduled tasks")))
                  (todo "PROJ"
                        ((org-agenda-overriding-header "Projects")))
                  (tags "CLOSED>=\"<today>\""
                        ((org-agenda-overriding-header "Completed today\n"))))))

  (add-to-list 'org-agenda-custom-commands
               '("w" "Weekly agenda and long deadlines"
                 ((agenda nil
                          ((org-agenda-skip-function
                            '(org-agenda-skip-entry-if
                              'todo '("PROJ")))
                           (org-agenda-start-on-weekday 1) ; Start from Monday
                           (org-agenda-span 'week)
                           (org-agenda-show-all-dates t)
                           (org-agenda-use-time-grid nil)))
                  (todo "STRT"
                        ((org-agenda-max-entries 5)
                         (org-agenda-todo-ignore-scheduled 'past)
                         (org-agenda-overriding-header "Started tasks")))
                  (todo "NEXT"
                        ;; ((org-agenda-skip-function
                        ;;   '(org-agenda-skip-entry-if 'regexp "\[#C\]"))
                        ((org-agenda-max-entries 5)
                         (org-agenda-todo-ignore-scheduled 'past)
                         (org-agenda-overriding-header "Next tasks")))
                  (alltodo nil
                           ((org-agenda-skip-function
                             '(org-agenda-skip-entry-if 'timestamp
                                                        'todo '("PROJ" "STRT" "NEXT")))
                            (org-agenda-max-entries 5)
                            (org-agenda-overriding-header "Unscheduled tasks")))
                  (todo "PROJ"
                        ((org-agenda-max-entries 5)
                         (org-agenda-overriding-header "Projects")))
                  (tags "CLOSED>=\"<-1w>\""
                        ((org-agenda-overriding-header "Completed today\n")
                         (org-agenda-archives-mode t))))))

  (defun mp-org-agenda-daily-agenda ()
    "Call the daily agenda.  See `org-agenda-custom-commands'"
    (interactive)
    (org-agenda nil "d"))

  (defun mp-org-agenda-weekly-agenda ()
    "Call the daily agenda.  See `org-agenda-custom-commands'"
    (interactive)
    (org-agenda nil "w")))

;;;; Org publish
(setq org-publish-timestamp-directory
      (expand-file-name "org-publish-timestamps" mp-emacs--cache-dir))

;;;; Extra: `org-contrib'
(use-package org-contrib
  :ensure t
  :after org
  :config
;;;;; FIXME! Add `CREATED' org-property to headers
  (require 'org-expiry)

  ;; (with-eval-after-load 'org-expiry
  ;;   (add-hook 'org-mode-hook 'org-expiry-insinuate)
  (setq org-expiry-inactive-timestamps t)

;;;;; Ignore heading but export content
  (require 'ox-extra)
  (with-eval-after-load 'ox-extra
    (ox-extras-activate '(ignore-headlines)))

;;;;; git-specific org liniks
  ;; TODO! Remove `org-git-store-link' from
  ;; `org-store-link-functions'.  Instead bind it so it can be used
  ;; only when required.
  (require 'ol-git-link)

;;;;; `org-crypt'
  (require 'org-crypt)
  (with-eval-after-load 'org-crypt
    (setopt org-crypt-tag-matcher "crypt")))

;;;; Extra: `org-edna’ for advanced dependencies
(use-package org-edna
  :ensure t
  :after org
  :hook
  (org-mode-hook . org-edna-mode)
  :bind
  ("C-c C-x D" . org-edna-edit))

;;;; Extra: `org-inline-pdf’ and `org-inline-anim’
;; See PDF, GIF, and animated PNG inline as normal images
(use-package org-inline-pdf
  :ensure t
  :bind
  ("C-c C-x M-v" . org-inline-pdf-mode))

(use-package org-inline-anim
  :ensure t)

;;;; Extra: `plantuml-mode'
(use-package plantuml-mode
  :ensure t
  :config
  (setopt org-plantuml-jar-path
          (expand-file-name "~/.local/opt/plantuml.jar")
          plantuml-jar-path
          (expand-file-name "~/.local/opt/plantuml.jar")
          plantuml-default-exec-mode 'jar)

  (add-to-list 'auto-mode-alist
               '("\\.plantuml\\'" . plantuml-mode)))

;;;; Extra: `org-agenda-property'
(use-package org-agenda-property
  :ensure t
  :config
  (setopt org-agenda-property-list '("LOCATION")))

;;;; Extra: `org-download'
(use-package org-download
  :ensure t
  :custom
  (org-download-method 'attach)
  (setq org-download-timestamp "%Y%m%dT%H%M%S--")
  :bind
  (:map org-mode-map
        ("C-c d s" . org-download-screenshot)
        ("C-c d y" . org-download-clipboard))
  :hook
  ((dired-mode-hook . org-download-enable)
   (org-mode-hook . org-download-enable)))

;;;; Extra: `orgit' for org-links to magit buffers
(use-package orgit
  :ensure t
  :if (package-installed-p 'magit)
  :after magit)

(use-package orgit-forge
  :ensure t
  :if (and (package-installed-p 'forge) (package-installed-p 'magit))
  :after (forge magit))

;;;; Extra: `org-modern' for prettier org buffers
(use-package org-modern
  :ensure t
  :bind
  (:map org-mode-map
        ("C-c C-x C-\\" . org-modern-mode-toggle))
  :hook
  (org-agenda-finalize-hook . org-modern-agenda)
  :preface
  (defun org-modern-mode-toggle ()
    (interactive)
    (org-modern-mode 'toggle)))

;;;; Extra: `org-appear’ for auto-toggling hidden elements
(use-package org-appear
  :ensure t
  :hook
  (org-mode-hook . org-appear-mode)
  :custom
  (org-appear-autoemphasis t)
  (org-appear-autolinks t)
  (org-appear-autosubmarkers t)
  (org-appear-inside-latex t)
  (org-appear-delay 1))

;;;; Extra: `org-tufte' for Tufte HTLM export

(unless (package-installed-p 'org-tufte)
  (package-vc-install "https://github.com/Zilong-Li/org-tufte"))

(use-package org-tufte
  :ensure nil
  :after org
  :config
  (require 'org-tufte)
  (setopt org-tufte-htmlize-code t
          org-tufte-goto-top-button t))

;;;; Extra: `org-glossary’ for glossaries and acronyms in org (beta)
(unless (package-installed-p 'org-glossary)
  (package-vc-install "https://github.com/tecosaur/org-glossary"))

(use-package org-glossary
  :after org
  :custom
  ((org-glossary-autodetect-in-headings . nil)))

;;;; Extra: `ob-async’ for asyncronous org babel execution
(use-package ob-async
  :ensure t)

;;;; Calendar
(use-package calendar
  :config
  (copy-face font-lock-constant-face 'calendar-iso-week-face)
  (setopt calendar-mode-line-format nil
          calendar-week-start-day 1
          calendar-date-style 'iso
          calendar-date-display-form calendar-iso-date-display-form
          calendar-time-zone-style 'numeric
          calendar-time-display-form
          '(24-hours ":" minutes
                     (when time-zone
                       (format "(%s)" time-zone))))

  ;; ISO week number in calendar
  (setopt calendar-intermonth-text
          '(propertize
            (format "%2d|"
                    (car
                     (calendar-iso-from-absolute
                      (calendar-absolute-from-gregorian (list month day year)))))
            'font-lock-face 'calendar-iso-week-face))

  (setopt calendar-intermonth-header
          (propertize "Wk|" 'font-lock-face 'calendar-iso-week-header-face)))

(provide 'mp-app-org)
;;; mp-app-org.el ends here
