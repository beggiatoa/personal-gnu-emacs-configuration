(use-package markdown-mode
  :ensure t
  :mode ("README" . gfm-mode)
  :bind
  (:map markdown-mode-map
        ("C-c C-e" . markdown-do))
  :init
  (setq markdown-command '("pandoc" "--from=markdown" "--to=html5"))
  :config
  (setopt markdown-disable-tooltip-prompt t
          markdown-hide-markup nil
          markdown-fontify-code-blocks-natively t
          markdown-italic-underscore t
          markdown-gfm-uppercase-checkbox t)) ; For org-mode compatibility

(provide 'mp-prog-markdown)
