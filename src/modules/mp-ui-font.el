;;; mp-ui-font.el --- Typefaces (fonts) configuration  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marcop@p7560-f37>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;;;; General
(setq-default x-underline-at-descent-line nil
              text-scale-remap-header-line t)

(define-key ctl-x-x-map (kbd "v") #'variable-pitch-mode)

;;;; Configure fonts using the `fontaine' package
;; Read the manual: <https://protesilaos.com/emacs/fontaine>
(use-package fontaine
  :ensure t
  :config
  (setq fontaine-latest-state-file (expand-file-name
                                    "fontaine-latest-state-file.eld"
                                    mp-emacs--cache-dir))

  (setq fontaine-presets
        '((t
           :default-family "Iosevka"
           :default-weight regular
           :default-height 110
           :fixed-pitch-family "Iosevka Term"
           :fixed-pitch-weight nil
           :fixed-pitch-height 1.0
           :fixed-pitch-serif-family "Iosevka Etoile"
           :fixed-pitch-serif-weight light
           :fixed-pitch-serif-height 1.0
           :variable-pitch-family "Iosevka Aile"
           :variable-pitch-weight nil
           :variable-pitch-height 1.0
           :bold-family nil ; use whatever the underlying face has
           :bold-weight bold
           :italic-family "Victor Mono"
           :italic-slant italic
           :line-spacing nil)
          (regular)
          (small
           :default-family "Iosevka Fixed"
           :default-height 80
           :variable-pitch-family "Iosevka Comfy Wide Duo")
          (large
           :default-weight semilight
           :default-height 140
           :bold-weight extrabold)
          (presentation
           :default-family "Iosevka Curly"
           :default-weight semilight
           :default-height 200
           :bold-weight extrabold)
          (reading
           :default-family "Iosevka Aile"
           :default-weight semilight)))

  (fontaine-set-preset (or (fontaine-restore-latest-preset) 'regular))
  :hook
  ((kill-emacs-hook . fontaine-store-latest-preset)
   (ef-themes-post-load-hook . fontaine-apply-current-preset)
   (after-init-hook . fontaine-apply-current-preset)
   (server-after-make-frame-hook . fontaine-apply-current-preset))
  :bind
  (("C-c f" . fontaine-set-preset)
   ("C-c F" . fontaine-set-face-font)))

;;;; Additional fonts icons
(use-package all-the-icons
  :ensure t)

(provide 'mp-ui-font)
;;; mp-ui-font.el ends here
