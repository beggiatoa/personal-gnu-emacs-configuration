;;; mp-ux-prog.el --- Interact with programming code  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marcop@p7560-f37>
;; Keywords: programming, code

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Specific programming languages are configured individually

;;; Code:

;;;; Fill column for programming

(add-hook 'prog-mode-hook (lambda () (setq-local fill-column 72)))
(add-hook 'prog-mode-hook (lambda () (setq-local comment-fill-column 120)))

;;;; Manage delimiters
(use-package paren
  :custom
  (show-paren-context-when-offscreen 'child-frame))

;;;; Easier Regions Selections
(use-package expand-region
  :ensure t
  :bind
  (("C-M-SPC" . er/expand-region)))

(use-package change-inner
  :ensure t
  :after expand-region
  :bind
  (("M-[" . change-inner)
   ("M-]" . change-outer)))

;;;; Highlight certain comments in code
(defgroup mp-highlight nil
  "Personal customs for automatic word highlightings."
  :group 'hi-lock-faces)

  (defcustom mp-highlight-keywords-list nil
    "List of regexp to highlight with `mp-highlight-keywords'."
    :type '(repeat string))

  (defcustom mp-highligh-default-face "error"
    "Default face used by `mp-highlight-keywords'."
    :type 'face)

  (defcustom mp-highlight-only-comments t
    "Highlight only keywords preceded by `comment-start'."
    :type 'boolean)

  (defun mp-highlight-keywords (&optional face)
    "Highlight keywords in the list `mp-highlight-keywords-list'.

By default highlight with the `error' face.  If the optional
argument FACE is a valid face name use that instead.

When called interactivelly with a (\\[universal-argument]),
 prompt the use for a face."
    (interactive
     (list (when current-prefix-arg
             (read-face-name "Highlighting face: "))))
    (if (length= mp-highlight-keywords-list 0)
        (warn "`mp-highlight-keywords-list' is empty")
      (let ((regexp (concat
                     (when mp-highlight-only-comments
                       (concat comment-start "+ *"))
                     "\\(?:"
                     (string-join mp-highlight-keywords-list "\\|")
                     "\\)"))
            (face (if (facep face) face mp-highligh-default-face)))
        (highlight-regexp regexp face))))

  (setopt mp-highlight-keywords-list '("FIXME[!:]?"
                                       "TODO[!:]?"
                                       "BUG[!:]?"))

  (add-hook 'prog-mode-hook #'mp-highlight-keywords)

;;;; Navigate <https://devdocs.io> documentation from Emacs
(use-package devdocs
  :ensure t
  :bind
  ("C-h D" . devdocs-lookup)
  :config
  (setopt devdocs-data-dir
          (expand-file-name "devdocs" mp-emacs--cache-dir)))

;;;; Format code buffer
(use-package format-all
  :ensure t)

;;;; Provide file
(provide 'mp-ux-prog)

;;; mp-ux-prog.el ends here
