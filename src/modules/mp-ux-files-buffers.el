;;; mp-ux-files-buffers.el --- Interaction with buffers and files  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello;;; Go to last change <marcop@p7560-f37>
;; Keywords: buffers

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Adjusting the user experience of interacting with buffers and
;; files.

;;; Code:
;;;; Search and substitute in buffers

;;;;; `substitute' package

(use-package substitute
  :ensure t
  :config
  (let ((map global-map))
    (define-key map (kbd "M-£ s") #'substitute-target-below-point)
    (define-key map (kbd "M-£ r") #'substitute-target-above-point)
    (define-key map (kbd "M-£ d") #'substitute-target-in-defun)
    (define-key map (kbd "M-£ b") #'substitute-target-in-buffer)))

;;;; Editable greps (`wgrep')
(use-package wgrep
  :ensure t)

;;;; Search across files with ripgrep
(use-package rg
  :ensure t
  :bind
  ("C-c s" . rg-menu))

;;;; Set scratch mode to fundamental-mode
(setq initial-major-mode 'fundamental-mode)

;;;; Ctl-x-x key map for buffer commands
(define-key ctl-x-x-map (kbd "TAB") #'mp-common-untabify-buffer)

;;;; Uniquify buffer names
(use-package uniquify
  :custom
  (uniquify-buffer-name-style 'forward)
  (uniquify-strip-common-suffix t)
  (uniquify-after-kill-buffer-p t))

;;;; Use `ibuffer'
(use-package ibuffer
  :bind
  (("C-x C-b" . ibuffer)
   :map ibuffer-mode-map
   ("* f" . ibuffer-mark-by-file-name-regexp)
   ("* g" . ibuffer-mark-by-content-regexp) ; "g" is for "grep"
   ("* n" . ibuffer-mark-by-name-regexp)
   ("s n" . ibuffer-do-sort-by-alphabetic)  ; "sort name" mnemonic
   ("/ g" . ibuffer-filter-by-content)
   ("{"   . ibuffer-backwards-next-marked)
   ("}"   . ibuffer-forward-next-marked)
   ("["   . ibuffer-backward-filter-group)
   ("]"   . ibuffer-forward-filter-group)
   ("$"   . ibuffer-toggle-filter-group))
  :hook
  ((ibuffer-mode-hook . hl-line-mode)
   (ibuffer-mode . ibuffer-auto-mode)   ; auto-update
   (ibuffer-mode-hook . mp-ibuffer--enable-default-grouping)
   (ibuffer-mode-hook . mp-ibuffer--hide-unimportant))
  :config
  (require 'ibuf-ext)

  (defun mp-ibuffer--enable-default-grouping ()
    "See the variable `ibuffer-saved-filter-groups'."
    (ibuffer-switch-to-saved-filter-groups "default"))

  (defun mp-ibuffer--hide-unimportant ()
    "Hide buffer from the default/Unimportant filter group.

See the variable `ibuffer-saved-filter-groups'."
    (ibuffer-switch-to-saved-filter-groups "default")
    (setq-local ibuffer-hidden-filter-groups (list "Unimportant" "Special"))
    (ibuffer-update nil t))

  ;; Use human readable Size column instead of original one
  (define-ibuffer-column size-h
    (:name "Size" :inline t)
    (cond
     ((> (buffer-size) 1000000) (format "%7.1fM" (/ (buffer-size) 1000000.0)))
     ((> (buffer-size) 100000) (format "%7.0fk" (/ (buffer-size) 1000.0)))
     ((> (buffer-size) 1000) (format "%7.1fk" (/ (buffer-size) 1000.0)))
     (t (format "%8d" (buffer-size)))))

  (setopt ibuffer-expert t
          ibuffer-show-empty-filter-groups nil
          ibuffer-movement-cycle nil
          ibuffer-old-time 48
          ibuffer-default-sorting-mode 'filename/process
          ibuffer-formats '((mark modified read-only locked " "
                                  (name 40 40 :left :elide)
                                  " "
                                  (size-h 9 -1 :right)
                                  " "
                                  (mode 16 16 :left :elide)
                                  " " filename-and-process)
                            (mark " "
                                  (name 16 -1)
                                  " " filename))
          ibuffer-saved-filter-groups '(("default"
                                         ("Files" (filename . ".*"))
                                         ("Dired" (mode . dired-mode))
                                         ("Organized"
                                          (or
                                           (name . "^\\*Calendar\\*$")
                                           (name . "^diary$")
                                           (and (mode . org-mode) (not (name . "^\\*scratch\\*$")))
                                           (mode . org-agenda-mode)))
                                         ("Help"
                                          (or
                                           (name . "^\\*info*")
                                           (name . "^\\*Help*")))
                                         ("Special"
                                          (name . "^\\*.*\\*$"))
                                         ("Unimportant"
                                          (or
                                           (name . "^\\*scratch\\*$")
                                           (name . "^\\*Messages\\*$")
                                           (name . "^\\*Custom*")
                                           (name . "^\\*Backtrace\\*$")
                                           (name . "^\\*Async*")))))
          ))

;;;; Group buffers by version control in IBuffer
(use-package ibuffer-vc
  :ensure t
  :after (ibuffer vc)
  :bind
  (:map ibuffer-mode-map
        ("/ v" . ibuffer-vc-set-fileter-groups-by-vc-root)))

;;;; Recent files and directories
(use-package recentf
  :init
  (setq recentf-save-file (expand-file-name "recentf" mp-emacs--cache-dir))
  (setq recentf-max-saved-items 200)
  (setq recentf-exclude '(".gz" ".xz" ".zip" "/elpa/" "/ssh:" "/sudo:"))
  :config
  (defun mp-recentf-keep-predicate (file)
    "Additional conditions for saving FILE in `recentf-list'.
Add this function to `recentf-keep'."
    (cond
     ((file-directory-p file) (file-readable-p file))))

  (add-to-list 'recentf-keep 'mp-recentf-keep-predicate)
  :hook
  (after-init-hook . recentf-mode))

;;;; Move inside a buffer

;; Quickly jump with Avy
(use-package avy
  :ensure t
  :bind
  (("M-g j" . avy-goto-char-timer)
   ("M-g M-j" . avy-goto-char-timer)))

;; Jump to last change
(use-package goto-last-change
  :ensure t
  :bind
  ("C-z" . goto-last-change))

;;;; Buffer narrowing and outlining
(use-package outline
  :bind
  (("<f10>" . outline-minor-mode)
   ("C-x x o" . outline-minor-mode)
   :map outline-minor-mode-map
   ("C-x o n" . outline-next-visible-heading)
   ("C-x o p" . outline-previous-visible-heading)
   ("C-x o f" . outline-forward-same-level)
   ("C-x o b" . outline-backward-same-level)
   ("C-x o a" . outline-show-all)
   ("C-x o h" . outline-hide-sublevels)
   ("C-x o o" . outline-hide-other)
   ("C-x o u" . outline-up-heading)))
;; :config
;; (setopt outline-minor-mode-highlight 'override
;;         outline-minor-mode-cycle t
;;         outline-minor-mode-use-buttons 'in-margins))

(use-package outline-minor-faces
  :ensure t
  :after outline
  :config (add-hook 'outline-minor-mode-hook
                    #'outline-minor-faces-mode))

(use-package backline
  :ensure t
  :after outline
  :config (advice-add 'outline-flag-region :after 'backline-update))

(use-package bicycle
  :ensure t
  :after outline
  :bind
  (:map outline-minor-mode-map
        ("C-<tab>" . bicycle-cycle)
        ("<backtab>" . bicycle-cycle-global)))

;;;; Centered text with `olivetti' and `logos'
;;;;; Olivetti
(use-package olivetti
  :ensure t
  :config
  (setopt olivetti-body-width 90
          olivetti-minimum-body-width 40
          olivetti-recall-visual-line-mode-entry-state t
          olivetti-style nil))

;;;;; Logos
(use-package logos
  :ensure t
  :after olivetti
  :bind
  (("C-x n n" . logos-narrow-dwim)
   ("C-x ]"   . logos-forward-page-dwim)
   ("C-x ["   . logos-backward-page-dwim)
   ("M-]"     . logos-forward-page-dwim)
   ("M-["     . logos-backward-page-dwim)
   ("<f9>"    . logos-focus-mode))
  :hook
  ((logos-focus-mode-extra-functions . mp-logos-hide-menu-bar)
   (ef-themes-post-load-hook . logos-update-fringe-in-buffers)
   (modus-themes-post-load-hook . logos-update-fringe-in-buffers)
   (logos-page-motion-hook . mp-logos--recenter-top))
  :config
  (setopt logos-outlines-are-pages t
          logos-outline-regexp-alist
          `((emacs-lisp-mode . ,(format "\\(^;;;+ \\|%s\\)" logos--page-delimiter))
            (org-mode . ,(format "\\(^\\*+ +\\|^-\\{5\\}$\\|%s\\)" logos--page-delimiter))
            (markdown-mode . ,(format "\\(^\\#+ +\\|^[*-]\\{5\\}$\\|^\\* \\* \\*$\\|%s\\)" logos--page-delimiter))
            (ess-r-mode . ,(format "\\(^\\#+[^\\#]+[\\#-=]\\{4,\\}$\\|%s\\)" logos--page-delimiter))) ; BUG!
          )
  ;; These apply when `logos-focus-mode' is enabled.  Their value is
  ;; buffer-local.
  (setq-default logos-hide-mode-line nil
                logos-hide-buffer-boundaries t
                logos-hide-fringe t
                mp-logos-hide-menu-bar t
                logos-variable-pitch nil
                logos-buffer-read-only nil
                logos-scroll-lock nil
                logos-olivetti t)
  :preface
  (defcustom mp-logos-hide-menu-bar nil
    "When non-nil hide the menu bar.
This is only relevant when `logos-focus-mode' is enabled."
    :type 'boolean
    :group 'logos
    :local t)

  (defun mp-logos-hide-menu-bar ()
    "Set `menu-bar-mode' to -1"
    (when mp-logos-hide-menu-bar
      (logos--mode 'menu-bar-mode -1)))

  (defun mp-logos--recenter-top ()
    "Use `recenter' to reposition the view at the top."
    (unless (derived-mode-p 'prog-mode)
      (recenter 1))) ; Use 0 for the absolute top
  )

(provide 'mp-ux-files-buffers)
;;; mp-ux-files-buffers.el ends here
