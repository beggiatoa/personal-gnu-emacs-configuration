;;; mp-app-pocket.el --- Mozilla's Pocket            -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Marco Prevedello

;; Author: Marco Prevedello <marco.prevedello@outlook.it>
;; Keywords: extensions, news

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(use-package pocket-reader
  :ensure t
  :bind ("C-c r" . pocket-reader))

(provide 'mp-app-pocket)
;;; mp-app-pocket.el ends here
