;;; mp-app-bootstrap-projects.el --- Bootstrap projects from templates   -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <m.prevedello1@universityofgalway.ie>
;; Keywords: tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Generate a project from a template

;;; Code:

;;;; Requirements
(use-package s :ensure t)
(use-package f :ensure t)

;;;; User variables
(defgroup project-bootstrap nil
  "Generating project from templates.

Inspired from cookiecutter."
  :prefix "pb-")

(defcustom pb-projects-root-dir "~/1-Projects/"
  "Root directory for projects")

(defcustom pb-templates-plist
  '((generic
     (:name "Generic project"
            :description "A generic project with basic features like a README file and an org-mode notebook."
            :directories ("docs/")
            :files (("docs/main.org" . "#+title: {NAME}
#+subtitle: Documentation notebook
#+author: Marco Prevedello
#+email: marco.prevedello@outlook.it
:options:
:end:

* Foreword
* Main
* Bibliography

#+print_bibliography:

* Footnotes")
                    ("README.md" . "# {NAME}

**License**: CC BY-SA 4.0. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/

**Contact**: Marco Prevedello ([@Preve92](https://twitter.com/Preve92),
[marco.prevedello@outlook.it](mailto:marco.prevedello@outlook.it))

**Project Status:** See the [changelog](./CHANGELOG.md).

## Repository Structure

### Project Data Management

## Getting started

### 0. Install the Requirements

### 1. Clone the Project Repository

### 2. Generate All Processed Data

## Repository Guidelines

## How to Contribute

")
                    ("LICENSE" . ""))
            :timestamp t
            :git-init t
            :git-cliff t
            :conda-env nil
            :extra-comds nil))
    (empty-template
     (:name "Empty"
            :description "An empty template: creates only the pproject root directory.")))
  "Property list of project templates.")

;;;; Helper functions
(defun pb-write-content-to-file (file content)
  (with-temp-buffer
    (insert content)
    (write-region nil nil file)))

(defvar pb-project-names-history nil
  "History for `pb-read-valid-project-name'")

(defun pb-read-valid-project-name ()
  "Read a valid project name.

The project name can contain only alphanumeric characters, spaces,
 dots, underscores, and hyphens."

  (let* ((input (read-from-minibuffer "Project name: " nil nil nil 'pb-project-names-history))
         (valid-p (and
                   (not (string-empty-p input))
                   (not (s-matches? "[^a-zA-Z0-9. _-]" input)))))
    (if valid-p
        input
      (message "Invalid input. Please enter a valid string: ")
      (sit-for 2)
      (pb-read-valid-project-name))))

(defun pb-timestamp-file (filename &optional time)
  "Prefix FILENAME with current time stamp."
  (let ((time (or time (current-time))))
    (format "%s--%s"
            (format-time-string "%Y%m%dT%H%M%S")
            filename)))

(defun pb-clean-path-from-name (name)
  "Convert NAME to syntatically clean path."
  (unless (string-empty-p name)
    (s-replace " " "-"
               (s-downcase
                (s-collapse-whitespace
                 (s-trim name))))))

(defun pb-make-project-directories-recursively (template path)
  "Create project directories according to TEMPLATE in PATH."
  (dolist (dir (plist-get template :directories))
    (make-directory
     (expand-file-name dir path)
     t)))

(defun pb-populate-project-files (template name path)
  "Populate project files according to TEMPLATE and NAME in PATH."
  (dolist (file-info (plist-get template :files))
    (let ((file-name (expand-file-name (car file-info) path))
          (boilerplate (replace-regexp-in-string
                        "{name}" name
                        (cdr file-info) t)))
      (f-touch file-name)
      (pb-write-content-to-file file-name boilerplate))))

(defun pb-git-init (path)
  "Initialize git in PATH."
  (call-process-shell-command
   (format "cd %s && git init"
           path)))

(defun pb-configure-git-cliff (path)
  "Configure Git Cliff in PATH.

See <https://git-cliff.org/>"
  (unless (string-empty-p (shell-command-to-string "command -v git-cliff"))
  (call-process-shell-command
   (format "cd %s && git cliff --init" path))))

(defun pb-configure-conda-env (path name)
  (let ((env-file (expand-file-name "environment.yml" path)))
    (unless (file-exists-p env-file)
      (pb-write-content-to-file
       env-file
       (format
        "name: %s\nchannels:\n  - conda-forge\n  - bioconda\n  - defaults"
        name)))))

;;;; Main functions

;; Bootstrap project
(defun pb-bootstrap-project (template project-name)
  "Bootstrap a project according to TEMPLATE and PROJECT-NAME.

TEMPLATE must be an element of `pb-templates-plist'.  See the
function `pb-define-template' for how to interactively create a
template.

PROJECT-NAME must be a string of alphanumeric characters, spaces,
underscores, or hyphens. No other character is allowed."

  (interactive
   (list (intern (completing-read
                  "Project template: "
                  (mapcar 'car pb-templates-plist)
                  nil t))
         (pb-read-valid-project-name)))

  (message "Bootstrapping %s with template %s" project-name template)
  (sit-for 2)

  (let* ((time (current-time))
         (template-name (car (assoc template pb-templates-plist)))
         (template-plist (cadr (assoc template pb-templates-plist)))
         (timestamp-p (plist-get template-plist :timestamp))
         (git-init-p (plist-get template-plist :git-init))
         (git-cliff-p (plist-get template-plist :git-cliff))
         (conda-p (plist-get template-plist :conda))
         (extra-cmds (plist-get template-plist :extra-cmds))
         (project-simple-name (pb-clean-path-from-name project-name))
         (project-dir
          (if timestamp-p
              (pb-timestamp-file project-simple-name time)
             project-simple-name))
         (project-path (expand-file-name project-dir pb-projects-root-dir)))

    (if (not (plistp template-plist))
        (error "%s is not a valid template" template-name)

      ;; Create directories
      (pb-make-project-directories-recursively
       template-plist project-path)
      (message "Created directories %s"
               (plist-get template-plist :directories))

      ;; Create files with boilerplate
      (pb-populate-project-files
       template-plist project-name project-path)
      (message "Populated files %s with boilerplate"
               (mapcar 'car (plist-get template-plist :files)))

      ;; Initialize Git repository
      (when git-init-p
        (pb-git-init project-path)
        (message "Git initialized"))

      ;; Confiure git-cliff
      (when git-cliff-p
        (pb-configure-git-cliff project-path)
        (message "Git Cliff initialized"))

      ;; Confiure basic conda environment
      (when conda-p
        (pb-configure-conda-env project-dir project-name)
        (message "Conda initialized"))

      ;; Run extra commands
      (unless (string-empty-p extra-cmds)
        (call-process-shell-command
         (format "cd %s && %s"
                 project-path extra-cmds)))

      (message "Project %s successfully bootstrapped."
               project-name))

    (project-switch-project project-path)))

;;;; Provide script
(provide 'mp-app-bootstrap-projects)

;;; mp-app-bootstrap-projects.el ends here
