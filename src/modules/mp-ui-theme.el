;;; mp-ui-theme.el --- Theme user interface (UI) settings  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marcop@p7560-f37>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:
;;;; Modus-themes settings

(use-package modus-themes
  :ensure t
  :bind
  (("s-<f5>" . modus-themes-select))
  :custom
  (modus-themes-disable-other-themes t)
  (modus-themes-mixed-fonts t)
  (modus-themes-headings ; read the manual's entry or the doc string
           '((0 variable-pitch light 1.8)
             (1 variable-pitch light 1.5)
             (2 variable-pitch regular 1.3)
             (3 variable-pitch regular 1.2)
             (4 variable-pitch 1.1)     ; absence of weight means `bold'
             (t variable-pitch 1.1)))
  :hook
  ((modus-themes-post-load-hook . modus-themes-org-custom-faces)
   (pdf-tools-enabled-hook . modus-themes-pdf-tools-midnight-toggle)
   (modus-themes-post-load-hook . modus-themes-pdf-tools-themes-toggle))
  :config
  ;; Faces for org-mode
  (defun modus-themes-org-custom-faces ()
    "Configure org-mode faces with modus themes colors.
The exact color values are taken from the active modus theme."
    (modus-themes-with-colors
      (custom-set-faces
       `(org-todo ((,c :weight bold)))
       `(org-done ((,c :weight bold)))
       `(mp-org-next ((,c :slant italic :foreground ,prose-todo)))
       `(mp-org-started ((,c :slant italic :foreground ,yellow)))
       `(mp-org-wait ((,c :underline t)))
       `(mp-org-stop ((,c :foreground ,fg-dim)))
       `(mp-org-cancelled ((,c :foreground ,fg-dim :strike-through t)))
       `(mp-org-delegated ((,c ::foreground ,blue-faint :weight light)))
       `(mp-org-review ((,c :foreground ,magenta)))
       `(mp-org-project ((,c :foreground ,cyan :weight bold)))
       `(mp-org-done-project ((,c :foreground ,green-cooler)))
       `(org-quote ((,c :foreground ,fg-alt)))
       `(org-verse ((,c :foreground ,fg-alt))))))
  ;; PDF tools background settings
  (defun modus-themes-pdf-tools-backdrop ()
    (modus-themes-with-colors
      (face-remap-add-relative
       'default
       `(:background ,bg-dim))))

  (defun modus-themes-pdf-tools-midnight-toggle ()
    (when (derived-mode-p 'pdf-view-mode)
      (if (eq (car custom-enabled-themes) 'modus-vivendi)
          (pdf-view-midnight-minor-mode 1)
        (pdf-view-midnight-minor-mode -1))
      (modus-themes-pdf-tools-backdrop)))

  (defun modus-themes-pdf-tools-themes-toggle ()
    (mapc
     (lambda (buf)
       (with-current-buffer buf
         (modus-themes-pdf-tools-midnight-toggle)))
     (buffer-list))))

;;;; Extra: Ef-themes
(use-package ef-themes
  :ensure t
  :hook
  ((pdf-tools-enabled-hook . ef-themes-pdf-tools-midnight-toggle)
   (ef-themes-post-load-hook . ef-themes-pdf-tools-themes-toggle))
  :bind
  (("<f5>" . ef-themes-toggle))
  :custom
  (ef-themes-disable-other-themes t)
  (ef-themes-to-toggle '(ef-light ef-dream))
  (ef-themes-mixed-fonts t)
  ()
  (ef-themes-headings ; read the manual's entry or the doc string
           '((0 variable-pitch light 1.8)
             (1 variable-pitch light 1.5)
             (2 variable-pitch regular 1.3)
             (3 variable-pitch regular 1.2)
             (4 variable-pitch 1.1)     ; absence of weight means `bold'
             (t variable-pitch 1.1)))
  :config
  (defun ef-themes-pdf-tools-backdrop ()
    (ef-themes-with-colors
      (face-remap-add-relative
       'default
       `(:background ,bg-dim))))

  (defun ef-themes-pdf-tools-midnight-toggle ()
    (when (derived-mode-p 'pdf-view-mode)
      (if (eq (car custom-enabled-themes) 'ef-dream)
          (pdf-view-midnight-minor-mode 1)
        (pdf-view-midnight-minor-mode -1))
      (ef-themes-pdf-tools-backdrop)))

  (defun ef-themes-pdf-tools-themes-toggle ()
    (mapc
     (lambda (buf)
       (with-current-buffer buf
         (ef-themes-pdf-tools-midnight-toggle)))
     (buffer-list))))

;;;; Dbus to set theme according to GNOME theme
(use-package dbus
  :preface
  (defun modus-theme-select-from-dbus-value (value)
    "Set the appropiate theme according to the color-scheme setting value."
    (if (equal value '1)
        (modus-themes-select 'modus-vivendi)
      (modus-themes-select 'modus-operandi)))

  (defun dbus-color-scheme-changed (path var value)
    "DBus handler to detect when the color-scheme has changed."
    (when (and (string-equal path "org.freedesktop.appearance")
               (string-equal var "color-scheme"))
      (modus-theme-select-from-dbus-value (car value))))
  :config

  ;; Register for future changes
  (dbus-register-signal
     :session "org.freedesktop.portal.Desktop"
     "/org/freedesktop/portal/desktop" "org.freedesktop.portal.Settings"
     "SettingChanged"
     #'dbus-color-scheme-changed)

  ;; Request the current color-scheme
  (dbus-call-method-asynchronously
   :session "org.freedesktop.portal.Desktop"
   "/org/freedesktop/portal/desktop" "org.freedesktop.portal.Settings"
   "Read"
   (lambda (value) (modus-theme-select-from-dbus-value (caar value)))
   "org.freedesktop.appearance"
   "color-scheme"))

;;;; Ligatures and additional glyphs
(use-package ligature
  :ensure t
  :config
  (ligature-set-ligatures 'org-mode '("<---" "<--" "<-" "->" "-->"
                                      "--->" "<->" "<-->" "<--->"
                                      "<---->" "<==" "<===" "<="
                                      "=>" "==>" "===>" ">=" "<=>"
                                      "<==>" "<===>" "<====>"
                                      "==" "!=" ":-" "<|" "<|>"
                                      "|>" "++" "+++"))
  ;; Enable all Iosevka ligatures in programming modes
  (ligature-set-ligatures 'prog-mode '("<---" "<--"  "<<-" "<-" "->"
                                       "-->" "--->" "<->" "<-->" "<--->"
                                       "<---->" "<!--" "<==" "<===" "<="
                                       "=>" "=>>" "==>" "===>" ">=" "<=>"
                                       "<==>" "<===>" "<====>" "<!---"
                                       "<~~" "<~" "~>" "~~>" "::" ":::"
                                       "==" "!=" "===" "!==" ":=" ":-"
                                       ":+" "<*" "<*>" "*>" "<|" "<|>"
                                       "|>" "+:" "-:" "=:" "<******>"
                                       "++" "+++"))
  ;; Enables ligature checks globally in all buffers. You can also do it
  ;; per mode with `ligature-mode'.
  (global-ligature-mode t))

;;;; Cursor appearance
(blink-cursor-mode)
(setq-default blink-cursor-delay 2.5)

(setq-default cursor-type 'box)
(setq-default cursor-in-non-selected-windows 'hollow)

(defun mp-cursor--set-hbar ()
  "To be hooked in `view-mode-hook'."
  (setq cursor-type (if view-mode 'hbar 'box)))

(add-hook 'view-mode-hook #'mp-cursor--set-hbar)

;;;; Fringes
(setq bookmark-set-fringe-mark nil)

;;;; Extra: `spacious-padding' for Spacious fringes

(use-package spacious-padding
  :ensure t
  :bind
  (("C-<f9>" . spacious-padding-mode))
  :custom
  (spacious-padding-subtle-mode-line t))

;;;; Better `hl-line-mode' with lin.el
(use-package lin
  :ensure t
  :init
  (setq lin-mode-hooks
        '(dired-mode-hook
          elfeed-search-mode-hook
          grep-mode-hook
          ibuffer-mode-hook
          log-view-mode-hook
          magit-log-mode-hook
          mu4e-headers-mode
          occur-mode-hook
          org-agenda-mode-hook
          pdf-outline-buffer-mode-hook
          proced-mode-hook
          tabulated-list-mode-hook))

  (lin-global-mode))

;;;; Pulsar
;; Read the pulsar manual: <https://protesilaos.com/emacs/pulsar>.
(use-package pulsar
  :ensure t
  :bind
  (("C-h C-h" . pulsar-pulse-line) ; override `count-lines-page'
   ("C-h M-h" . pulsar-highlight-dwim)
   ("C-h h" . help-for-help))
  :hook
  (;; integration with the `consult' package:
   (consult-after-jump-hook . (pulsar-recenter-top pulsar-reveal-entry))
   ;; integration with the built-in `imenu':
   (imenu-after-jump-hook . (pulsar-recenter-top pulsar-reveal-entry))
   (next-error-hook . pulsar-pulse-line-red))
  :config
  (dolist (cmd '(other-window
                 narrow-to-page
                 narrow-to-defun
                 narrow-to-region
                 widen
                 logos-forward-page-dwim
                 logos-backward-page-dwim))
    (add-to-list 'pulsar-pulse-functions cmd))

  (setopt pulsar-pulse t
          pulsar-delay 0.055
          pulsar-iterations 10
          pulsar-face 'pulsar-magenta
          pulsar-highlight-face 'pulsar-yellow)

  (pulsar-global-mode 1))

;;;; Provide file

(provide 'mp-ui-theme)
;;; mp-ui-theme.el ends here
