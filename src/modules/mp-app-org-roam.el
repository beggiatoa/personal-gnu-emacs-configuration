;;; mp-app-org-roam.el --- Note taking with org-mode -*- lexical-binding: t -*-

;; Copyright (C) 2020-2022 Marco Prevedello <marco.prevedello@outlook.it>

;; Author: Marco Prevedello <marco.prevedello@outlook.it>
;; Maintainer: Marco Prevedello <marco.prevedello@outlook.it>
;; Created: [2023-07-05 Wed]
;; Version: 0.1.0
;; Homepage: https://gitlab.com/beggiatoa/personal-gnu-emacs-configuration.git

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:
;;;; Org-roam
(use-package org-roam
  :ensure t
  :after org
  :bind
  (("C-c nl" . org-roam-buffer-toggle)
   ("C-c nf" . org-roam-node-find)
   ("C-c ni" . org-roam-node-insert)
   ("C-c n <tab>" . mp/org-capture-org-roam-inbox)
   ("C-c ng" . mp/org-roam-consult-ripgrep)
   :map org-mode-map
   ("C-c n a" . org-roam-alias-add)
   ("C-c n t" . org-roam-tag-add))
  :hook
  ((after-init-hook . org-roam-db-autosync-mode)
   (org-roam-capture-new-node-hook . mp//org-roam-tag-add-draft))
  :preface
  ;; Search notes with consult ripgrep
  (defun mp/org-roam-consult-ripgrep ()
    "Search org-roam directory using consult-ripgrep. With live-preview."
    (interactive)
    (consult-ripgrep org-roam-directory))
  ;; Set new notes as drafts
  (defun mp//org-roam-tag-add-draft ()
    "Add tag \"draft\" to a org roam node."
    (org-roam-tag-add '("draft")))
  :init
  (setq org-roam-directory
        (file-truename
         (expand-file-name "~/Dropbox/Org-Roam"))
        org-roam-completion-everywhere t
        org-roam-db-gc-threshold most-positive-fixnum
        org-roam-db-location
        (expand-file-name "org-roam.db"
                          mp-emacs--cache-dir)
        org-roam-mode-sections
        (list #'org-roam-backlinks-section
              #'org-roam-reflinks-section
              #'org-roam-unlinked-references-section))

  (unless (file-exists-p org-roam-directory)
    (make-directory org-roam-directory))

  (setq org-roam-capture-templates
        '(("o" "original" plain
           "%?"
           :if-new (file+head "Original/${slug}.org"
                              ":PROPERTIES:
:CREATED: %u
:END:
#+title: ${title}
")
           :immediate-finish nil
           :unnarrowed t)

          ("r" "reference" plain "%?"
           :if-new
           (file+head "References/${slug}.org"
                      ":PROPERTIES:
:CREATED: %u
:END:
#+title: ${title}
")
           :immediate-finish t
           :unnarrowed t)

          ("p" "placeholder" plain "%?"
           :if-new
           (file+head "Placeholders/${slug}.org"
                      ":PROPERTIES:
:CREATED: %u
:END:
#+title: ${title}
")
           :immediate-finish t
           :unnarrowed t)

          ("m" "meeting" plain "%?"
           :if-new
           (file+head "Meetings/${slug}.org"
                      ":PROPERTIES:
:CREATED: %u
:END:
#+title: ${title}
"))))

  :config
  ;; General org capture for notes inbox
  (add-to-list 'org-capture-templates
               `("r" "Org-Roam Inbox" entry
                 (file ,(expand-file-name "inbox.org" org-roam-directory))
                 "* %?\n:PROPERTIES:\n:CREATED: %u\n:END:"
                 :prepend t :empty-lines 1 :clock-resume t))

  (defun mp/org-capture-org-roam-inbox ()
    (interactive)
    (org-capture nil "r"))

  ;; Additional methods for `org-roam-display-template'
  (cl-defmethod org-roam-node-type ((node org-roam-node))
    "Return the TYPE of NODE."
    (condition-case nil
        (file-name-nondirectory
         (directory-file-name
          (file-name-directory
           (file-relative-name (org-roam-node-file node)
                               org-roam-directory))))
      (error "")))

  (cl-defmethod org-roam-node-backlinkscount ((node org-roam-node))
    (let* ((count (caar (org-roam-db-query
                         [:select (funcall count source)
                                  :from links
                                  :where (= dest $s1)
                                  :and (= type "id")]
                         (org-roam-node-id node)))))
      (format "[%d]" count)))

  (setq org-roam-node-display-template
        (concat "${type:14} ${title:48} "
                (propertize "${tags:*}" 'face 'org-tag)
                " ${backlinkscount:4}"))

  (setq org-roam-db-node-include-function
        (lambda ()
          (not (member                    ; Exclude these tags
                "ATTACH"
                (org-get-tags)))))

  (org-roam-setup))

;;;; Org roam UI
(use-package org-roam-ui
  :ensure t
  :after org-roam
  :init
  (setq org-roam-ui-browser-function #'browse-url-firefox)
  (setq browse-url-new-window-flag t)
  (setq org-roam-ui-sync-theme t)
  (setq org-roam-ui-follow t)
  (setq org-roam-ui-update-on-save t)
  (setq org-roam-ui-open-on-start t)
  :bind
  ("C-c nu" . org-roam-ui-open))

;;;; Websocket (required by org-roam-ui)
(use-package websocket
  :ensure t
  :after org-roam-ui)

;;; mp-app-org-roam.el ends here:
(provide 'mp-app-org-roam)
