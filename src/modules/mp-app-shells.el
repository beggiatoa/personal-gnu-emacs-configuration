;;; mp-app-shells.el --- Shells and terminals within Emacs  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marcop@p7560-f37>
;; Keywords: unix, terminals

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
;;;; EShell
(use-package eshell
  :bind
  (("C-c t" . eshell)
   )
  :config
  (setopt eshell-directory-name
          (expand-file-name "eshell/" mp-emacs--cache-dir)
          eshell-banner-message ""
          eshell-visual-commands nil))

;;;; Eat
(use-package eat
  :ensure t
  :hook
  (eshell-load-hook . eat-eshell-mode))

(provide 'mp-app-shells)
;;; mp-app-shells.el ends here
