;;; mp-app-pdf.el --- View, edit, annotate PDF files in Emacs  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marcop@p7560-f37>
;; Keywords: extensions, tools, multimedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:
;;;; Pdf-tools for higher resolution
(use-package pdf-tools
  :ensure t
  :custom
  (pdf-view-display-size 'fit-height)
  (pdf-view-use-dedicated-register nil)
  (pdf-view-use-unicode-ligther nil)
  :hook
  ((after-init-hook . pdf-loader-install))
  :preface
  (defun mp-scroll-other-window ()
    (interactive)
    (let* ((wind (other-window-for-scrolling))
           (mode (with-selected-window wind major-mode)))
      (if (eq mode 'pdf-view-mode)
          (with-selected-window wind
            (pdf-view-scroll-up-or-next-page))
        (scroll-other-window 2))))

  (defun mp-scroll-other-window-down ()
    (interactive)
    (let* ((wind (other-window-for-scrolling))
           (mode (with-selected-window wind major-mode)))
      (if (eq mode 'pdf-view-mode)
          (with-selected-window wind
            (progn
              (pdf-view-scroll-down-or-previous-page)
              (other-window 1)))
        (scroll-other-window-down 2))))

  ;; "Print" selected pages to a new file
  (defvar mp-pdf-selected-pages '())

  (defun mp-pdf-select-page ()
    "Add current page to list of selected pages."
    (interactive)
    (add-to-list 'mp-pdf-selected-pages (pdf-view-current-page) t))

  (defun mp-pdf-extract-selected-pages (file)
    "Save selected pages to FILE."
    (interactive "FSave as: ")
    (setq mp-pdf-selected-pages (sort mp-pdf-selected-pages #'<))
    (start-process "pdfjam" "*pdfjam*"
                   "pdfjam"
                   (buffer-file-name)
                   (mapconcat #'number-to-string
                              mp-pdf-selected-pages
                              ",")
                   "-o"
                   (expand-file-name file)))
  :bind
  (("C-M-v" . mp-scroll-other-window)
   ("C-M-S-v". mp-scroll-other-window-down)
   :map pdf-view-mode-map
   ("M-g g" . pdf-view-goto-page)
   ("M-g M-g" . pdf-view-goto-page)
   ("S" . mp-pdf-select-page)
   ("E" . mp-pdf-extract-selected-pages)))

;;;; Extra: org links to PDFs
(use-package org-pdftools
  :ensure t
  :after (pdf-tools org))

;;;; Extra: save position in PDF files
(use-package saveplace-pdf-view
  :ensure t)

;;;; Provide mp-app-pdf
(provide 'mp-app-pdf)
;;; mp-app-pdf.el ends here
