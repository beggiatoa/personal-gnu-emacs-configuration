;;; mp-app-casual-suite.el --- Transient based suite of utils  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Marco Prevedello

;; Author: Marco Prevedello <marco.prevedello@outlook.it>
;; Keywords: tools, convenience, extensions, transient

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; A suite of packages by Charles Choi that facilitate the use of other
;; packages via transient menus.  See
;; <https://github.com/kickingvegas/casual-suite>.

;;; Code:
(use-package casual-suite
  :ensure t
  :bind
  (("M-g a" . casual-avy-tmenu)
  (:map calc-mode-map ("C-o" . casual-calc-tmenu))
  (:map Info-mode-map ("C-o" . casual-info-tmenu))
  (:map dired-mode-map ("C-o" . casual-dired-tmenu)))
  (:map isearch-mode-map ("C-o" . casual-isearch-tmenu))
  (:map ibuffer-mode-map
        ("C-o" . casual-ibuffer-tmenu)
        ("F" . casual-ibuffer-filter-tmenu)
        ("s" . casual-ibuffer-sortby-tmenu))
  (:map bookmark-bmenu-mode-map
        ("C-o" . casual-bookmarks-tmenu)
        ("S" . casual-bookmarks-sortby-tmenu)
        ("J" . bookmark-jump))
  (:map org-agenda-mode-map
        ("C-o" . casual-agenda-tmenu)
        ("M-j" . org-agenda-clock-goto)
        ("J" . bookmark-jump)))

(provide 'mp-app-casual-suite)
;;; mp-app-casual-suite.el ends here
