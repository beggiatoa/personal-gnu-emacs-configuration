;;; mp-app-biblio.el --- Manage bibliographies (.bib) in Emacs  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <m.prevedello1@nuigalway.ie>
;; Keywords: bib

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Using Emacs, Org Mode, and Zotero for bibliography management.  See
;; also `mp-org-core' and `mp-org-note-taking'.

;;; Code:
;;;; Setting common bibliography files
(defcustom mp-bibliography-dir (file-truename "~/3-Resources/Bibliographies/")
  "Directory for bibliography files.")

(defcustom mp-bibliography-bib-files nil
  "A list of bib files to use as bibliographies.")

(defcustom mp-bibliography-pdf-dir-list nil
  "A list of directories of PDF files listed in one or more `mp-bibliography-bib-files'.")

(setq mp-bibliography-bib-files
      (directory-files
       (expand-file-name "Bib-files" mp-bibliography-dir)
       t
       ".*\\.bib$"))

(setq mp-bibliography-pdf-dir-list
      `(,(expand-file-name "Attachments" mp-bibliography-dir)))

;;;; Bibtex

(use-package bibtex
  :config
  (setopt bibtex-align-at-equal-sign t)
  (bibtex-set-dialect 'biblatex)
  ;; Add support for bib2gls
  (add-to-list 'bibtex-biblatex-entry-alist
               '("index" "Index (category=index) format for bib2gls"
                 nil
                 nil
                 (("name")
                  ("description")
                  ("plural")
                  ("symbol")
                  ("category" "Default to \"index\""))))
  (add-to-list 'bibtex-biblatex-entry-alist
               '("entry" "Glossary entry format for bib2gls"
                 (("name")
                  ("description"))
                 nil
                 (("plural")
                  ("symbol")
                  ("category" "Default to \"main\""))))
  (add-to-list 'bibtex-biblatex-entry-alist
               '("abbreviation" "Abbreviation entry format for bib2gls"
                 (("short")
                  ("long"))
                 nil
                 (("description")
                  ("plural")
                  ("symbol")
                  ("category" "Default to \"abbreviation\""))))
  (add-to-list 'bibtex-biblatex-entry-alist
               '("acronym" "Acronym entry format for bib2gls"
                 (("short")
                  ("long"))
                 nil
                 (("plural")
                  ("symbol")
                  ("category" "Default to \"acronym\"")))))


;;;; `Citar' for browsing on BibTeX, BibLaTeX, etc. and as org-cite back-end
(use-package citar
  :ensure t
  :after org
  :bind
  (("C-c C-x @" . citar-insert-citation)
   ("C-c C-@" . citar-dwim)
   :map org-mode-map :package org
   ("C-c C-x @" . #'org-cite-insert))
  :init
  (setq org-cite-insert-processor 'citar
        org-cite-follow-processor 'citar
        org-cite-activate-processor 'citar
        citar-open-prompt '(citar-open citar-attach-files)
        citar-library-paths mp-bibliography-pdf-dir-list
        citar-bibliography mp-bibliography-bib-files
        citar-notes-paths '("~/Dropbox/Org-Roam/References/")
        citar-symbol-separator "  ")

  (when (featurep 'all-the-icons)
    (with-eval-after-load 'all-the-icons
      (setq citar-symbols
            `((file ,(all-the-icons-faicon "file-o" :face 'all-the-icons-green) . " ")
              (note ,(all-the-icons-faicon "sticky-note-o" :face 'all-the-icons-blue) . " ")
              (link ,(all-the-icons-faicon "link" :face 'all-the-icons-orange) . " ")))))
  :config
  (add-to-list 'citar-link-fields
               '(isbn . "https://isbnsearch.org/isbn/%s")))

;;;; Acting on bibliographic entries with `citar-embark'
(use-package citar-embark
  :ensure t
  :after citar embark
  :init
  (setq citar-at-point-function 'embark-act)
  :config (citar-embark-mode))

;;;; Citations in Org Mode
(use-package oc
  :after org
  :init
  (setq org-cite-global-bibliography mp-bibliography-bib-files))

;;;;; Org-cite processors
(use-package oc-basic
  :after oc)

(use-package oc-biblatex
  :after oc)

(use-package oc-natbib
  :after oc)

(use-package oc-csl
  :after oc
  :init
  (setq org-cite-csl-styles-dir "~/3-Resources/Bibliographies/Zotero/styles")
  (setq org-cite-export-processors '((beamer natbib)
                                     (latex biblatex)
                                     (t csl "chicago-author-date.csl"))))

;; ;;;; Bibliographics notes with citar-org-roam
;; (use-package citar-org-roam
;;   :ensure t
;;   :after (citar org-roam)
;;   :init
;;   (setq citar-org-roam-subdir "References/")
;;   (setq citar-org-roam-note-title-template
;;         "${author editor} (${year date issued:4})")

;;   (defun mp-citar-org-roam--create-capture-note (citekey entry)
;;     "Open or create org-roam node for CITEKEY and ENTRY."
;;     ;; adapted from https://jethrokuan.github.io/org-roam-guide/#orgc48eb0d
;;     (let ((title (citar-format--entry
;;                   citar-org-roam-note-title-template entry))
;;           (mp-actual-title (citar-format--entry "${title}" entry)))
;;       (org-roam-capture-
;;        :templates
;;        '(("r" "reference" plain "%?" :if-new
;;           (file+head
;;            "%(concat
;;  (when citar-org-roam-subdir (concat citar-org-roam-subdir \"/\")) \"${citekey}.org\")"
;;            "#+title: ${title}\n#+author: %n\n#+date: %<%F>\n\nNotes on /${actual-title}/.\n\n[cite:@${citekey}]\n\n|\n\n#+print_bibliography:")
;;           :immediate-finish t
;;           :unnarrowed t))
;;        :info (list :citekey citekey :actual-title mp-actual-title)
;;        :node (org-roam-node-create :title title)
;;        :props '(:finalize find-file))
;;       (search-backward "|")
;;       (delete-char 1)
;;       (org-roam-ref-add (concat "@" citekey))))

;;   (setq-default citar-org-roam-notes-config
;;                 (list :name "Org-Roam Notes"
;;                       :category 'org-roam-node
;;                       :items #'citar-org-roam--get-candidates
;;                       :hasitems #'citar-org-roam-has-notes
;;                       :open #'citar-org-roam-open-note
;;                       :create #'mp-citar-org-roam--create-capture-note))

;;   (with-eval-after-load 'citar
;;     (setcdr (assoc 'note citar-templates) citar-org-roam-note-title-template)
;;     (global-set-key (kbd "C-c nr") #'citar-create-note))
;;   :config
;;   (citar-org-roam-mode))

(provide 'mp-app-biblio)
;;; mp-app-biblio.el ends here
