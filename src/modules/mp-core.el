;;; mp-core.el --- Essential settings -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marco.prevedello@outlook.it>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; A complex amount of essential settings and definitions used
;; throughout my Emacs configuration.  The only "requirement" for this
;; file is to NOT load any external library.

;;; Code:
;;;; Increase how much is read from processes in a single chunk
(setq read-process-output-max (* 64 1024)) ; default is 4kb

;;;; Disable os-specific command line options that aren't useful
(unless (eq system-type 'darwin)
  (setq command-line-ns-option-alist nil))

;;;; Ignore bidirectional text
(setq bidi-inhibit-bpa t)

;;;; Store secrets only in encrypted files
(setq auth-sources '("~/.authinfo.gpg"))

;;;; Keep the `user-emacs-directory' organized
(defconst mp-emacs--cache-dir (expand-file-name "cache/" user-emacs-directory)
  "Directory for volatile storage.")
(unless (file-directory-p mp-emacs--cache-dir)
  (make-directory mp-emacs--cache-dir))

;; Other cache files are set in their specific package sections
(setq-default auto-save-list-file-prefix
              (expand-file-name "session" mp-emacs--cache-dir)
              bookmark-file
              (expand-file-name "bookmarks" mp-emacs--cache-dir)
              transient-history-file
              (expand-file-name "transient" mp-emacs--cache-dir)
              request-storage-directory
              (expand-file-name "request" mp-emacs--cache-dir)
              tramp-persistency-file-name
              (expand-file-name "tramp" mp-emacs--cache-dir))

;;;; Preserve bookmarks between Emacs sessions
(setq bookmark-save-flag 1)

;;;; General keybindings
(setq-default use-short-answers t)

(use-package mp-common
  :bind
  (("C-g" . mp-keyboad-quit)
   ("C-x C-z" . nil)
   ;; I keep closing Emacs instead of using org’s "C-c C-x" keybinding
   ("C-x C-c" . nil)
   ("C-x M-q" . save-buffers-kill-terminal)
   ("C-h h" . nil)
   ("M-`" . nil)
   ("C-h K" . describe-keymap)
   ("C-h C-k" . describe-key-briefly)
   ("C-h c" . describe-char)
   ("C-h t" . describe-face)
   ("C-h ." . mp-common-describe-symbol-dwim)

   ("M-o" . other-window)
   ("C-x 2" . mp-common-split-window-below-prompt-buffer)
   ("C-x 3" . mp-common-split-window-right-prompt-buffer)
   ("<f1>" . mp-window-buffer-focus-mode)
   ("C-<f1>" . tear-off-window)                 ; Remove the current buffer in a dedicated frame
   ("C-x k" . mp-common-kill-buffer-current)
   ("C-x K" . kill-buffer)

   ("M-D" . backward-kill-word)
   ("C-S-k" . mp-common-kill-line-backward)
   ("M-S-k" . mp-common-kill-sentence-backward)
   ("C-M-S-k" . mp-common-kill-sexp-backward)
   ("M-z" . zap-up-to-char)
   ("M-Z" . mp-common-zap-up-to-char-backward)
   ("C-M-z" . zap-to-char)
   ("C-M-S-z" . mp-common-zap-to-char-backward)
   ("M-t" . mp-common-transpose-words)
   ("M-T" . mp-common-transpose-words-backward)
   ("C-x M-t" . transpose-sentences)
   ("C-x M-T" . transpose-paragraphs)
   ("M-SPC" . cycle-spacing)
   ("C-<return>" . mp-common-new-line-below)
   ("C-S-<return>" . mp-common-new-line-above)

   ("M-'" . mp-common-insert-pair)      ; Overwrites `abbrev-prefix-mark'
   ("C-\"" . mp-common-insert-pair)
   ("M-\\" . mp-common-delete-pair-dwim)
   ("C-=" . mp-common-insert-date-time)

   ("M-c" . capitalize-dwim)
   ("M-l" . downcase-dwim)
   ("M-u" . upcase-dwim)

   ("M-Q" . mp-common-unfill-region-or-paragraph)
   ("M-=" . count-words)

   ("<f2>" . mp-common-rename-file-and-buffer) ; Mask 2C-* commands
   (:map ctl-x-x-map
         ("e" . eval-buffer)
         ("f" . follow-mode)            ; override `font-lock-update'
         ("r" . rename-uniquely)
         ("l" . visual-line-mode)
         ("(" . check-parens)
         ("w" . whitespace-cleanup))))

;;;; Enhanced page scrolling
(when (functionp 'pixel-scroll-precision-mode)
  (pixel-scroll-precision-mode))

(setq-default scroll-preserve-screen-position t
              scroll-conservatively 1 ; affects `scroll-step'
              scroll-margin 0
              next-screen-context-lines 1)

;;;; Use `view-mode' in read-only buffers
(setq view-read-only t)
;; Allow for kills command to copy when in `view-mode'
(setq kill-read-only-ok t)

;;;; Delete selection
(use-package delsel
  :hook
  (after-init-hook . delete-selection-mode))

;;;; Always use spaces
(setq-default indent-tabs-mode nil)

;;;; Preserve contents of system clipboard
(setq save-interprogram-paste-before-kill t)

;;;; Newline characters for file ending
(setq mode-require-final-newline 'visit-save)

;;;; Repeatable key chords (repeat-mode)
(use-package repeat
  :init
  (setq repeat-on-final-keystroke t
        repeat-exit-timeout 5
        repeat-exit-key "<escape>"
        repeat-keep-prefix nil
        repeat-check-key t
        repeat-echo-function 'ignore
        ;; Technically, this is not in repeal.el, though it is the
        ;; same idea.
        set-mark-command-repeat-pop t)
  :hook
  (after-init-hook . repeat-mode))

;; Enable certain "protected" commands
(dolist (c '(narrow-to-region
             narrow-to-page
             upcase-region
             downcase-region
             dired-find-alternate-file))
  (put c 'disabled nil))

;;;; Set dedicated custom file
;; The custom file is specific to the machine in use and it is NOT
;; tracked.  If in doubt about its content see the Emacs manual page
;; info:emacs#Easy Customization.

(setq custom-file (expand-file-name
                   (concat "custom-" (system-name) ".el")
                   mp-emacs--cache-dir))

(defun mp-emacs--load-cutom-file nil
  (load custom-file :no-error-if-file-is-missing))

(add-hook 'after-init-hook 'mp-emacs--load-cutom-file)

;;;; Emacs desktop (save state of various variables)
(use-package desktop
  :hook
  (server-after-make-frame-hook . desktop-read)
  (server-after-make-frame-hook . desktop-save-mode)
  :config
  (setq-default desktop-dirname mp-emacs--cache-dir)
  (setopt desktop-path `(,mp-emacs--cache-dir)
          desktop-auto-save-timeout 300
          desktop-base-file-name "desktop"
          desktop-load-locked-desktop 'check-pid
          desktop-missing-file-warning nil
          desktop-restore-frames nil
          desktop-save 'ask-if-new)

  (dolist (symbol '(kill-ring log-edit-comment-ring))
    (add-to-list 'desktop-globals-to-save symbol)))

;;;; Command history with `savehist'
(use-package savehist
  :config
  (setq history-length 10000)
  (setq history-delete-duplicates t)
  (setq savehist-save-minibuffer-history t)
  (setq savehist-file (expand-file-name
                       "history" mp-emacs--cache-dir))

  (savehist-mode))

;;;; Recent files with `recentf'
(use-package recentf
  :config
  (setq recentf-save-file (expand-file-name
                           "recentf" mp-emacs--cache-dir))
  (setq recentf-max-saved-items 200)
  (setq recentf-exclude '(".gz" ".xz" ".zip" "/elpa/" "/ssh:" "/sudo:"))

  (recentf-mode))

;;;; Record cursor position
(use-package saveplace
  :config
  (setq save-place-file (expand-file-name
                         "places" mp-emacs--cache-dir))
  (setq save-place-forget-unreadable-files t)
  (setq goto-line-history-local t)

  (save-place-mode))

;;;; Backups
(defconst mp-emacs--backup-dir (concat user-emacs-directory "backup/")
  "Directory for backup storage.")

(setopt auto-save-file-name-transforms
        `((".*" ,(concat mp-emacs--backup-dir "\\1") t))
        backup-directory-alist
        `(("." . ,mp-emacs--backup-dir))
        backup-by-copying t
        version-control t
         delete-old-versions t
         kept-new-versions 6
         kept-old-versions 2)

;;;; Provide mp-core

(provide 'mp-core)
;;; mp-core.el ends here
