;;; mp-app-git.el --- Git integration via `magit'    -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marcop@p7560-f37>
;; Keywords: vc

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Mainly magit

;;; Code:

;;;; Magit
(use-package magit
  :ensure t
  :bind
  (("C-x g" . magit-status)
   ("C-x M-g" . magit-dispatch)
   ("C-c M-g" . magit-file-dispatch))
  :hook
  ((git-commit-setup-hook . conventional-commit-setup))
  :init
  (setq vc-follow-symlinks t)
  :config
  (require 'git-commit)
  (require 'magit-diff)
  (require 'magit-repos)
  (require 'magit-extras)

  ;; Inspired from Akira Komamura <akira.komamura@gmail.com>
  ;; URL: https://github.com/akirak/conventional-commit.el

  (defvar conventional-commit-type-list '("feat" "fix" "build"
                                          "chore" "ci" "docs"
                                          "style" "refactor" "perf"
                                          "test")
    "List of types allowed at the beginning of a message.

See <https://www.conventionalcommits.org/en/v1.0.0/> for full
specifications.")

  (defvar conventional-commit-scope-history nil
    "History of conventional commit scope

See <https://www.conventionalcommits.org/en/v1.0.0/> for full specifications.")

  (defun conventional-commit-setup ()
    (let ((type (completing-read "Commit type: "
                                 conventional-commit-type-list
                                 nil t))
          (scope (read-from-minibuffer "Scope(optional): "
                                       nil nil nil
                                       conventional-commit-scope-history))
          (breaking (y-or-n-p "Breaking changes? ")))
      (insert type)
      (if (not (string-empty-p scope)) (insert "(" scope ")"))
      (if (not breaking) nil
        (insert "!")
        (save-excursion (insert "\n\nBREAKING CHANGE: ")
                        (insert (read-from-minibuffer "Breaking change: "))))
      (insert ": ")))

  (magit-add-section-hook 'magit-status-sections-hook
                          'magit-insert-modules
                          'magit-insert-unpulled-from-upstream)

  (setopt magit-module-sections-nested nil)

  ;; BUG: Warning for match type when using `setopt'
  (setq git-commit-style-convention-checks
        '(non-empty-second-line overlong-summary-line))

  (setopt git-commit-summary-max-length 50
          git-commit-fill-column 70
          git-commit-known-pseudo-headers
          '("Signed-off-by" "Acked-by" "Modified-by" "Cc"
            "Suggested-by" "Reported-by" "Tested-by" "Reviewed-by")
          magit-comit-show-diff nil     ; Use C-c C-d to toggle display
          magit-diff-refine-hunk t
          magit-repository-directories '(("~/1-Projects" . 1))))

;;;; Magit LFS (interaction with git-lfs)
(use-package magit-lfs
  :ensure t)

;;;; Working with Git Forges
;;;;; For API request integrations with magit
(use-package forge
  :ensure t
  :after magit
  :config
  (setopt forge-database-file
          (expand-file-name
           (f-filename forge-database-file)
           mp-emacs--cache-dir)))

;;;; Git-Cliff integration (CHANGELOG)
(use-package git-cliff
  :ensure t
  :config
  (setopt git-cliff-executable "~/.cargo/bin/git-cliff")
  (with-eval-after-load 'magit-tag
    (transient-append-suffix 'magit-tag
      '(1 0 -1)
      '("c" "changelog" git-cliff-menu))))

;;;; Provide mp-app-git

(provide 'mp-app-git)

;;; mp-app-git.el ends here
