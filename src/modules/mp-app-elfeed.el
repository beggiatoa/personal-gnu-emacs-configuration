;;; mp-rss-feed.el --- RSS Feeds in Emacs            -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Marco Prevedello

;; Author: Marco Prevedello <m.prevedello1@universityofgalway.ie>
;; Created: 2022-11-26

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
;;;; RSS feeds with Elfeed
(use-package elfeed
  :ensure t
  :hook
  (elfeed-show-mode . visual-line-fill-column-mode)
  :bind
  (("C-c e" . elfeed)
   :map elfeed-search-mode-map
   ("w" . elfeed-search-yank)
   ("g" . elfeed-update)
   ("G" . elfeed-search-update--force)
   :map elfeed-show-mode-map
   ("w" . elfeed-show-yank))
  :config
  ;; Set feeds
  (setopt elfeed-feeds
          '(;; Stat blogs
            ("https://statmodeling.stat.columbia.edu/feed/" blog
             statistics social-sciences)
            ("https://elevanth.org/blog/feed" blog statistics
             antrophology)
            ;; General science blogs
            ("https://retractionwatch.com/feed" publishing retraction
             science-policy)
            ;; Journals
            ;;; Applied and Environmental Microbiology
            ("https://journals.asm.org/action/showFeed?type=etoc&feed=rss&jc=aem"
             journal ASM AEM microbiology envmicro)
            ;;; Microbiology Spectrum
            ("https://journals.asm.org/action/showFeed?type=etoc&feed=rss&jc=spectrum"
             journal ASM spectrum microbiology reviews)
            ;;; mSystems
            ("https://journals.asm.org/action/showFeed?type=etoc&feed=rss&jc=msystems"
             journal ASM mSys omics bioinfo microbiology)
            ;;; mBio
            ("https://journals.asm.org/action/showFeed?type=etoc&feed=rss&jc=mbio"
             journal ASM mBio microbiology envmicro)))

  (setq elfeed-db-directory
        (expand-file-name "elfeed/" mp-emacs--cache-dir))
  (setq elfeed-enclosure-default-dir "~/0-Inbox")
  (setopt elfeed-search-filter "@2-weeks-ago +unread"
          elfeed-search-clipboard-type 'CLIPBOARD
          elfeed-search-title-max-width 90
          elfeed-search-title-min-width 30
          elfeed-search-trailing-width 25
          elfeed-search-date-format '("%F" 10 :left))
  (setopt elfeed-show-truncate-long-urls t
          elfeed-show-unique-buffers t)

  ;; Mark old post as read
  (add-hook 'elfeed-new-entry-hook
            (elfeed-make-tagger :before "1 year ago"
                                :remove 'unread))

  ;; Mark all YouTube entries
  (add-hook 'elfeed-new-entry-hook
            (elfeed-make-tagger :feed-url "youtube\\.com"
                                :add '(video youtube)))

  ;; Concatenate entry authors list
  (defun concatenate-authors (authors-list)
    "Given AUTHORS-LIST, list of plists; return string of all authors
concatenated."
    (mapconcat
     (lambda (author) (plist-get author :name))
     authors-list ", "))
  ;; Display authors in elfeed-search buffer
  (defun mp-elfeed-search-print-fn (entry)
    "Personal variation of `elfeed-search-print-entry--default’.

Show author(s) additional to the data, title, and tags."
  (let* ((date (elfeed-search-format-date (elfeed-entry-date entry)))
         (title (or (elfeed-meta entry :title) (elfeed-entry-title entry) ""))
         (title-faces (elfeed-search--faces (elfeed-entry-tags entry)))
         (authors (or (concatenate-authors (elfeed-meta entry :authors)) ""))
         (feed (elfeed-entry-feed entry))
         (feed-title
          (when feed
            (or (elfeed-meta feed :title) (elfeed-feed-title feed))))
         (feed-authors
          (when feed
            (or (concatenate-authors (elfeed-meta feed :authors)) "")))
         (tags (mapcar #'symbol-name (elfeed-entry-tags entry)))
         (tags-str (mapconcat
                    (lambda (s) (propertize s 'face 'elfeed-search-tag-face))
                    tags ","))
         (title-width (- (window-width) 10 elfeed-search-trailing-width))
         (title-column (elfeed-format-column
                        title (elfeed-clamp
                               elfeed-search-title-min-width
                               title-width
                               elfeed-search-title-max-width)
                        :left))
         (authors-width 30)
         (authors-column (elfeed-format-column
                          authors (elfeed-clamp 25 authors-width 40)
                        :left)))
    (insert (propertize date 'face 'elfeed-search-date-face) " ")
    (insert (propertize title-column 'face title-faces 'kbd-help title) " ")
    (insert (propertize authors-column 'face title-faces 'kbd-help authors) " ")

    ;; (when feed-title
    ;;   (insert (propertize feed-title 'face 'elfeed-search-feed-face) " "))
    (when tags
      (insert "(" tags-str ")"))))

  (setopt elfeed-search-print-entry-function #'mp-elfeed-search-print-fn))

;;;; Provide this file
(provide 'mp-app-elfeed)

;;; mp-rss-feed.el ends here.
