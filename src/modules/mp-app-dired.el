;;; mp-app-dired.el --- Directory Editor in Emacs    -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marcop@p7560-f37>
;; Keywords: files, convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; See the DirEd manual.

;;; Code:
;;;; Built-in dired
(use-package dired
  :custom
  ;; This breaks listing in pCloud
  (dired-listing-switches "-AFGhl --group-directories-first --time-style=long-iso")
  (dired-hide-details-hide-symlink-targets nil)
  (dired-dwim-target t)
  (dired-recursive-copies 'always)
  (dired-recursive-deletes 'always)
  (delete-by-moving-to-trash t)
  (dired-auto-revert-buffer #'dired-directory-changed-p) ; also see `dired-do-revert-buffer'
  (dired-free-space nil) ; Emacs 29.1
  (dired-mouse-drag-files t) ; Emacs 29.1
  :hook
  ((dired-mode-hook . dired-hide-details-mode)
   (dired-mode-hook . hl-line-mode)))

;;;; Dired-aux
(use-package dired-aux
  :init
  (require 'dired-aux)
  :custom
  (dired-isearch-filenames 'dwim)
  ;; The following variables were introduced in Emacs 27.1
  (dired-create-destination-dirs 'ask)
  (dired-vc-rename-file t)
  ;; And this is for Emacs 28
  (dired-do-revert-buffer (lambda (dir) (not (file-remote-p dir))))
  :bind
  (:map dired-mode-map
        ("C-+" . dired-create-empty-file)
        ("M-s f" . nil)
        ("C-x v v" . dired-vc-next-action))
  :config
  (add-to-list 'dired-compress-file-alist
               '("\\.7z\\'" . "7z a -t7z %o %i"))
  (add-to-list 'dired-compress-files-alist
               '("\\.7z\\'" . "7z a -t7z %o %i"))
  )

;;;; Dired-X for extra features
(use-package dired-x
  :after dired
  :custom
  (dired-omit-files "\\`[.]?#\\|^\\..*\\|\\`[.][.]?\\'")
  :bind
  (:map dired-mode-map
        ("C-x M-o" . dired-omit-mode))
  :hook
  (dired-mode-hook . dired-omit-mode))

(use-package wdired
  :custom
  (wdired-allow-to-change-permissions t)
  (wdired-create-parent-directories t))

(use-package image-dired
  :custom
  (image-dired-dir
   (expand-file-name "image-dired-thumbs/" mp-emacs--cache-dir)))

;;;; dired-like mode for the trash (trashed.el)
(use-package trashed
  :ensure t
  :custom
  (trashed-action-confirmer 'y-or-n-p)
  (trashed-use-header-line t)
  (trashed-sort-key '("Date deleted" . t))
  (trashed-date-format "%Y-%m-%d %H:%M:%S"))

;;;; Dired subtree
(use-package dired-subtree
  :ensure t
  :custom
  (dired-subtree-use-backgrounds nil)
  :init
  (require 'dired-subtree)
  :bind
  (:map dired-mode-map
        ("<tab>" . dired-subtree-toggle)
        ("<backtab>" . dired-subtree-cycle)))

;;;; Dired file preview with dired-peep.el
(use-package peep-dired
  :ensure t
  :bind
  (:map dired-mode-map
   ("P" . peep-dired)
   :map peep-dired-mode-map
   ("SPC" . nil)
   ("C-SPC" . nil)
   ("<backspace>" . nil)))

;;;; File information with postframe
(use-package hydra :ensure t)
(use-package browse-at-remote :ensure t)
(use-package posframe :ensure t)
(use-package file-info
  :ensure t
  :bind (("M-<f1>" . 'file-info-show))
  :config
  (setq hydra-hint-display-type 'posframe)
  (setq hydra-posframe-show-params `(:poshandler posframe-poshandler-frame-center
                                               :internal-border-width 2
                                               :internal-border-color "#61AFEF"
                                               :left-fringe 16
                                               :right-fringe 16)))
;;;; Provide this file
(provide 'mp-app-dired)

;;; mp-app-dired.el ends here
