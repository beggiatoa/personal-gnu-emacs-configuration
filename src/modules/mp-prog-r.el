;;; mp-prog-r.el --- Emacs Speak Statistics         -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marcop@p7560-f37>
;; Keywords: statistics, languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Configuration for Emacs Speak Statistics (ESS).  See the manual at
;; <https://ess.r-project.org>.
;;
;; TODO! See `double-mode' for easier insertion of R-specific symbols
;; (e.g., "|" to insert "|>" first and "|" second)

;;; Code:
;;;; General
(use-package ess-site
  :ensure ess
  :mode
  ("/R/.*\\.q\\'"       . R-mode)
  ("\\.[rR]\\'"         . R-mode)
  ("\\.[rR]profile\\'"  . R-mode)
  ("NAMESPACE\\'"       . R-mode)
  ("CITATION\\'"        . R-mode)
  ("renv.lock"   . js-json-mode)
  (".Rhistory"   . ess-r-mode)
  (".lintr"      . conf-mode)
  ("\\.Rproj\\'" . conf-mode)
  ("\\.[Rr]out"         . R-transcript-mode)
  ("\\.Rd\\'"           . Rd-mode)
  ("\\.[Bb][Uu][Gg]\\'" . ess-bugs-mode)
  ("\\.[Bb][Oo][Gg]\\'" . ess-bugs-mode)
  ("\\.[Bb][Mm][Dd]\\'" . ess-bugs-mode)
  ("\\.[Jj][AaOoMm][Gg]\\'" . ess-jags-mode)
  ("\\.[Jj][Ll]\\'"     . ess-julia-mode)
  :commands (R julia ess-julia-mode)
  :bind
  (("C-x M-e" . ess-request-a-process)
   (:map ess-r-mode-map
         ("C-x x o" . mp-ess-outline-minor-mode)
         ("C-c -" . mp-insert-hyphen-to-fill)
         ;; Shortcut for pipes (|> %>%)
         ("C-S-m"   . " |>")
         ("C-%"     . " %>%")
         ;; Shortcut for assign <-
         ("M--"     . ess-insert-assign))
   (:map inferior-ess-r-mode-map
         ("C-S-m" . " |>")
         ("C-%"   . " %>%")
         ("M--"   . ess-insert-assign)
         ("<home>" . comint-bol)))
  :custom
  (ess-use-flymake nil)                 ; Potentially deactivate in favour of TS and Eglot
  (ess-use-ido nil)                     ; Prefer native completion
  (ess-plain-first-buffername nil)      ; Have all iESS buffers numbered
  (ess-ask-for-ess-directory nil)       ; don't ask for working directory
  (ess-directory-function 'vc-root-dir) ; use vs-root as working dir
  (ess-help-own-frame 'one)       ; Dedicated frame for help buffers
  (ess-nuke-trailing-whitespace-p 'ask)
  (ess-assign-list '(" <-" " <<- " " = " " -> " " ->> "))
  (ess-style 'RStudio)  ; Set code indentation
  (ess-ask-for-ess-directory nil) ; Do not ask what is the project directory
  (inferior-R-args "--quiet --no-restore-history --no-save ")
  ;; Font-locking
  (ess-R-font-lock-keywords
   '((ess-R-fl-keyword:keywords . t)
     (ess-R-fl-keyword:constants . t)
     (ess-R-fl-keyword:modifiers . t)
     (ess-R-fl-keyword:fun-defs . t)
     (ess-R-fl-keyword:assign-ops . t)
     (ess-R-fl-keyword:%op% . t)
     (ess-fl-keyword:fun-calls . t)
     (ess-fl-keyword:numbers . t)
     (ess-fl-keyword:operators . t)
     (ess-fl-keyword:delimiters . t)
     (ess-fl-keyword:= . t)
     (ess-R-fl-keyword:F&T . t)))
  (ess-function-template " <- function( ) {\n}\n") ; Rstudio style guide
  :hook
  (ess-mode-hook . prettify-symbols-mode)
  (Rd-mode-hook  . (lambda () (abbrev-mode 1) (font-lock-mode 1)))
  :config
  (setenv "IS_ESS" "true")
  :preface
  (defun mp-insert-hyphen-to-column (column)
    "Insert hyphen (\"-\") to COLUMN.
Internally calls `mp-repeat-char-to-column'."
    (interactive "NRepeat to column: ")
    (funcall 'mp-repeat-char-to-column column 45))

  (defun mp-insert-hyphen-to-fill ()
    "Insert hyphen (\"-\") to FILL-COLUMN.
Internally calls `mp-insert-hyphen-to-column'."
    (interactive)
    (mp-insert-hyphen-to-column fill-column))

  (defun mp-ess-outline-minor-mode ()
    "Activate and configure `outline-minor-mode` for ESS[R]."
    (interactive)
    (if (null outline-minor-mode)
        (progn
          (setq-local outline-regexp "^#+ +.*\\(----\\|====\\|####\\)")
          (outline-minor-mode 1)
          (defun outline-level ()
            (cond
             ((looking-at "^### ") 1)
             ((looking-at "^#### ") 2)
             ((looking-at "^##### ") 3)
             ((looking-at "^###### ") 4)
             ((looking-at "^[a-zA-Z0-9_\.]+ ?<- ?function(.*{") 3)
             (t 1000)))
    (message "Enabled `outline-minor-mode'"))
  (outline-minor-mode -1)
  (message "Disabled `outline-minor-mode'"))))

;;;; ESS insert object
(use-package ess-r-insert-obj
  :ensure t
  :after ess
  :bind
  (:map ess-r-mode-map
        ("C-c p v" . ess-r-insert-obj-col-name)
        ("C-c p V" . ess-r-insert-obj-value))
  (:map inferior-ess-r-mode-map
        ("C-c p v" . ess-r-insert-obj-col-name)
        ("C-c p V" . ess-r-insert-obj-value)))

;;;; ESS smart equals
(use-package ess-smart-equals
  :ensure t
  :custom
  (ess-smart-equals-extra-ops '(brace percent))
  :after
  (:any ess-r-mode inferior-ess-r-mode ess-r-transcript-mode)
  :config
  (ess-smart-equals-activate)

  (setopt ess-smart-equals-contexts
          '((t
             (comment)
             (string)
             (arglist "=" "==" "!=" "<=" ">=" "<-" "<<-" "%>%")
             (index "==" "!=" "%in%" "%nin%" "<" "<=" ">" ">=" "=")
             (conditional "==" "!=" "<" "<=" ">" ">=" "%in%" "%nin%")
             (base "<-" "<<-" "=" "==" "!=" "<=" ">=" "->" "->>" ":=")
             (% "|>" "%>%" "%*%" "%%" "%/%" "%in%" "%<>%" "%o%" "%x%")
             (not-% "<-" "<<-" "=" "->" "->>" "==" "!=" "<" "<=" ">" ">=" "+" "-" "*" "**" "/" "^" "&" "&&" "|" "||")
             (all "<-" "<<-" "=" "->" "->>" "==" "!=" "<" "<=" ">" ">=" "%*%" "%%" "%/%" "%in%" "%nin%" "%x%" "%o%" "%<>%" "%>%" "|>" "+" "-" "*" "**" "/" "^" "&" "&&" "|" "||")
             (t "<-" "<<-" "=" "==" "->" "->>" "%<>%"))
            (ess-roxy-mode
             (comment "<-" "=" "==" "<<-" "->" "->>" "%<>%"))))
  )

;;;; Interact with Renv
(use-package rutils
  :ensure t
  :defer t)

;;;; View data sets with `ess-view'
;; View manual at <https://github.com/GioBo/ess-view>
(use-package ess-view-data
  :ensure t
  :after ess
  :bind
  (:map ess-r-mode-map
        ("C-c C-. v" . ess-view-data-print))
  (:map ess-rdired-mode-map
        ("v" . ess-view-data-print))
  (:map ess-view-data-mode-map
        ("f" . ess-view-data-filter)
        ("g" . ess-view-data-group)
        ("m" . ess-view-data-mutate)
        ("o" . ess-view-data-sort)
        ("q" . ess-view-data-quit)
        ("S" . ess-view-data-summarise)
        ("s" . ess-view-data-select)
        ("u" . ess-view-data-unique)
        ("l 2 w" . ess-view-data-long2wide)
        ("w 2 l" . ess-view-data-wide2long)
        ("C-c C-p" . ess-view-data-goto-previous-page)
        ("C-c C-n" . ess-view-data-goto-next-page))
  :custom
  (ess-view-data-current-update-print-backend 'kable)
  (ess-view-data-rows-per-page 1000))

;;;; View plots in Emacs
(use-package essgd
  :ensure t
  :if (equal window-system 'pgtk)
  :defer t)

;;;; Support for RMarkdown
(use-package poly-R
  :ensure t
  :after ess org
  :mode
  (("\\.[rR]nw\\'"       . poly-noweb+r-mode)
   ("\\.[rR]md\\'"       . poly-markdown+r-mode)
   ("\\.[sS]nw\\'"       . poly-noweb+r-mode))
  :custom
  (markdown-code-block-braces t)
  :bind
  (:map org-mode-map
        ("<f12>" . poly-org-mode)))

;; ;;;; Support for Quarto
;; (use-package poly-markdown
;;   :bind (:map polymode-eval-map ("p" . quarto-preview)))

;; (use-package poly-R
;;   :mode ("\\.Rmd" . poly-markdown+r-mode))

;; (unless (package-installed-p 'quarto-mode)
;;   (package-vc-install "https://github.com/christophe-gouel/quarto-emacs"
;;                       "transient"))

;;;; R in org-mode
(with-eval-after-load 'org
  (add-to-list 'org-babel-load-languages '(R . t))
  (org-babel-do-load-languages 'org-babel-load-languages
                               org-babel-load-languages))

(provide 'mp-prog-r)
;;; mp-prog-r.el ends here
