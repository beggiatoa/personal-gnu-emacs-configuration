;;; mp-app-csv.el --- Editing of CSV (& co.) files   -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello(use-package  <marco.prevedello@outlook.it>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
;;;; Support for character separated text files (CSV, TSV, etc)
(use-package csv-mode
  :ensure t
  :bind
  (("C-c C-y" . csv-yank-fields))
  :hook
  (((csv-mode-hook tsv-mode-hook) . hl-line-mode)
   ((csv-mode-hook tsv-mode-hook) . orgtbl-mode)))

;;;; Orgtabl mode
(use-package org-table
  :config
  (add-to-list 'orgtbl-radio-table-templates
               '(csv-mode "# BEGIN RECEIVE ORGTBL %n
# END RECEIVE ORGTBL %n

#+ORGTBL: SEND %n orgtbl-to-csv
| | |")))

(provide 'mp-app-csv)
;;; mp-app-csv.el ends here
