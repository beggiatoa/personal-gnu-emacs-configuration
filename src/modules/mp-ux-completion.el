;;; mp-ux-completion.el --- REPLACE ME               -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marcop@p7560-f37>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Minibuffer and in-buffer text completion based on the built-in
;; `completing-read' (`CR'), expanded with the Orderless, Vertico,
;; Marginalia, and Consult packages.  Detailed informations about
;; these packages is available on their respective GitHub's
;; repositories:
;;
;; - Orderless completion style <https://github.com/oantolin/orderless>
;; - Vertico UI <https://github.com/minad/vertico>
;; - Marginalia metadata aggregator <https://github.com/minad/marginalia>
;; - Consult `CR' based functions <https://github.com/minad/consult>
;;
;; Additionally, the `embark' (<https://github.com/oantolin/embark>)
;; is set-up to act on things at point and completion candidates.

;;; Code:
;;;; Using vertico UI for in-buffer completion
;; Using `consult-completion-in-region' and `vertico' to have
;; completion in the minibuffer.  This has the disadvantage of not
;; bringing the completion at the point position, like `corfu' would
;; do.  However, it allows for a consistend UI/UX, where the
;; completion is always at the bottom of the frame.
(use-package consult
  :ensure t
  :after vertico                        ; See vertico section below
  :init
  (setq completion-cycle-threshold 3)
  (setq tab-always-indent 'complete)
  :config
  (setq completion-in-region-function
        (lambda (&rest args)
          (apply (if vertico-mode
                     #'consult-completion-in-region
                   #'completion--in-region)
                 args))))

;;;; Abbrevs
;; These are pre-determined expansions/sostitutions that happens
;; automatically.
(use-package abbrev
  :bind
  (("C-x a e" . expand-abbrev) ; default, just here for visibility
   ("C-x a '" . abbrev-prefix-mark)   ; The =M-'= is overwritten by `mp-common-insert-pair'
   ("C-x a u" . unexpand-abbrev)
   ("C-x a L" . list-abbrevs))
  :hook
  ((text-mode-hook prog-mode-hook git-commit-setup-hook) . abbrev-mode)
  :init
  (setq abbrev-file-name (expand-file-name "abbrevs" mp-emacs--cache-dir))
  (setq only-global-abbrevs nil)
  (setq abbrev-suggest t)
  (setq save-abbrevs 'silently)
  :config
  (require 'mp-abbrev-defs))

;;;; Dabbrev
;; Dynamic "abbrevs" based on buffer look-up.
(use-package dabbrev
  ;; Swap M-/ and C-M-/
  :bind
  ;; `hippie-expand' could be used but it's fragile.  See
  ;; <https://github.com/minad/cape/issues/42>
  ("M-/" . dabbrev-expand)
  ("C-M-/" . dabbrev-completion)
  :init
  (setq dabbrev-ignored-buffer-regexps '("\\.\\(?:pdf\\|jpe?g\\|png\\)\\'")
        dabbrev-abbrev-char-regexp nil ; "\\sw\\|\\s_"
        dabbrev-abbrev-skip-leading-regexp "[$*/=~']" ; Ignore $ and org-markup symbols
        dabbrev-backward-only nil
        dabbrev-case-fold-search 'case-fold-search
        dabbrev-case-distinction 'case-replace
        dabbrev-case-replace 'case-replace
        dabbrev-check-other-buffers t
        dabbrev-eliminate-newlines t
        dabbrev-upcase-means-case-search t))

;;;; Cape <https://github.com/minad/cape>
;; Extra `completion-at-point' functions (/capfs/)
(use-package cape
  :ensure t
  :bind
  (("C-c p p" . completion-at-point) ;; capf
   ("C-c p t" . complete-tag)        ;; etags
   ("C-c p d" . cape-dabbrev)        ;; or dabbrev-completion
   ("C-c p h" . cape-history)
   ("C-c p f" . cape-file)
   ("C-c p k" . cape-keyword)
   ("C-c p s" . cape-elisp-symbol)
   ("C-c p e" . cape-elisp-block)
   ("C-c p a" . cape-abbrev)
   ("C-c p l" . cape-line)
   ("C-c p w" . cape-dict)
   ("C-c p :" . cape-emoji)
   ("C-c p \\" . cape-tex))
  :init
  ;; Add `completion-at-point-functions', used by `completion-at-point'.
  (add-to-list 'completion-at-point-functions #'dabbrev-completion)
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-symbol))

;;;; Extra expansions with `tempel’
(use-package tempel
  :ensure t
  :custom
  (tempel-trigger-prefix "<")           ; require trigger prefix
  :bind (("C-c p >" . tempel-complete) ;; Alternative tempel-expand
         ("C-c p <" . tempel-insert))
  :init
  (setq tempel-path (expand-file-name "templates" user-emacs-directory))

  ;; Setup completion at point
  (defun tempel-setup-capf ()
    ;; Add the Tempel Capf to `completion-at-point-functions'.
    ;; `tempel-expand' only triggers on exact matches. Alternatively use
    ;; `tempel-complete' if you want to see all matches, but then you
    ;; should also configure `tempel-trigger-prefix', such that Tempel
    ;; does not trigger too often when you don't expect it. NOTE: We add
    ;; `tempel-expand' *before* the main programming mode Capf, such
    ;; that it will be tried first.
    (setq-local completion-at-point-functions
                (cons #'tempel-expand
                      completion-at-point-functions)))

  (add-hook 'conf-mode-hook 'tempel-setup-capf)
  (add-hook 'prog-mode-hook 'tempel-setup-capf)
  (add-hook 'text-mode-hook 'tempel-setup-capf))

;; Optional: Add tempel-collection.
;; The package is young and doesn't have comprehensive coverage.
(use-package tempel-collection
  :ensure t
  :after tempel)

;;;; Minibuffer completion built-in settings
;; Preferences for built-in features.  See the `orderless' and
;; `vertico' sections below for redefinition of completion styles and
;; minibuffer UI/UX.
(use-package minibuffer
  :bind
  (("M-g M-m" . mp-minibuffer-toggle-select)
   :map minibuffer-mode-map
   ("C-r" . minibuffer-complete-history)
   :map minibuffer-local-completion-map
   ("SPC" . nil)
   ("?" . nil))
  :init
  (defun mp-minibuffer-toggle-select ()
    "Go back and forth between the minibuffer and the other window."
    (interactive)
    (if (window-minibuffer-p (selected-window))
        (select-window (minibuffer-selected-window))
      (select-window (active-minibuffer-window))))
  :config
  (setq completions-format 'horizontal  ; Or `one-column'
        completions-max-height 20
        completion-auto-select 'second-tab ; Faster *Completions* selection
        completion-auto-help 'visible      ; Keep *Completions* visible
        completions-header-format nil      ; Hide extra info
        completion-show-help nil           ; ""
        completions-detailed t)            ; Show doc and keybindings

  (setq completion-styles
        ;; Added `substring' for matching foo with \*foo*\ instead of
        ;; \foo*\ only
        '(basic partial-completion substring emacs22))

  (setq completion-category-overrides   ; These are _merged_ with `completion-styles'
        '((file (cycle . 2))                 ; Show when 3+
          (project-file
           ;; using flex since a project generally has a restricted
           ;; number of files
           (styles basic partial-completion flex)
           (cycle . 2))))

  (setq completion-ignore-case t
        read-buffer-completion-ignore-case t
        read-file-name-completion-ignore-case t)

  ;; Hide commands (`M-x') which do not work in the current mode.
  (setq read-extended-command-predicate
        #'command-completion-default-include-p)

  (setq enable-recursive-minibuffers t)
  (setq resize-mini-windows t)
  (setq minibuffer-eldef-shorten-default t) ; See Emacs manual sec. 8.1
  (setq read-answer-short t)

  ;; Do not allow the cursor to move inside the minibuffer prompt.  I
  ;; got this from the documentation of Daniel Mendler's Vertico
  ;; package: <https://github.com/minad/vertico>.
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))

  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Add prompt indicator to `completing-read-multiple'.  We display
  ;; [CRM<separator>], e.g., [CRM,] if the separator is a comma.  This
  ;; is a modified version from the README of the `vertico' package.
  (defun crm-indicator (args)
    (cons (format "[%s %s] %s"
                  (propertize "CRM" 'face 'error)
                  (propertize
                   (replace-regexp-in-string
                    "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                    crm-separator)
                   'face 'success)
                  (car args))
          (cdr args)))

  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; (file-name-shadow-mode)
  (minibuffer-depth-indicate-mode)
  (minibuffer-electric-default-mode))

;;;; Minibuffer history
(use-package savehist
  :init
  (setq history-length 10000
        history-delete-duplicates t
        savehist-save-minibuffer-history t
        savehist-file (expand-file-name
                       "history" mp-emacs--cache-dir))
  :hook
  (after-init-hook savehist-mode))

;;;; Orderless completion style
(use-package orderless
  :ensure t
  :demand t                             ; Immediately require
  :config
  (orderless-define-completion-style orderless+initialism
    "Style matching '\foo\\' to '\<f.*<o.*o.*\\', '\foo\\', or
 'regexp\(foo\)'."
    (orderless-matching-styles '(orderless-initialism
                                 orderless-literal
                                 orderless-regexp)))

  (orderless-define-completion-style orderless+prefixes
    "Style matching '\foo-bar\\' to '\foo.*-bar.*\\', '\foo-bar\\', or
 'regexp\(foo-bar\)'."
    (orderless-matching-styles '(orderless-prefixes
                                 orderless-literal
                                 orderless-regexp
                                 )))

  (setq orderless-style-dispatchers
        '(mp-orderless--without-if-bang
          mp-orderless--literal
          mp-orderless--file-ext))

  (defun mp-orderless--without-if-bang (pattern _index _total)
    (cond
     ((equal "!" pattern)
      '(orderless-literal . ""))
     ((string-prefix-p "!" pattern)
      `(orderless-without-literal . ,(substring pattern 1)))))

  (defun mp-orderless--literal (word _index _total)
    "Read WORD= as a literal string."
    (when (string-suffix-p "=" word)
      ;; The `orderless-literal' is how this should be treated by
      ;; orderless.  The `substring' form omits the `=' from the
      ;; pattern.
      `(orderless-literal . ,(substring word 0 -1))))

  (defun mp-orderless--file-ext (word _index _total)
    "Expand WORD. to a file suffix when completing file names."
    (when (and minibuffer-completing-file-name
               (string-suffix-p "." word))
      `(orderless-regexp . ,(format "\\.%s\\'" (substring word 0 -1)))))

  (setq orderless-component-separator
        #'orderless-escapable-split-on-space)

  ;; See the
  ;; [[../../resources/20230307T143856--known-completion-categories.txt]]
  ;; for categories to use in the `completion-category-overrides'
  (setq completion-styles '(basic orderless+prefixes)
        completion-category-overrides
        '((file (cycle . 2))
          (project-file (cycle . 2))
          (buffer (styles basic orderless+initialism))
          (imenu (styles orderless+initialism))
          (command (styles orderless+initialism))
          (symbol (styles orderless+initialism))
          (variable (styles orderless+initialism)))))

;;;; Minibuffer annotations with Marginalia
(use-package marginalia
  :ensure t
  :bind
  (:map minibuffer-local-map ("M-A" . marginalia-cycle))
  :init
  (setq marginalia-max-relative-age 259200) ; 3 days instead of two weeks
  (marginalia-mode))

;;;; Minibuffer UI/UX with `vertico'
;; See the vertico manual: <https://github.com/minad/vertico> Vertico
;; offers a UI for completion, with enhanced user experience (e.g.,
;; candidates grouping).
(use-package vertico
  :ensure t
  :bind
  (:map vertico-map
        ("M-," . vertico-quick-insert)  ; from the `vertico-quick' extension
        ("M-." . vertico-quick-exit)
        ("RET" . vertico-directory-enter) ; from the `vertico-directory' extension
        ("DEL" . vertico-directory-delete-char)
        ("M-DEL" . vertico-directory-delete-word)
        ("C-M-f" . vertico-next-group)
        ("C-M-b" . vertico-previous-group)
        ("M-V" . vertico-multiform-vertical) ; from the `vertico-multiform' extension
        ("M-G" . vertico-multiform-grid)
        ("M-F" . vertico-multiform-flat)
        ("M-R" . vertico-multiform-reverse)
        ("M-U". vertico-multiform-unobtrusive))
  :hook
  ;; Clean mini buffer with path change
  (rfn-eshadow-update-overlay-hook . vertico-directory-tidy)
  (minibuffer-setup-hook . vertico-repeat-save)
  :init
  (setq vertico-scroll-margin 1
        vertico-count 8
        vertico-resize t
        vertico-cycle nil)

  ;; FIXME! currenty this does nothing
  ;; (defvar mp-vertico--transform-functions nil)

  ;; (cl-defmethod mp-vertico--format-candidate :around
  ;;   (cand prefix suffix index start &context ((not mp-vertico--transform-functions) null))
  ;;   (dolist (fun (ensure-list mp-vertico--transform-functions))
  ;;     (setq cand (funcall fun cand)))
  ;;   (cl-call-next-method cand prefix suffix index start))

  ;; (defun mp-vertico--highlight-directory (file)
  ;;   "Highlight FILE if it ends with a slash."
  ;;   (if (string-suffix-p "/" file)
  ;;       (propertize file 'face 'bold)
  ;;     file))

  (defun mp-vertico--sort-directories-first (files)
    (setq files (vertico-sort-history-length-alpha files))
    (nconc (seq-filter (lambda (x) (string-suffix-p "/" x)) files)
           (seq-remove (lambda (x) (string-suffix-p "/" x)) files)))

  (setq vertico-multiform-commands
        '(("dired"
           (vertico-sort-function . mp-vertico--sort-directories-first))
          ))

  ;; (setq vertico-multiform-categories
  ;;       '((file
  ;;          (mp-vertico--transform-functions . mp-vertico--highlight-directory))))

  (vertico-mode)
  (vertico-multiform-mode))

;;;; Enhancing `completing-read'-based functions with `consult'
(use-package consult
  :ensure t
  :bind ;; Many keybindings here, brace yourself
  (([remap bookmark-jump] . consult-bookmark) ; =C-x r b=
   ("M-g g" . consult-goto-line)              ; remap `goto-line'
   ("M-g l" . consult-line)
   ("M-g i" . consult-imenu)                  ; remap `imenu'
   ("M-g I" . consult-imenu-multi)
   ("M-g o" . consult-outline)
   ("M-g m" . consult-mark)
   ("M-g M" . consult-global-mark)
   ("M-g f" . consult-flymake)
   ("M-g C-a" . consult-org-agenda)

   ([remap Info-search] . consult-info)
   ("C-h C-m" . consult-man)
   ("C-h RET" . consult-man)

   ("M-s d" . consult-locate)
   ("M-s f" . consult-find)
   ("M-s e" . consult-isearch-history)
   ("M-s g" . consult-ripgrep)
   ("M-s i" . consult-info)
   ("M-s l" . consult-line)
   ("M-s L" . consult-line-multi)

   ("C-x b" . consult-buffer)           ; remap `switch-to-buffer'
   ("C-x 4 b" . consult-buffer-other-window) ; remap `switch-to-buffer-other-window'
   ("C-x 5 b" . consult-buffer-other-frame)  ; remap `switch-to-buffer-other-frame'
   ("C-x p b" . consult-project-buffer)
   ("M-y" . consult-yank-pop)                ; remap `yank-pop'
   ("C-x M-:" . consult-complex-command)
   ("C-x M-m" . consult-minor-mode-menu)
   ("C-x M-k" . consult-kmacro)
   ("C-x r r" . consult-register) ; Use the register's prefix
   ("C-x r s" . consult-register-store)
   ("C-c h" . consult-history)
   ("C-c i" . consult-info)

   :map isearch-mode-map
   ("M-e" . consult-isearch-history)    ; orig. isearch-edit-string
   ("M-s e" . consult-isearch-history)  ; orig. isearch-edit-string
   ("M-s l" . consult-line) ; needed by consult-line to detect isearch
   ("M-s L" . consult-line-multi) ; needed by consult-line to detect isearch
   ;; Minibuffer history
   :map minibuffer-local-map
   ("M-s" . consult-history) ; orig. next-matching-history-element
   ("M-r" . consult-history) ; orig. previous-matching-history-element
   :map consult-narrow-map
   ("?" . consult-narrow-help)
   :map vertico-map
   ("M-h" . consult-history))
  :init
  (with-eval-after-load 'org-mode
    (define-key org-mode-map (kbd "M-g o") #'consult-org-heading))

  (setq consult-narrow-key "<")

  ;; Faster register preview
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)
  (advice-add #'register-preview :override #'consult-register-window)

  (setq consult-async-min-input 2)      ; Reduced from 3

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)
  :config
  ;; Search through bundled manuals
  (defun consult-info-emacs ()
    "Search through Emacs info pages."
    (interactive)
    (consult-info "emacs" "efaq" "elisp" "cl" "compat"))

  (defun consult-info-org ()
    "Search through the Org info page."
    (interactive)
    (consult-info "org"))

  ;; Add org-mode buffer as specific source for `consult-buffer'
  (defvar org-source
    (list :name     "Org Buffer"
          :category 'buffer
          :narrow   ?o
          :face     'consult-buffer
          :history  'buffer-name-history
          :state    #'consult--buffer-state
          :new
          (lambda (name)
            (with-current-buffer (get-buffer-create name)
              (insert "#+title: " name "\n\n")
              (org-mode)
              (consult--buffer-action (current-buffer))))
          :items
          (lambda ()
            (mapcar #'buffer-name
                    (seq-filter
                     (lambda (x)
                       (eq (buffer-local-value 'major-mode x) 'org-mode))
                     (buffer-list))))))

  (add-to-list 'consult-buffer-sources 'org-source 'append)

  ;; Disable automatic preview for certain consult comands
  (consult-customize
   consult-theme :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-xref
   consult-buffer consult-bookmark consult-recent-file
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   :preview-key "C-S-p")                ; overwrites `backward-list'

  ;; Added `imenu' categories for elisp
  (setq consult-imenu-config
        '((emacs-lisp-mode
           :toplevel "Functions"
           :types ((?f "Functions" font-lock-function-name-face)
                   (?m "Macros"    font-lock-keyword-face)
                   (?p "Packages"  font-lock-constant-face)
                   (?t "Types"     font-lock-type-face)
                   (?v "Variables" font-lock-variable-name-face))))))

;;;; Switch directory quickly with `consult-dir'
(use-package consult-dir
  :ensure t
  :config
  (add-to-list 'consult-dir-sources 'consult-dir--source-tramp-ssh t)
  :bind
  (([remap list-directory] . consult-dir)
   (:map vertico-map
         ("C-x C-d" . consult-dir)
         ("C-x C-j" . consult-dir-jump-file))))

;;;; Completion-based actions with `embark'
(use-package embark
  :ensure t
  :bind
  ([remap describe-bindings] . embark-bindings) ; Describe bindings with `completing-read'
  ("C-;" . embark-act)
  (:map vertico-map
        ("C-;" . embark-act)
        ("C-c C-e" . embark-export)
        ("C-c C-;" . embark-become))
  (:map embark-general-map
        ("G" . mp-embark-online-search))
  (:map embark-region-map
        ("r" . repunctuate-sentences)        ; overrides `rot13-region'
        ("s" . sort-lines)
        ("u" . untabify))
  (:map embark-file-map
        ("S" . mp-sudo-find-file)
        ("g" . mp-embark-magit-status))
  (:map embark-symbol-map
        ("." . embark-find-definition)
        ("k" . describe-keymap))
  (:map embark-variable-map
        ("!" . embark-act-with-eval))
  (:map embark-expression-map
        ("!" . embark-act-with-eval))
  (:map embark-become-file+buffer-map
        ("B" . project-switch-to-buffer)
        ("F" . project-find-file))
  (:map mp-embark-become-line-map
        ("l" . consult-line)
        ("i" . consult-imenu)
        ("s" . consult-outline))
  :init
  (setq prefix-help-command #'embark-prefix-help-command) ; replaces `which-key'
  (setq embark-confirm-act-all nil)

  ;; Keep track of it with the `display-buffer-alist' variable (see
  ;; the mp-ux-window.el file)
  (setq embark-verbose-indicator-display-action nil)
  ;; Simplify the indicators
  (setq embark-indicators '(embark-mixed-indicator
                            embark-highlight-indicator))
  (setq embark-verbose-indicator-excluded-actions
        '("\\`customize-" "\\(local\\|global\\)-set-key"
          set-variable embark-cycle embark-keymap-help
          embark-isearch))
  (setq embark-verbose-indicator-buffer-sections
        `(target "\n" shadowed-targets " " cycle "\n" bindings))

  (setq embark-mixed-indicator-both nil)
  (setq embark-mixed-indicator-delay 1.0)
  :config
  (defun mp-embark-online-search (term)
    (interactive "sSearch term: ")
    (browse-url
     (format "https://www.startpage.com/sp/search?q=%s" term)))

  (defun mp-sudo-find-file (file)
    "Open FILE as root."
    (interactive "FOpen file as root: ")
    (when (file-writable-p file)
      (user-error "File is user wrieable, aborting sudo"))
    (find-file (if (file-remote-p file)
                   (concat "/" (file-remote-p file 'method) ":"
                           (file-remote-p file 'user) "@" (file-remote-p file 'host)
                           "|sudo:root@"
                           (file-remote-p file 'host) ":"
                           (file-remote-p file 'localname))
                 (concat "/sudo:root@localhost:" file))))

  (defun mp-embark-magit-status (file)
    "Run `magit-status` on repo containing the embark target."
    (interactive "GFile: ")
    (magit-status (locate-dominating-file file ".git")))

  (defun my/consult-outline-narrow-heading (heading)
    "Narrow to and expand HEADING."
    (embark-consult-goto-location heading)
    (outline-mark-subtree)
    (and
     (use-region-p)
     (narrow-to-region (region-beginning) (region-end))
     (deactivate-mark))
    (outline-show-subtree))

  (defvar-keymap embark-consult-outline-map
    :doc "Keymap for embark actions in `consult-outline'."
    "r" #'my/consult-outline-narrow-heading)

  (defun with-embark-consult-outline-map (fn &rest args)
    "Let-bind `embark-keymap-alist' to include `consult-location'."
    (let ((embark-keymap-alist
           (cons '(consult-location . embark-consult-outline-map) embark-keymap-alist)))
      (apply fn args)))

  (advice-add 'consult-outline :around #'with-embark-consult-outline-map)

  (defun embark-act-with-eval (expression)
    "Evaluate EXPRESSION and call `embark-act' on the result."
    (interactive "sExpression: ")
    (with-temp-buffer
      (insert (eval (read expression)))
      (embark-act)))

  (defvar mp-embark-become-line-map (make-sparse-keymap)
    "Line-specific custom `embark-become' keymap.

Add this to `embark-become-keymaps'.")

  (defvar mp-embark-become-general-map
    (make-sparse-keymap)
    "General custom `embark-become' keymap.

Add this to `embark-become-keymaps'.")

  (dolist (map (list 'mp-embark-become-line-map
                     'mp-embark-become-general-map))
    (add-to-list 'embark-become-keymaps map)))

;;;; Integration between Embark and Consult
(use-package embark-consult
  :ensure t
  :after (embark consult)
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(provide 'mp-ux-completion)
;;; mp-ux-completion.el ends here
