;;; mp-ux-windows.el --- Emacs windows and frames management  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marcop@p7560-f37>
;; Keywords: frames, windows

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(setq switch-to-buffer-obey-display-actions t)

(global-set-key (kbd "C-x 2") #'mp-common-split-window-below-prompt-buffer)
(global-set-key (kbd "C-x 3") #'mp-common-split-window-right-prompt-buffer)
(global-set-key (kbd "C-x w 2") #'mp-common-split-root-window-below-prompt-buffer)
(global-set-key (kbd "C-x w 3") #'mp-common-split-root-window-right-prompt-buffer)
(global-set-key (kbd "C-x w 1") #'delete-other-windows-vertically)
(global-set-key (kbd "C-x w =") #'balance-windows)
(global-set-key (kbd "C-x w -") #'fit-window-to-buffer)
(global-set-key (kbd "C-x w t") #'tab-window-detach)
(global-set-key (kbd "C-x w s") #'window-toggle-side-windows)
(global-set-key (kbd "C-x w d") #'mp-window-toggle-window-dedication)

(defun mp-window-toggle-window-dedication ()
  "Toggles window dedication in the selected window."
  (interactive)
  (set-window-dedicated-p (selected-window)
                          (not (window-dedicated-p (selected-window)))))

;;;; Frames settings
;;;;; Set frame name to tab name, if any
(setq frame-title-format '((:eval
                            (format
                             "%s"
                             (or
                              (cdr (assoc 'name (tab-bar--current-tab)))
                              (buffer-name))))))

;;;; Windows settings
;;;;; Buffer display rules
(defun make-display-buffer-matcher-function (major-modes)
  (lambda (buffer-name action)
    (with-current-buffer buffer-name (apply #'derived-mode-p major-modes))))

(setq
 display-buffer-alist
 `(
;;;;;; Shells, terminals, and other REPLs
   ("\\(?:\\*e?shell\\|v?term\\:\\)"
    (display-buffer-reuse-window
     display-buffer-reuse-mode-window
     display-buffer-in-side-window)
    (inhibit-same-window . nil)
    (mode vterm-mode vterm-copy-mode eshell-mode)
    (side . bottom)
    (window-height . 12)
    (preserve-size . (nil . t)))
;;;;;; `org-capture' key selection in bottom side
   ("\\*Org Select\\*"
    (display-buffer-reuse-window
     display-buffer-in-side-window)
    (side . bottom)
    (slot . -1)
    (window-parameters
     ((mode-line-format . none))))
;;;;;; Calendars in bottom side
   ("\\*Calendar\\*"
    (display-buffer-reuse-window
     display-buffer-reuse-mode-window
     display-buffer-in-side-window)
    (side . bottom)
    (slot . 1)
    (window-parameters
     ((mode-line-format . none))))
;;;;;; Messages, compile logs, and backtraces
   ("\\`\\*\\(Warnings\\|Compile-Log\\)\\*\\'"
    (display-buffer-no-window)
    (allow-no-window . t))
   ("\\*.*\\(?:Messages\\|[Bb]acktrace\\).*"
    (display-buffer-reuse-window
     display-buffer-reuse-mode-window
     display-buffer-in-side-window)
    (side . top)
    (window-height 10)
    (dedicated . t))
;;;;;; Help and info windows
   ((or . ((derived-mode . Man-mode)
           (derived-mode . woman-mode)
           "\\(?:\\*\\(?:[Mm]an\\|[Ww]oman\\|[Hh]elp\\|[Ss]hortdoc\\|[Ii]nfo\\)\\)"))
    (display-buffer-reuse-window
     display-buffer-reuse-mode-window
     display-buffer-in-side-window)
    (side . left)
    (window-width . 80)
    (window-parameters
     (no-delete-other-windows . t)))
;;;;;; Buffers to hide: Async shell command
   ("\\(?:\\*Async Shell Command\\*\\)"
    (display-buffer-no-window))
   ))

;;;;; Obey rules even with manual buffer switching
(setq switch-to-buffer-obey-display-actions t)

;;;;; Use `visual-line-mode' for small windows
(add-hook 'help-mode-hook #'visual-line-mode)
(add-hook 'shortdoc-mode-hook #'visual-line-mode)
(add-hook 'Info-mode-hook #'visual-line-mode)

;;;;; Limit number of side windows
(setq window-sides-slots '(1 2 1 2))    ; left, top, right, bottom

;;;;; Pop new window when swithcing buffer from dedicated window
(setq switch-to-buffer-in-dedicated-window 'pop)

;;;;; Resize window combinations together
(setq window-combination-resize t)

;;;;; Even windows only height-wise
(setq even-window-sizes 'height-only)

;;;;; TODO! Quickly select root left window

;;;; Extra: Quickly move and act on windows with `ace-window`
(use-package ace-window
  :ensure t
  :bind
  ("M-o" . ace-window)
  :config
  (setq aw-keys  '(?a ?s ?d ?f ?g ?h ?j ?k ?l)))

;;;; Extra: Quickly move window around with `transpose-frames'
(use-package transpose-frame
  :ensure t
  :bind
  (("C-x w m" . flop-frame)
   ("C-x w c" . rotate-frame-clockwise)
   ))

;;;; Tabs settings
;; Tabs can be seen as windows configuration storage devices
(use-package tab-bar
;;;;; Bindings
  :bind
  (("C-x w t" . toggle-frame-tab-bar)
   ("C-x w r" . tab-bar-history-forward)
   ("C-x w u" . tab-bar-history-back)
   ("C-x t n" . tab-bar-new-tab)
   ("C-x t k" . tab-bar-close-tab)
   )
;;;;; Hooks
  :hook
  (after-init-hook . tab-bar-mode)
  (tab-bar-mode-hook . tab-bar-history-mode)
;;;;; Options
  :config
  (setopt tab-bar-show 1
          tab-bar-close-button-show nil
          tab-bar-new-button-show nil
          ))

;;;; Provide file
(provide 'mp-ux-windows)
;;; mp-ux-windows.el ends here
