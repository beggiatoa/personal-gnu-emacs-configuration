;;; mp-app-mu4e.el --- Managing e-mails in Emacs     -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marcop@p7560-f37>
;; Keywords: mail

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; REQUIREMENTS
;;
;; - davmail
;; - msmtp
;; - isync
;; - mu
;;
;; Install them with `sudo dnf install isync msmtp mail-utils' and `flatpak install davmail'

;;; Code:
;;;; Check requirements
(defconst mp-mail--requirements-p nil
  "t if the system requirements for the `mp-mail' module are met.")

(defun mp-email//requirement-check ()
  "Warn if the system requirements are not met."
  (let* ((isync-version
          (nth 1
               (split-string
                (shell-command-to-string
                 "mbsync --version 2>/dev/null"))))
         (isyncp (version<= "1.4" isync-version))
         (mu-version
          (nth 1
               (split-string
                (shell-command-to-string
                 "mu --version 2>/dev/null"))))
         (mup (version<= "1.8" mu-version))
         (davmailp (not (string-empty-p
                    (shell-command-to-string
                     "which davmail 2>/dev/null")))))
    (cond
     ((not isyncp)
      (setq mp-mail--requirements-p nil)
      (display-warning
       'mp-mail
       "Please install ISync version 1.4 or above."))
     ((not mup)
      (setq mp-mail--requirements-p nil)
      (display-warning
       'mp-mail--requirements-p
       "Please install mail-utils version 1.8 or above."))
     ((not davmailp)
      (setq mp-mail--requirements-p nil)
      (display-warning
       'mp-mail
       "Please install davmail version 6.0 or above."))
     ((and isyncp mup davmailp)
      (setq mp-mail--requirements-p t)))))

;; (mp-email//requirement-check)

;;;; Message composition settings
(use-package message
  ;; :if mp-mail--requirements-p
  :init
  (setq message-mail-user-agent t)
  (setq mail-signature "Marco Prevedello
PhD candidate
———
m.prevedello1@universityofgalway.ie
+353 852566133
Room 105, Orbsen Building.
School of Biological and Chemical Sciences
University of Galway")
  (setq message-signature "Marco Prevedello\n")
  (setq message-citation-line-format "On %Y-%m-%d %a, %R %z, %f wrote:\n")
  (setq message-citation-line-function 'message-insert-formatted-citation-line)
  (setq message-confirm-send t)
  (setq message-kill-buffer-on-exit t)
  (setq message-wide-reply-confirm-recipients t)
  (when (fboundp 'imagemagick-register-types)
    (imagemagick-register-types))
  :config
  (add-to-list 'mm-body-charset-encoding-alist '(utf-8 . base64))
  :hook
  (message-setup-hook . message-sort-headers))

;;;; Sending emails with MSMTP
(use-package sendmail
  :init
  (setq send-mail-function 'sendmail-send-it)
  (setq sendmail-program (executable-find "msmtp"))
  (setq mail-specify-envelope-from t)
  (setq message-sendmail-envelope-from 'header)
  (setq mail-envelope-from 'header)
  (setq smtpmail-debug-info t))
;; (setq smtpmail-smtp-server "localhost")
;; (setq smtpmail-smtp-service 5003))

;;;; mu4e configuration
(use-package mu4e
  ;; :if mp-mail--requirements-p
  :load-path "/usr/share/emacs/site-lisp/mu4e"
  :bind
  (("C-x m" . mu4e)
   :map mu4e-view-mode-map
   ("C-c RET s" . mu4e-view-save-attachments))
  :hook
  ((mu4e-view-mode-hook . visual-line-mode)
   (mu4e-compose-mode-hook . flyspell-mode)
   (dired-mode-hook . turn-on-gnus-dired-mode)
   (mu4e-compose-mode-hook . mp-message-add-cc-bcc))
  :init
  (require 'mu4e)
  (require 'mu4e-icalendar)
  (mu4e-icalendar-setup)
  (setq mail-user-agent 'mu4e-user-agent) ; Use mu4e

  (defun mp-message-add-cc-bcc ()
    "My Function to automatically add Cc & Bcc: headers.
This is in the mu4e compose mode."
    (save-excursion (message-add-header "Cc:\n"))
    (save-excursion (message-add-header "Bcc:\n")))
  :config
  (setopt mu4e-get-mail-command "mbsync o365-uog-quick"
          mu4e-update-interval 1800 ; 30 min
          mu4e-display-update-status-in-modeline t
          mu4e-confirm-quit nil
          mu4e-change-filenames-when-moving t ; Optimized for mbsync
          mu4e-completing-read-function 'completing-read
          mu4e-index-update-error-warning t
          mu4e-index-update-error-continue t
          mu4e-index-cleanup t ; I have a fast machine
          mu4e-index-lazy-check t
          ;; Use long lines to ensure sent email legibility on modern
          ;; devices
          mu4e-compose-format-flowed t
          fill-flowed-encode-column 998
          mu4e-compose-complete-addresses t
          mu4e-compose-context-policy 'ask-if-none
          mu4e-compose-dont-reply-to-self t
          mu4e-compose-keep-self-cc nil
          mu4e-compose-signature mail-signature
          mu4e-headers-auto-update t
          mu4e-headers-include-related nil
          mu4e-attachment-dir "~/0-Inbox"
          mu4e-sent-messages-behavior 'sent
          mu4e-context-policy 'pick-first
          mu4e-maildir-shortcuts '(("/o365-uog/INBOX" . ?i)
                                   ("/o365-uog/Archive" . ?a)
                                   ("/o365-uog/Drafts" . ?d)
                                   ("/o365-uog/Sent" . ?s)))

  (setopt mu4e-contexts
          `(,(make-mu4e-context
              :name "o365-uog"
              :enter-func (lambda () (mu4e-message "Entering UoG context"))
              :leave-func (lambda () (mu4e-message "Leaving UoG context"))
              :match-func (lambda (msg)
                            (when msg
                              (mu4e-message-contact-field-matches
                               msg
                               :to "m.prevedello1@universityofgalway.ie")))
              :vars '((user-mail-address . "m.prevedello1@universityofgalway.ie")
                      (user-full-name . "Marco Prevedello")
                      (message-user-organization . "University of Galway")
                      (mu4e-compose-signature . (concat
                                                 "Marco Prevedello\n"
                                                 "PhD candidate @ Abram's lab\n"
                                                 "———\n"
                                                 "m.prevedello1@universityofgalway.ie\n"
                                                 "+353 (0) 852566133\n"
                                                 "Room 105, Orbsen Building\n"
                                                 "School of Biological and Chemical Sciences\n"
                                                 "UoG of Galway"))
                      (mu4e-maildir-shortcuts . (("/o365-uog/INBOX" . ?i)
                                                 ("/o365-uog/Archive" . ?a)
                                                 ("/o365-uog/Drafts" . ?d)
                                                 ("/o365-uog/Sent" . ?s)))
                      (mu4e-drafts-folder . "/o365-uog/Drafts")
                      (mu4e-refile-folder . "/o365-uog/Archive")
                      (mu4e-sent-folder . "/o365-uog/Sent")
                      (mu4e-trash-folder . "/o365-uog/Trash"))
              )
            ,(make-mu4e-context
              :name "Outlook (ITA)"
              :enter-func (lambda () (mu4e-message "Entering Outlook (ITA) context"))
              :leave-func (lambda () (mu4e-message "Leaving Outlook (ITA) context"))
              :match-func (lambda (msg)
                            (when msg
                              (mu4e-message-contact-field-matches
                               msg
                               :to "marco.prevedello@outlook.it")))
              :vars '((user-mail-address . "marco.prevedello@outlook.it")
                      (user-full-name . "Marco Prevedello")
                      (message-user-organization . "")
                      (mu4e-compose-signature . "")
                      (mu4e-maildir-shortcuts . (("/Outlook/Inbox" . ?i)
                                                 ("/Outlook/Archive" . ?a)
                                                 ("/Outlook/Drafts" . ?d)
                                                 ("/Outlook/Sent" . ?s)))
                      (mu4e-drafts-folder . "/Outlook/Drafts")
                      (mu4e-refile-folder . "/Outlook/Archive")
                      (mu4e-sent-folder . "/Outlook/Sent")
                      (mu4e-trash-folder . "/Outlook/Bin"))
              )
            ,(make-mu4e-context
              :name "gmail (spam)"
              :enter-func (lambda () (mu4e-message "Entering Gmail (spam) context"))
              :leave-func (lambda () (mu4e-message "Leaving Gmail (spam) context"))
              :match-func (lambda (msg)
                            (when msg
                              (mu4e-message-contact-field-matches
                               msg
                               :to "marco.preve@gmail.com")))
              :vars '((user-mail-address . "marco.preve@gmail.com")
                      (user-full-name . "Marco")
                      (message-user-organization . "")
                      (mu4e-compose-signature . "")
                      (mu4e-maildir-shortcuts . (("/gmail/All" . ?i)
                                                 ("/gmail/Sent" . ?s)))
                      (mu4e-sent-folder . "/gmail/Sent")
                      (mu4e-trash-folder . "/gmail/Bin"))))))

;;;; Extra: mu4e thread folding
(unless (package-installed-p 'mu4e-folding)
  (package-vc-install "https://www.github.com/rougier/mu4e-folding"))

(use-package mu4e-folding
  :bind
  (:map mu4e-headers-mode-map
        ("TAB" . mu4e-folding-toggle-at-point)
        ("<backtab>" . mu4e-folding-toggle-all))
  :config
  (set-face-attribute 'mu4e-folding-root-folded-face nil :box t))

;;;; HTML emails with `org-msg'
(use-package org-msg
  :ensure t
  :after htmlize
  :bind
  ((:map mu4e-main-mode-map
         ("C-c h" . org-msg-mode))
   (:map mu4e-headers-mode-map
         ("C-c h" . org-msg-mode))
   (:map mu4e-view-mode-map
         ("C-c h" . org-msg-mode))
   )
  :custom
  (org-msg-convert-citation t)
  (org-msg-greeting-fmt "Dear %s,\n\n")
  (org-msg-signature
   "Best Regards,

#+begin_signature
--
Marco Prevedello
PhD candidate
———
m.prevedello1@universityofgalway.ie
+353 (0) 852566133
Room 105, Orbsen Building
School of Biological and Chemical Sciences
University of Galway
#+end_signature")
  (org-msg-default-alternatives
   '((new . (text html))
     (reply-to-html . (text html))
     (reply-to-text . (text))))
  )

(use-package htmlize
  :ensure t)

;;;; Security settings
(use-package auth-source
  :config
  (setopt auth-sources '("~/.authinfo.gpg")
          user-full-name "Marco Prevedello"
          user-mail-address "marco.prevedello@outlook.it"))

;;;; Provide mu4e
(provide 'mp-app-mu4e)

;;; mp-app-mu4e.el ends here
