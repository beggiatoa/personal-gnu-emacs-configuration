;;; mp-prog-latex.el --- Editing of LaTeX (& co.) files   -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello(use-package  <marco.prevedello@outlook.it>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
;;;; AucTeX
(use-package tex
  :ensure auctex
  :bind
  (:map TeX-mode-map
   ("C-c _" . nil))                     ; Reserved for `powerthesaurus-transient'
  :hook
  ((LaTeX-mode-hook . turn-on-reftex)       ; RefTeX
   (LaTeX-mode-hook . turn-on-flyspell)     ; Flyspell
   (LaTeX-mode-hook . LaTeX-math-mode)       ; easy to type greek letters
   (LaTeX-mode-hook . reftex-isearch-minor-mode) ; ISearch multi-file TeX
   (TeX-after-compilation-finished-functions . TeX-revert-document-buffer))
  :config
  (setopt TeX-parse-self t ; parse on load
          TeX-auto-save t  ; parse on save
          ;; Use hidden directories for AUCTeX files.
          TeX-auto-local ".auctex-auto"
          TeX-style-local ".auctex-style"
          TeX-source-correlate-mode t     ; Link cursor pos. to output
          TeX-source-correlate-method 'synctex ; I only use PDFs
          ;; Ask to starting the Emacs server when correlating sources.
          TeX-source-correlate-start-server 'ask
          ;; Automatically insert braces after sub/superscript in
          ;; `LaTeX-math-mode'.
          TeX-electric-sub-and-superscript t
          TeX-electric-math '("\(" . "\)")
          TeX-refuse-unmatched-dollar t
          TeX-electric-escape t
          LaTeX-electric-left-right-brace t
          ;; Just save, don't ask before each compilation.
          TeX-save-query nil
          font-latex-fontify-script nil)

  ;; Ask for master file
  (setq-default TeX-master nil)

  ;; Use pdf-tools to open PDF files
  (setopt TeX-view-program-selection
          '(((output-dvi has-no-display-manager)
             "dvi2tty")
            ((output-dvi style-pstricks)
             "dvips and gv")
            (output-dvi "xdvi")
            (output-pdf "PDF Tools")
            (output-html "xdg-open")))

  ;; Toggle pdflatex shell escape.  Useful for syntax highlighting
  ;; with `minted'
  (define-minor-mode TeX-shell-escape-mode
    "Minor mode for runs of pdfLaTeX with \"-shell-escape\"."
    :init-value nil :lighter nil :keymap nil
    :group 'TeX-command
    (setq TeX-command-extra-options
          (if (string-empty-p  TeX-command-extra-options)
              "-shell-escape"
            "")))

  (defalias 'tex-shell-escape-mode #'TeX-shell-escape-mode)
  (add-to-list 'minor-mode-alist '(TeX-shell-escape-mode ""))

  (define-key TeX-mode-map (kbd "C-c C-t C-e") #'TeX-shell-escape-mode))

;;;;; TeX-Fold for beautifying and reading TeX code
(use-package tex-fold
  :hook
  (TeX-fold-mode-hook . prettify-symbols-mode) ; for greek letters and other math symbols
  :config
  ;; add tables and figure to fold
  (add-to-list 'TeX-fold-env-spec-list '("[table]" ("table")))
  (add-to-list 'TeX-fold-env-spec-list '("[figure]" ("figure")))
  (add-to-list 'TeX-fold-macro-spec-list '("[r]" ("autoref")))
  (add-to-list 'TeX-fold-macro-spec-list '("{1}{2}" ("qty")))
  (add-to-list 'TeX-fold-macro-spec-list '("{1}-{2}{3}" ("qtyrange")))
  (add-to-list 'TeX-fold-macro-spec-list '(1 ("num" "gls" "Gls")))
  (add-to-list 'TeX-fold-macro-spec-list '("{1}s" ("glspl" "Glspl"))))

;;;; CdLaTeX for fast math input

(use-package cdlatex
  :ensure t
  :hook
  ((LaTeX-mode-hook . turn-on-cdlatex)))

;;;; LaTeX-Mk (disabled)
(use-package auctex-latexmk
  :ensure t
  :disabled t
  :config
  (auctex-latexmk-setup))

;;;; Provide file

(provide 'mp-prog-latex)

;;; mp-app-csv.el ends here
