;;; mp-app-gptel.el --- LLMs integration in Emacs  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Marco Prevedello

;; Author: Marco Prevedello <marco.prevedello@outlook.it>
;; Keywords: tools, extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; LLMs integration in Emacs with GPTel
;; <https://github.com/karthink/gptel>.

;;; Code:

(use-package gptel
  :ensure t
  :bind
  (("H-s-g" . gptel-menu)
   ("C-c <f1>" . gptel-menu))
  :config
  (setopt gptel-default-mode 'markdown-mode
          gptel-track-response t
          gptel-use-header-line t)

  (require 'gptel-anthropic)

  (setopt gptel-model 'claude-3-5-sonnet-20241022
          gptel-backend
          (gptel-make-anthropic "Claude 3.5"
            :key (gptel-api-key-from-auth-source
                  "api.anthropic.com" "apikey")
            :stream t)))

(unless (package-installed-p 'gptel-quick)
  (package-vc-install "https://github.com/karthink/gptel-quick"))

(use-package posframe :ensure t)

(use-package gptel-quick
  :ensure nil
  :after (gptel postframe)
  :config
  (with-eval-after-load 'embark
  (keymap-set embark-general-map "?" #'gptel-quick)))

(provide 'mp-app-gptel)
;;; mp-app-gptel.el ends here
