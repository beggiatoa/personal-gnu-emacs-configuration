;;; mp-org.el --- Extension library for `org-mode'   -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marco.prevedello@outlook.it>
;; Keywords: extensions, org

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
;;;; Customs
(defgroup mp-org nil
  "`mp-org' expands org-mode.")

(defcustom mp-org-inbox-file
  (expand-file-name "inbox.org" org-directory)
  "Org file to store quick captures.
See also `mp-org-inbox-clean-reminder'.")

(defcustom mp-org-scheduled-file
  (expand-file-name "scheduled.org" org-directory)
  "Org file to store _scheduled_ and repeated events.")

;;;; Faces

(defface mp-org-next '((default . (:inherit org-todo
                                         :slant italic
                                         :weight normal)))
  "Face for `org-todo-keyword' \"NEXT\"."
  :group 'org-faces)

(defface mp-org-started '((default . (:inherit org-todo
                                            :foreground "orange red")))
  "Face for `org-todo-keyword' \"STRT\"."
  :group 'org-faces)

(defface mp-org-wait '((default . (:inherit org-todo
                                         :weight light)))
  "Face for `org-todo-keyword' \"WAIT\"."
  :group 'org-faces)

(defface mp-org-stop '((default . (:inherit org-done
                                         :underline t)))
  "Face for `org-todo-keyword' \"STOP\"."
  :group 'org-faces)

(defface mp-org-cancelled '((default . (:inherit org-done
                                              :strike-through t)))
  "Face for `org-todo-keyword' \"CANC\"."
  :group 'org-faces)

(defface mp-org-delegated '((default . (:inherit org-done
                                              :slant italic
                                              :weight normal)))
  "Face for `org-todo-keyword' \"DELG\"."
  :group 'org-faces)

(defface mp-org-review '((default . (:inherit org-done
                                           :underline t)))
  "Face for `org-todo-keyword' \"REVW\"."
  :group 'org-faces)

(defface mp-org-project '((default . (:inherit org-todo
                                            :underline t)))
  "Face for `org-todo-keyword' \"PROJ\"."
  :group 'org-faces)

(defface mp-org-done-project '((default . (:inherit org-done)))
  "Face for `org-todo-keyword' \"DNPR\"."
  :group 'org-faces)

;;;; Functions
;;;;; Quick sort entries by TODO order
(defun mp-org-sort-entries-quick-todo ()
  "Call `org-sort-entries' sorting by TODO state"
  (interactive)
  (org-sort-entries nil ?o))

;;;;; Highlight current line in org-table
(defun mp-org-table--highlight-row ()
  (if (org-at-table-p)
      (hl-line-mode 1)
    (hl-line-mode -1)))

(defun mp-org-table--setup-table-highlighting ()
  (add-hook 'post-command-hook #'mp-org-table--highlight-row nil t))

;;;;; Export org-tables with `org-babel-tangle'
(defun mp-org-babel-tangle-table-at-point ()
  "Tangle Org table at point.
The table is ignored if it is not preceeded by a line like:
#+ATTR_TANGLE: DATAFILE
or
#+ATTR_TANGLE: (\"DATAFILE\" \"TYPE\")"
  (interactive)
  (unless (org-at-table-p)
    (user-error "Not at Org table"))
  (let* (type (data (org-element-context))
              (parent (org-element-property :parent data))
              file)
    (while parent
      (setq data parent)
      (setq parent (org-element-property :parent data)))
    (when (and (setq prop (org-element-property :attr_tangle data))
               (setq file (read prop)))
      (cond
       ((symbolp file)
        (setq file (symbol-name file)))
       ((stringp file)) ;; keep it that way
       ((consp file)
        (setq type (nth 1 file)
              file (car file)))
       (t
        (user-error "Unexpected format of table to be tangled")))
      (org-table-export file (or type org-table-export-default-format))
      t)))

(defun mp-org-babel-tangle-tables (fun &optional arg target-file lang)
  "Tangle Org tables in current buffer.
For around advice with `org-babel-tangle' as FUN.
See `org-babel-tangle' for the args ARG, TARGET-FILE and LANG.

Currently, only one table per file is possible."
  (cond
   ((equal arg '(4)) ;; at point
    (if (org-at-table-p)
    (mp-org-babel-tangle-table-at-point)
      (funcall fun arg target-file lang)))
   ((equal arg '(16))
    (funcall fun arg target-file lang))
   (t
    (funcall fun arg target-file lang)
    (let ((table-count 0))
      (org-with-wide-buffer
       (goto-char (point-min))
       (while (re-search-forward "^[ \t\r]*|" nil t)
     (goto-char (match-beginning 0))
     (when (org+-tangle-table-at-point)
       (cl-incf table-count))
     (goto-char (org-table-end)))
       )
      (message "Tangled %d tables from %s." table-count (buffer-name))))))

;;;;; Automatically set to "DONE" when all sub-tasks are completed
(defun mp-org--summary-todo (n-done n-not-done)
  "Switch entry to DONE when all subentries are done, to PROJ otherwise."
  (let (org-log-done org-log-states)   ; turn off logging
    (org-todo (if (= n-not-done 0) "DNPR" "PROJ"))))

;;;;; Personal `org-emphasize' for word or region
(defun mp-org-emphasize (&optional char)
  "Expand `org-emphasize' to emphasize word or region."
  (interactive)
  (unless (region-active-p)
    (backward-word)
    (mark-word))
  (org-emphasize char))

;;;;; Hide empty org agenda blocks
(defun mp-org-agenda--delete-empty-blocks ()
  "Remove empty agenda blocks.
A block is identified as empty if there are fewer than 2
non-empty lines in the block (excluding the line with
`org-agenda-block-separator' characters)."
  (when org-agenda-compact-blocks
    (user-error "Cannot delete empty compact blocks"))
  (setq buffer-read-only nil)
  (save-excursion
    (goto-char (point-min))
    (let* ((blank-line-re "^\\s-*$")
           (content-line-count (if (looking-at-p blank-line-re) 0 1))
           (start-pos (point))
           (block-re (format "%c\\{10,\\}" org-agenda-block-separator)))
      (while (and (not (eobp)) (forward-line))
        (cond
         ((looking-at-p block-re)
          (when (< content-line-count 2)
            (delete-region start-pos (1+ (point-at-bol))))
          (setq start-pos (point))
          (forward-line)
          (setq content-line-count (if (looking-at-p blank-line-re) 0 1)))
         ((not (looking-at-p blank-line-re))
          (setq content-line-count (1+ content-line-count)))))
      (when (< content-line-count 2)
        (delete-region start-pos (point-max)))
      (goto-char (point-min))
      ;; The above strategy can leave a separator line at the beginning
      ;; of the buffer.
      (when (looking-at-p block-re)
        (delete-region (point) (1+ (point-at-eol))))))
  (setq buffer-read-only t)
  (goto-char (point-min)))

;;;;; Ensure blank lines between headings and before contents

;; Taken from
;; <https://github.com/alphapapa/unpackaged.el#ensure-blank-lines-between-headings-and-before-contents>

;;;###autoload
(defun mp-org-fix-blank-lines (&optional prefix)
  "Ensure that blank lines exist between headings and between headings and their contents.
With prefix, operate on whole buffer. Ensures that blank lines
exist after each headings's drawers."
  (interactive "P")
  (org-map-entries (lambda ()
                     (org-with-wide-buffer
                      ;; `org-map-entries' narrows the buffer, which prevents us from seeing
                      ;; newlines before the current heading, so we do this part widened.
                      (while (not (looking-back "\n\n" nil))
                        ;; Insert blank lines before heading.
                        (insert "\n")))
                     (let ((end (org-entry-end-position)))
                       ;; Insert blank lines before entry content
                       (forward-line)
                       (while (and (org-at-planning-p)
                                   (< (point) (point-max)))
                         ;; Skip planning lines
                         (forward-line))
                       (while (re-search-forward org-drawer-regexp end t)
                         ;; Skip drawers. You might think that `org-at-drawer-p' would suffice, but
                         ;; for some reason it doesn't work correctly when operating on hidden text.
                         ;; This works, taken from `org-agenda-get-some-entry-text'.
                         (re-search-forward "^[ \t]*:END:.*\n?" end t)
                         (goto-char (match-end 0)))
                       (unless (or (= (point) (point-max))
                                   (org-at-heading-p)
                                   (looking-at-p "\n"))
                         (insert "\n"))))
                   t (if prefix
                         nil
                       'tree)))

(provide 'mp-org)
;;; mp-org.el ends here
