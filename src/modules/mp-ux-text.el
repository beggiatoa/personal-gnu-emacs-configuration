;;; mp-ux-text.el --- Interact with text or code  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marcop@p7560-f37>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Specific programming languages are configured individually

;;; Code:
;;;; Enable a bunch of useful commands
(put 'set-goal-column 'disabled nil)

;;;; Subword for cammelCase
(add-hook 'after-init-hook #'global-subword-mode)

;;;; Set default encoding system
(set-language-environment "UTF-8")

;;;; Auto fill almost everywhere
(setq-default fill-column 72)
(add-hook 'prog-mode-hook #'auto-fill-mode)
(add-hook 'text-mode-hook (lambda () (setq-local fill-column 80)))

(use-package adaptive-wrap
  :ensure t)

(use-package visual-fill-column
  :ensure t
  :after adaptive-wrap
  :hook
  ((visual-fill-column-mode-hook . adaptive-wrap-prefix-mode))
  :bind
  (("C-x x l" . visual-line-fill-column-mode))
  :config
  (advice-add 'text-scale-adjust :after #'visual-fill-column-adjust)
  (setopt visual-fill-column-enable-sensible-window-split t))

;;;; Automatic delimiter pairs
(add-hook 'text-mode-hook #'electric-pair-mode)
(add-hook 'text-mode-hook #'electric-quote-mode)
(setopt electric-quote-context-sensitive t)

;;;; Insert a character until specified column
(defun mp-repeat-char-to-column (column character)
  "Insert copies of CHARACTER on the current line until column COLUMN.
Interactively, prompt for COLUMN and CHARACTER. If the current column is
equal to or greater than COLUMN, do nothing."
  (interactive "NRepeat to column: \ncCharacter to repeat: ")
  (let ((cur (current-column)))
    (if (< cur column)
        (insert (make-string (- column cur) character))
      (warn "%s is before the current column" column))))

;;;; Spell checking
;; Replacing flyspell with jinx <https://github.com/minad/jinx>
(use-package jinx
  :ensure t
  :hook (emacs-startup . global-jinx-mode)
  :bind (("M-$" . jinx-correct)
         ("C-M-$" . jinx-languages))
  :config
  (with-eval-after-load 'vertico
    (add-to-list 'vertico-multiform-categories
             '(jinx grid (vertico-grid-annotate . 20)))
    (vertico-multiform-mode 1))
  (setopt jinx-languages "en_US italian"))

;; (use-package flyspell
;;   :config
;;   (setopt
;;    ispell-program-name "aspell"
;;    ispell-dictionary "en_us"
;;    ispell-silently-savep t
;;    flyspell-use-meta-tab nil))

;; ;; `flyspell-correct-wrapper' correct word before point.  with one
;; ;; `prefix-argument' correct multiple words, with two changes direction,
;; ;; and with three changes direction and enables multiple corrections.
;; (use-package flyspell-correct
;;   :ensure t
;;   :after flyspell
;;   :bind
;;   (:map flyspell-mode-map
;;         ("c-;" . nil)
;;         ("c-." . nil)
;;         ("c-," . nil)
;;         ("<f8>" . flyspell-correct-wrapper)))

;; Advanced spell checking with LanguageTool
(use-package languagetool
  :ensure t
  :config
  (setopt languagetool-api-key "pit-M5zjEvXch0Wm"
          languagetool-username "marco.preve@gmail.com"
          languagetool-java-arguments '("-Dfile.encoding=UTF-8")
          languagetool-console-command "~/.local/opt/LanguageTool-6.2/languagetool-commandline.jar"
          languagetool-server-command "~/.local/opt/LanguageTool-6.2/languagetool-server.jar"))

;;;; Definitions and Synonyms
;; mandatory, as the dictionary misbehaves!
(use-package dictionary
  :bind
  (("M-#" . dictionary-search))
  :config
  (setopt switch-to-buffer-obey-display-actions t
          dictionary-server "dict.org")
  (add-to-list 'display-buffer-alist
               '("^\\*Dictionary\\*" display-buffer-in-side-window
                 (side . left)
                 (window-width . 50))))

;; ;; WordNet requires wordned executable
;; (use-package wordnut
;;   :ensure t
;;   :if (executable-find "wn"))

;; ;; Synosaurus requires wordnet executable
;; (use-package synosaurus
;;   :ensure t
;;   :if (executable-find "wn")
;;   :config
;;   (setopt synosaurus-backend 'synosaurus-backend-wordnet
;;           synosaurus-choose-method 'default))

;;;; Provide this file for Emacs
(provide 'mp-ux-text)

;;; mp-ux-text.el ends here
