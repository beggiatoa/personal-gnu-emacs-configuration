;;; mp-ux-projects.el --- Project management in Emacs  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <marcop@p7560-f37>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
;;;; Built-in project.el library
(use-package project
  :after magit
  :hook
  (after-init-hook . project-forget-zombie-projects)
  :bind
  (:map project-prefix-map
        ("s" . project-find-regexp)
        ("S" . project-or-external-find-regexp)
        ("d" . project-dired)
        ("D" . project-find-dir)
        ("%" . project-query-replace-regexp)
        ("g" . magit-project-status)
        ("r" . project-forget-project))
  :config
  (setq project-switch-commands
        '((project-find-file "Find file")
          (project-dired "Dired")
          (project-find-regexp "Find regexp")
          (project-query-replace-regexp "Query replace")
          (magit-project-status "Magit")
          (project-shell-command "Shell command")
          (project-eshell "Eshell")))
  :custom
  ((project-list-file (expand-file-name "projects" mp-emacs--cache-dir))
   (project-vc-include-untracked t)))

;;;; Extra: Additional features with `project-x'
;; The [[https://github.com/karthink/project-x][project-x]] package adds support
;; for "local" projects, as well as facilities for periodically saving
;; project-specific window configurations. It also adds commands to save and
;; restore project windows.

(unless (package-installed-p 'project-x)
  (package-vc-install "https://www.github.com/karthink/project-x"))

(use-package project-x
  :after project
  :bind
  (("C-x p w" . project-x-window-state-save)
   ("C-x p j" . project-x-window-state-load))
  :config
  (add-hook 'project-find-functions 'project-x-try-local 90)
  (add-hook 'kill-emacs-hook 'project-x--window-state-write)

  (setq project-x-local-identifier
        '(".project" "Project.toml" "**.Rproj"))
  (setq project-x-window-list-file
        (expand-file-name "project-window-list" mp-emacs--cache-dir))
  (setq project-x-save-interval 600)

  (add-to-list 'project-switch-commands
               '(project-x-window-state-load "Restore windows") t)

  (project-x-mode 1))

;;;; Extra: Group projects in `tabspaces'
(use-package tabspaces
  :ensure t
  :bind-keymap ("C-c TAB" . tabspaces-command-map)
  :bind
  (:map tabspaces-command-map
        ("C" . tabspaces-clear-buffers)
        ("b" . tabspaces-switch-to-buffer)
        ("d" . tabspaces-close-workspace)
        ("k" . tabspaces-kill-buffers-close-workspace)
        ("o" . tabspaces-open-or-create-project-and-workspace)
        ("r" . tabspaces-remove-current-buffer)
        ("R" . tabspaces-remove-selected-buffer)
        ("s" . tabspaces-switch-or-create-workspace)
        ("t" . tabspaces-switch-buffer-and-tab))
  :init
  (add-hook 'after-init-hook #'tabspaces-mode 90)
  :config
  (setopt tabspaces-use-filtered-buffers-as-default nil
          tabspaces-default-tab "Home"
          tabspaces-remove-to-default t
          tabspaces-include-buffers '("*scratch*")
          tabspaces-keymap-prefix "C-c TAB"
          tabspaces-session nil         ; Use project-x for window management
          tabspaces-session-auto-restore nil
          tabspaces-initialize-project-with-todo nil)

  ;; Filter Buffers for Consult-Buffer
  (with-eval-after-load 'consult
    ;; hide full buffer list (still available with "b" prefix)
    (consult-customize consult--source-buffer :hidden t :default nil)
    ;; set consult-workspace buffer list
    (defvar consult--source-workspace
      (list :name     "Workspace Buffers"
            :narrow   ?w
            :history  'buffer-name-history
            :category 'buffer
            :state    #'consult--buffer-state
            :default  t
            :items    (lambda () (consult--buffer-query
                                  :predicate #'tabspaces--local-buffer-p
                                  :sort 'visibility
                                  :as #'buffer-name)))
      "Set workspace buffer list for consult-buffer.")
    ;; Add to `consult-buffer-sources'
    (add-to-list 'consult-buffer-sources 'consult--source-workspace)))

;;;; Provide file
(provide 'mp-ux-projects)

;;; mp-ux-projects.el ends here
