;;; early-init.el --- Emacs early init file -*- lexical-binding: t -*-

;; Copyright (C) 2020-2023 Marco Prevedello <marco.prevedello@outlook.it>

;; Author: Marco Prevedello <marco.prevedello@outlook.it>
;; URL: https://gitlab.com/beggiatoa/personal-gnu-emacs-configuration.git
;; Created: 2022-10-06
;; Version: 0.2.0
;; Package-Requires: ((emacs "29.0"))

;; This file is NOT part of GNU Emacs.

;; This file is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; The `early-init.el' file (this file) can be used to influence
;; settings before the package manager and GUI initialization.  Note
;; that customization of GUI features may not work reliably as these are
;; not initialized yet.

;; For further info refer to the manual page info:emacs#Early Init File.

;;; Code:

;;;; Set default frame display
(dolist (var '(default-frame-alist initial-frame-alist))
  (add-to-list var '(width . (text-pixels . 1214)))
  (add-to-list var '(height . (text-pixels . 900)))
  (add-to-list var '(left-fringe . 12))
  (add-to-list var '(right-fringe . 2))
  (add-to-list var '(undecorated . nil))
  (add-to-list var '(right-divider-width . 1))
  (add-to-list var '(bottom-divider-width . 1))
  (add-to-list var '(menu-bar-lines . 0))
  (add-to-list var '(tool-bar-lines . 0))
  (add-to-list var '(vertical-scroll-bars)))

(setq frame-resize-pixelwise t
      frame-inhibit-implied-resize t
      frame-title-format '("%b")
      ring-bell-function 'ignore
      use-dialog-box t ; only for mouse events, which I seldom use
      use-file-dialog nil
      use-short-answers t
      inhibit-splash-screen t
      inhibit-startup-screen t
      inhibit-x-resources t
      inhibit-startup-echo-area-message user-login-name ; read the docstring
      inhibit-startup-buffer-menu t)

(add-hook 'after-init-hook (lambda () (set-frame-name "Home")))
(add-hook 'server-after-make-frame-hook (lambda () (set-frame-name "Home")))

;;;; Make Emacs start faster

;; Temporarily increase the garbage collection threshold
;; Defer garbage collection further back in the startup process
(setq gc-cons-threshold most-positive-fixnum)

(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold (* 1024 1024 100)))) ; 100 Mb

;; In Emacs 27+, package initialization occurs before `user-init-file' is
;; loaded, but after `early-init-file'.
(setq package-enable-at-startup t
      package-quickstart t
      load-prefer-newer t)

;; Disable certain things if not in Daemon-Server mode
(unless (or (daemonp) noninteractive)
  (let ((old-file-name-handler-alist file-name-handler-alist))
    ;; `file-name-handler-alist' is consulted on each `require', `load' and
    ;; various path/io functions. You get a minor speed up by unsetting this.
    ;; Some warning, however: this could cause problems on builds of Emacs where
    ;; its site lisp files aren't byte-compiled and we're forced to load the
    ;; *.el.gz files (e.g. on Alpine).
    (setq-default file-name-handler-alist nil)
    ;; ...but restore `file-name-handler-alist' later, because it is needed for
    ;; handling encrypted or compressed files, among other things.
    (defun reset-file-handler-alist ()
      (setq file-name-handler-alist
            ;; Merge instead of overwrite because there may have bene changes to
            ;; `file-name-handler-alist' since startup we want to preserve.
            (delete-dups (append file-name-handler-alist
                                 old-file-name-handler-alist))))
    (add-hook 'emacs-startup-hook #'reset-file-handler-alist 101))

  (setq-default inhibit-redisplay t
                inhibit-message t)
  (add-hook 'window-setup-hook
            (lambda ()
              (setq-default inhibit-redisplay nil
                            inhibit-message nil)
              (redisplay)))

  ;; Site files tend to use `load-file', which emits "Loading X..." messages in
  ;; the echo area, which in turn triggers a redisplay. Redisplays can have a
  ;; substantial effect on startup times and in this case happens so early that
  ;; Emacs may flash white while starting up.
  (define-advice load-file (:override (file) silence)
    (load file nil 'nomessage))

  ;; Undo our `load-file' advice above, to limit the scope of any edge cases it
  ;; may introduce down the road.
  (define-advice startup--load-user-init-file (:before (&rest _) nomessage-remove)
    (advice-remove #'load-file #'load-file@silence)))

;;;; Configure native-compilation
(when (and (fboundp 'native-comp-available-p)
           (native-comp-available-p))
  (add-to-list 'native-comp-eln-load-path
               (concat "~/.cache/emacs/" "eln-cache/"))
  (setq native-comp-async-report-warnings-errors 'silent)
  (setq native-comp-async-query-on-exit t))

;;; early-init.el ends here.
