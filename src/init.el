;;; init.el --- Emacs init file -*- lexical-binding: t -*-

;; Copyright (C) 2020-2022 Marco Prevedello <marco.prevedello@outlook.it>

;; Author: Marco Prevedello <marco.prevedello@outlook.it>
;; Maintainer: Marco Prevedello <marco.prevedello@outlook.it>
;; Created: 2022-10-06
;; Version: 0.1.0
;; Homepage: https://gitlab.com/beggiatoa/personal-gnu-emacs-configuration.git

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; See the info:emacs#Init File.

;;; Code:
;;;; Package management
;;;;; Repositories

(require 'package)

(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))

;;;;; Personal libraries

(dolist (path '("lisp" "modules"))
  (add-to-list 'load-path (locate-user-emacs-file path)))

;;;;; Package.el interface (Emacs 28+)

(setq package-name-column-width 40
      package-version-column-width 14
      package-status-column-width 12
      package-archive-column-width 8)

(add-hook 'package-menu-mode-hook #'hl-line-mode)

;;;;; Configure use-package (now built-in with Emacs 29+)

(require 'use-package)

(setq use-package-always-ensure nil
      use-package-always-defer nil
      use-package-hook-name-suffix nil
      use-package-enable-imenu-support t)

;;;; Load modules
(require 'mp-core)             ; Core general setting
(require 'mp-ui-modeline)      ; Modeline user interface (UI) settings
(require 'mp-ui-theme)         ; Theme UI settings
(require 'mp-ui-font)          ; Font UI settings
(require 'mp-ux-completion)    ; Completion user experience (UX)
(require 'mp-ux-files-buffers) ; Interact with files and buffers
(require 'mp-ux-projects)      ; Interact with projects
;; (require 'mp-app-bootstrap-projects)   ; Bootstrap projects from Emacs
(require 'mp-ux-windows)       ; Interact with windows
(require 'mp-ux-text)          ; Interact with text
(require 'mp-ux-prog)          ; Interact with programming code (generic)
(require 'mp-ux-nerd-icons)    ; Cute icons

(require 'mp-prog-treesit)     ; tree-sitter
(require 'mp-prog-markdown)    ; Editing Markdown files
(require 'mp-prog-r)           ; Statistical programming
(require 'mp-prog-latex)       ; (La)TeX editing
(require 'mp-prog-python)      ; Python editing
(require 'mp-prog-yaml)        ; YAML, JSON, XLM, etc

(require 'mp-app-calc)         ; Emacs calculator: the best calcuolator
(require 'mp-app-org)          ; Stay organized with Org Modew
(require 'mp-app-org-roam)     ; Note taking with Org Roam
(require 'mp-app-dired)        ; File explorer in emacs
(require 'mp-app-csv)          ; Editing CSV (& co.) files
(require 'mp-app-pdf)          ; PDF viewer
(require 'mp-app-biblio)       ; Bibliography management in emacs
(require 'mp-app-git)          ; Git interface in emacs
(require 'mp-app-shells)       ; Terminal emulators and shells
(require 'mp-app-conda)
(when (executable-find "mu")   ; Emails
   (require 'mp-app-mu4e))
;; (require 'mp-app-aw)           ; See <https://activitywatch.net/>
(require 'mp-app-elfeed)       ; RSS feed
(require 'mp-app-pocket)       ; Integration with Firefox's Pocket
(require 'mp-app-casual-suite) ; See <https://github.com/kickingvegas/casual-suite>
(require 'mp-app-gptel)        ; LLMs integration. See <https://github.com/karthink/gptel>

;;;; Calculate Init time
(message "Emacs loaded in %s." (emacs-init-time))

;;; init.el ends here
