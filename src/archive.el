;;; archive.el --- Code archive -*- lexical-binding: t -*-

;; Copyright (C) 2020-2022 Marco Prevedello <marco.prevedello@outlook.it>

;; Author: Marco Prevedello <marco.prevedello@outlook.it>
;; Maintainer: Marco Prevedello <marco.prevedello@outlook.it>
;; Created: 2022-11-07
;; Version: 0.1.0
;; Homepage: https://gitlab.com/beggiatoa/personal-gnu-emacs-configuration.git

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Code snippets that are either broken or no longer in use.

;;; Org
;;;; BROKEN! Capture birthdays as org-anniversaries

;; The capture insert and error followed by the function definition.
(with-eval-after-load 'org-capture
  (add-to-list 'org-capture-templates
               '("a" "Templates for recurring events…")
               t)
  (add-to-list 'org-capture-templates
               '("ab" "Birthday" plain (file+headline  "agenda.org" "Birthdays")
                 "%(mp/org-insert-anniversary)"
                 :immediate-finish t
                 :empty-lines-after 1
                 :clock-resume t
                 :kill-buffer t)
               t))

(defun mp/org-insert-anniversary (year month day name)
  "Interactively insert an anniversary in org files.
See `org-anniversary.'"
  (interactive
   (let* ((date (read-string "Date [yyyy-mm-dd]: "))
          (year (string-to-number
                 (replace-regexp-in-string
                  ".*\\([[:digit:]]\\{4\\}\\).*" "\\1" date)))
          (month (string-to-number
                  (replace-regexp-in-string
                   ".*[[:digit:]]\\{4\\}-\\([[:digit:]]\\{2\\}\\).*" "\\1" date)))
          (day (string-to-number
                (replace-regexp-in-string
                 ".*[[:digit:]]\\{2\\}-\\([[:digit:]]\\{2\\}\\).*" "\\1" date)))
          (name (read-string "Anniversary name: ")))
     (list year month day name)))
  (print
   (format "%%%%(org-anniversary %d %d %d) %%d anniversary for %s"
           year month day name)))

;;; archive.el ends here.
