;;; mp-lisps.el --- Lisp(s) programming -*- lexical-binding: t -*-

;; Copyright (C) 2020-2022 Marco Prevedello <marco.prevedello@outlook.it>

;; Author: Marco Prevedello <marco.prevedello@outlook.it>
;; Maintainer: Marco Prevedello <marco.prevedello@outlook.it>
;; Created: 2022-10-06
;; Version: 0.1.0
;; Homepage: https://gitlab.com/beggiatoa/personal-gnu-emacs-configuration.git

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Generic configurations for editing LISP family code with GNU Emacs.

;;; Code:

;;;; Quickly eval buffer
(global-set-key (kbd "C-x x e") 'eval-buffer)

;;;; Parentheses (show-paren-mode)
(use-package paren
  :custom
  (show-paren-style 'parenthesis)
  (show-paren-when-point-in-periphery nil)
  (show-paren-when-point-inside-paren nil)
  :config
  (when (boundp 'show-paren-context-when-offscreen)
    (setq show-paren-context-when-offscreen 'child-frame))
  :hook
  (after-init-hook . show-paren-mode))

;;;; Eldoc (elisp live documentation feedback)
(use-package eldoc
  :blackout t
  :hook
  (after-init-hook . global-eldoc-mode))

;;;; Elisp linting

(use-package package-lint-flymake
  :ensure t
  :hook
  (flymake-diagnostic-functions . package-lint-flymake))

;;; mp-lisps ends here:
(provide 'mp-lisps)
