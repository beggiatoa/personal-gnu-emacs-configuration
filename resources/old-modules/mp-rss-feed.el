;;; mp-rss-feed.el --- RSS Feeds in Emacs            -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Marco Prevedello

;; Author: Marco Prevedello <m.prevedello1@universityofgalway.ie>
;; Created: 2022-11-26

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; RSS feeds with Elfeed

(use-package elfeed
  :ensure t
  :bind
  (("C-c e" . elfeed)
   :map elfeed-search-mode-map
   ("w" . elfeed-search-yank)
   ("g" . elfeed-update)
   ("G" . elfeed-search-update--force))
  :init
  (setq elfeed-db-directory
        (expand-file-name "elfeed/" mp-cache-dir))
  (setq elfeed-enclosure-default-dir
        (expand-file-name "downloads/" elfeed-db-directory))
  (setq elfeed-search-filter "@2-weeks-ago +unread")
  (setq elfeed-search-clipboard-type 'CLIPBOARD)
  (setq elfeed-search-title-max-width 100)
  (setq elfeed-search-title-min-width 30)
  (setq elfeed-search-trailing-width 25)
  (setq elfeed-feeds
        '(("https://fediscience.org/tags/RStats.rss"
           mastodon fediscience rstat r)
          ("https://mstdn.science/public/local.rss"
           mastodon microbiology science)
          ("https://www.science.org/action/showFeed?type=etoc&feed=rss&jc=science"
           science aaas journal toc)))

  (let ((dirs `(,elfeed-db-directory
                ,elfeed-enclosure-default-dir)))
    (dolist (dir dirs)
      (unless (file-directory-p dir)
        (make-directory dir)))))

;;; mp-rss-feed.el ends here:
(provide 'mp-rss-feed)
