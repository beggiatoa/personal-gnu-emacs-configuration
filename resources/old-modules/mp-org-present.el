(use-package mixed-pitch
  :ensure t)

(use-package olivetti
  :ensure t
  :init
  (setq olivetti-body-width 0.7)
  (setq olivetti-minimum-body-width 80)
  (setq olivetti-recall-visual-line-mode-entry-state t)
  :bind
  ("<f9>" . olivetti-mode))

(use-package org-tree-slide
  :ensure t
  :preface
  (defvar mp-hide-org-meta-line-p nil)

  (defun mp//show-org-meta-line ()
    "Show org-meta-line lines."
    (setq mp-hide-org-meta-line-p nil)
    (set-face-attribute 'org-meta-line nil :foreground nil))

  (defun mp//hide-org-meta-line ()
    "Show org-meta-line lines."
    (setq mp-hide-org-meta-line-p t)
    (set-face-attribute 'org-meta-line nil
            :foreground (face-attribute 'default :background)))

  (defun mp/toggle-org-meta-line ()
    "Toggle org-meta-line lines."
    (interactive)
    (if mp-hide-org-meta-line-p
    mp//show-org-meta-line
      mp//hide-org-meta-line))

  (defvar mp-face-remapping-p nil)

  (defun mp//face-remapping ()
    "Remap certain faces"
    (setq mp-face-remapping-p t)
    (setq-local line-spacing 10)
    (setq-local face-remapping-alist '((default (:height 1.5) default)
                    ;                                       (header-line (:height 4.5))
                    ;                                       (org-document-title (:height 1.75) org-document-title)
                    ;                                       (org-code (:height 1.55) org-code)
                    ;                                       (org-verbatim (:height 1.55) org-verbatim)
                    ;                                       (org-block (:height 1.25) org-block)
                    ;                                       (org-block-begin-line (:height 0.7) org-block))
                       )
        org-format-latex-options (plist-put org-format-latex-options :scale 2.5)))

  (defun mp//face-remapping-reset ()
    "Reset face remapping"
    (setq mp-face-remapping-p nil)
    (setq-local line-spacing nil)
    (setq-local face-remapping-alist '((default variable-pitch default))
        org-format-latex-options (plist-put org-format-latex-options :scale 1.0)))

  (defvar mp-org-tree-slide-beutify-p nil)

  (defun mp//org-tree-slide-beutify ()
    "Personal configuration for org-tree-slide presentations"
    (setq mp-org-tree-slide-beutify-p t)
    (set-frame-parameter nil 'internal-border-width 20)
    (mp//face-remapping)
    (mp//hide-org-meta-line)
    (scroll-lock-mode +1)
    (olivetti-mode +1)
    (org-indent-mode +1)
    (org-redisplay-inline-images)
                    ;    (mixed-pitch-mode +1)
    (hide-mode-line-mode +1)
    (blink-cursor-mode -1))

  (defun mp//org-tree-slide-reset ()
    "Reset values set with `mp//org-tree-slide-play'."
    (setq mp-org-tree-slide-beutify-p nil)
    (set-frame-parameter nil 'internal-border-width 0)
    (mp//face-remapping-reset)
    (mp//show-org-meta-line)
    (scroll-lock-mode -1)
    (olivetti-mode -1)
    (org-indent-mode -1)
                    ;    (mixed-pitch-mode -1)
    (hide-mode-line-mode -1))

  :init
  (setq org-tree-slide-breadcrumbs nil)
  (setq org-tree-slide-header nil)
  (setq org-tree-slide-slide-in-effect nil)
  (setq org-tree-slide-heading-emphasis nil)
  (setq org-tree-slide-cursor-init t)
  (setq org-tree-slide-modeline-display nil)
  (setq org-tree-slide-skip-done nil)
  (setq org-tree-slide-skip-comments t)
  (setq org-tree-slide-fold-subtrees-skipped t)
  (setq org-tree-slide-skip-outline-level 8)
  (setq org-tree-slide-never-touch-face t)
  (setq org-tree-slide-activate-message
    (format "Presentation %s" (propertize "ON" 'face 'success)))
  (setq org-tree-slide-deactivate-message
    (format "Presentation %s" (propertize "OFF" 'face 'error)))
  :config
  (add-hook 'org-tree-slide-play-hook #'mp//org-tree-slide-beutify)
  (add-hook 'org-tree-slide-stop-hook #'mp//org-tree-slide-reset)
  :bind
  (:map org-mode-map
    ("<f8>" . org-tree-slide-mode))
  (:map org-tree-slide-mode-map
    ("<f11>" . org-tree-slide-content)
    ("<down>" . org-tree-slide-display-header-toggle)
    ("<right>" . org-tree-slide-move-next-tree)
    ("<left>" . org-tree-slide-move-previous-tree)))

(use-package hide-mode-line
  :ensure t)
