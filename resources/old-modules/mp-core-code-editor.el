;;; mp-base-code-editor.el --- Code editor configuration -*- lexical-binding: t -*-

;; Copyright (C) 2020-2022 Marco Prevedello <marco.prevedello@outlook.it>

;; Author: Marco Prevedello <marco.prevedello@outlook.it>
;; Maintainer: Marco Prevedello <marco.prevedello@outlook.it>
;; Created: 2022-10-06
;; Version: 0.1.0
;; Homepage: https://gitlab.com/beggiatoa/personal-gnu-emacs-configuration.git

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Generic configurations for editing code with GNU Emacs.

;;; Flyspell in prog-mode
(use-package flyspell
  :hook
  (prog-mode-hook . flyspell-prog-mode))

;;; Smartparens
(use-package smartparens
  :ensure t
  :bind
  (:map smartparens-mode-map
        ("C-M-f" . sp-forward-sexp)
        ("C-M-b" . sp-backward-sexp)
        ("C-M-d" . sp-backward-down-sexp)
        ("C-M-u" . sp-backward-up-sexp)
        ("C-M-a" . sp-beginning-of-sexp)
        ("C-M-e" . sp-end-of-sexp)

        ("C-M-t" . sp-transpose-sexp)

        ("C-M-k" . sp-kill-sexp)
        ("C-M-w" . sp-copy-sexp)

        ("C-M-<delete>" . sp-unwrap-sexp)
        ("C-M-<backspace>" . sp-backward-unwrap-sexp)

        ("C-<right>" . sp-forward-slurp-sexp)
        ("C-<left>" . sp-forward-barf-sexp)
        ("C-M-<left>" . sp-backward-slurp-sexp)
        ("C-M-<right>" . sp-backward-barf-sexp)

        ("M-S-f" . sp-forward-symbol)
        ("M-S-b" . sp-backward-symbol)
        )
  :init
  (require 'smartparens-config)
  :hook
  ((prog-mode-hook . smartparens-mode)
   (ess-R-post-run-hook . smartparens-mode)))

;;; mp-base-code-editor ends here:
(provide 'mp-base-code-editor)
