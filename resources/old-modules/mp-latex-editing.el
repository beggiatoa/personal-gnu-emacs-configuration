;;; mp-latex-editing.el --- LaTeX editing            -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marco Prevedello

;; Author: Marco Prevedello <m.prevedello1@universityofgalway.ie>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package auctex
  :ensure t
  :config
  (setcdr (assoc 'output-pdf TeX-view-program-selection) '("PDF Tools"))
  :hook
  (TeX-mode-hook . turn-on-auto-fill))

;;; mp-latex-editing.el ends here
(provide 'mp-latex-editing)
