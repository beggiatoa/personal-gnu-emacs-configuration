;;; mp-ui-extras.el --- Extra UI elements -*- lexical-binding: t -*-

;; Copyright (C) 2020-2022 Marco Prevedello <marco.prevedello@outlook.it>

;; Author: Marco Prevedello <marco.prevedello@outlook.it>
;; Maintainer: Marco Prevedello <marco.prevedello@outlook.it>
;; Created: 2022-11-07
;; Version: 0.1.0
;; Homepage: https://gitlab.com/beggiatoa/personal-gnu-emacs-configuration.git

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Extra UI elements for embellishment.


;;; Centered text with `olivetti' and `logos'

(use-package olivetti
  :ensure t
  :init
  (setq olivetti-body-width 0.75)
  (setq olivetti-minimum-body-width 90)
  (setq olivetti-recall-visual-line-mode-entry-state t))

(use-package logos
  :ensure t
  :bind
  (([remap narrow-to-region] . logos-narrow-dwim)
   ([remap forward-page] . logos-forward-page-dwim)
   ([remap backward-page] . logos-backward-page-dwim)
   ("<f9>" . logos-focus-mode))
  :init
  (setq logos-outlines-are-pages t)
  (setq-default logos-olivetti t)
  (setq-default logos-hide-mode-line t)
  (setq-default logos-hide-fringe t)
  (setq-default logos-hide-buffer-boundaries t)
  (setq-default mp-logos-hide-menu-bar t)
  (setq logos-variable-pitch t)
  (setq logos-buffer-read-only t)
  :hook
  ((logos-focus-mode-extra-functions . mp//logos-hide-menu-bar)
   (modus-themes-after-load-theme-hook . logos-update-fringe-in-buffers)
   (logos-page-motion-hook . mp//logos--recenter-top))
  :preface
  (defcustom mp-logos-hide-menu-bar nil
    "When non-nil hide the menu bar.
This is only relevant when `logos-focus-mode' is enabled."
  :type 'boolean
  :group 'logos
  :local t)

  (defun mp//logos-hide-menu-bar ()
    "Set `menu-bar-mode' to -1"
    (when mp-logos-hide-menu-bar
      (logos--mode 'menu-bar-mode -1)))

  (defun mp//logos--recenter-top ()
    "Place point at the top when changing pages in non-prog-modes."
    (unless (derived-mode-p 'prog-mode)
      (recenter 0))))


;;; mp-ui-extras.el ends here:
(provide 'mp-ui-extras)
