

(setq org-clock-persist 'history)

(add-hook 'after-init-hook #'org-clock-persistence-insinuate)

;; Clock into a new task
(with-eval-after-load 'org-capture
  (add-to-list 'org-capture-templates
               `("c" "Clock in to a task" entry (file "inbox.org")
                 ,(concat "* TODO %^{Title}\n"
                          "SCHEDULED: %T\n"
                          ":PROPERTIES:\n"
                          ":EFFORT: %^{Effort estimate in minutes|5|10|15|30|45|60|90|120}\n"
                          ":END:")
                 :prepend t
                 :empty-lines-after 1
                  :clock-in t)))

;;; State logging

(setq-default org-log-into-drawer t)
(setq-default org-log-done 'time)

;;; Personal tags

(setq org-tag-alist
      '((:startgroup . "Location")
        ("@lab" . ?l) ("@home" . ?h) ("@office" . ?o)
        (:endgroup)
        (:startgroup . "Context")
        ("work" . ?w) ("personal" . ?p) ("crypt" . ?c) ; See `mp-org-crypt'
        (:endgroup)
        (:startgroup)
        ("project" . ?P)
        (:grouptags)
        ("P@.+")
        (:endgroup)
        ("mail" . ?M)))

(setq org-use-tag-inheritance t)
;; '("inbox" "agenda" "work" "personal" "project" "stuck"))
(setq org-fast-tag-selection-single-key nil)

;;; Custom org-agenda views
;; See also the `mp-org-core/;;; Org agenda appearance' where the general
;; appearance is set.

(with-eval-after-load 'org
  (setq org-agenda-files (expand-file-name "agenda-files" org-directory)))

(setq org-agenda-skip-scheduled-if-done t)
(setq org-agenda-skip-scheduled-if-deadline-is-shown t)
(setq org-agenda-skip-deadline-if-done t)
(setq org-agenda-skip-timestamp-if-done t)
(setq org-agenda-todo-list-sublevels nil)

(with-eval-after-load 'org-agenda
  (add-to-list 'org-agenda-custom-commands
               '("p" "Projects"
                  ((todo "PROJ"
                         ((org-agenda-dim-blocked-tasks nil)
                          (org-agenda-tags-column -80)
                          (org-agenda-prefix-format "%-8:c [%-3e]")
                          (org-agenda-overriding-header "Projects\n")))))
               t)
  (add-to-list 'org-agenda-custom-commands
               '("d" "Dashboard"
                 ((agenda nil
                          ((org-agenda-entry-types '(:deadline))
                           (org-agenda-span 1)
                           (org-agenda-show-future-repeats nil)
                           (org-agenda-format-date "")
                           (org-agenda-prefix-format "%-8:c [%-3e] ")
                           (org-deadline-warning-days 15)
                           (org-agenda-skip-function
                            '(org-agenda-skip-entry-if
                              'todo
                              '("CANC" "DELG" "DONE" "HOLD" "WAIT")))
                           (org-agenda-overriding-header "Deadlines")))
                  (agenda nil
                          ((org-agenda-skip-function
                            '(org-agenda-skip-entry-if 'deadline
                            'todo '("CANC" "DONE" "DELG")))
                           (org-agenda-span 5)
                           (org-agenda-prefix-format " [%-3e] %?-12t% s")
                           (org-agenda-scheduled-leaders '("" ""))
                           (org-deadline-warning-days 0)))
                  (todo "STRT"
                        ((org-agenda-skip-function
                          '(org-agenda-skip-entry-if 'deadline))
                         (org-agenda-prefix-format "%-8:c [%-3e] ")
                         (org-agenda-overriding-header "Started tasks\n")))
                  (todo "DELG"
                        ((org-agenda-prefix-format "%-8:c [%-3e] ")
                         (org-agenda-overriding-header "Delegated tasks\n")))
                  (tags-todo "inbox"
                             ((org-agenda-skip-function
                               '(org-agenda-skip-entry-if 'deadline 'scheduled))
                              (org-agenda-prefix-format "%?-12t% s")
                              (org-agenda-overriding-header "Inbox\n")))
                  (todo "WAIT|HOLD"
                        ((org-agenda-skip-function
                          '(org-agenda-skip-entry-if 'deadline))
                         (org-agenda-prefix-format "%-8:c [%-3e] ")
                         (org-agenda-overriding-header "Task waiting for...\n")))
                  (tags-todo "stuck"
                             ((org-agenda-skip-function
                               '(org-agenda-skip-entry-if 'deadline 'scheduled))
                              (org-agenda-prefix-format "%?-12t% s")
                              (org-agenda-overriding-header "Stuck\n")))
                  (tags "CLOSED>=\"<today>\""
                        ((org-agenda-overriding-header "Completed today\n")))))
               t))

;;; Others

(setq org-read-date-prefer-future 'time)

;;; mp-org-gtd.el ends here:
(provide 'mp-org-gtd)
