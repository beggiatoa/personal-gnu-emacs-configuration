;;; mp-r-ess.el --- R Language support in Emacs -*- lexical-binding: t -*-

;; Copyright (C) 2020-2022 Marco Prevedello <marco.prevedello@outlook.it>

;; Author: Marco Prevedello <marco.prevedello@outlook.it>
;; Maintainer: Marco Prevedello <marco.prevedello@outlook.it>
;; Created: 2022-10-06
;; Version: 0.1.0
;; Homepage: https://gitlab.com/beggiatoa/personal-gnu-emacs-configuration.git

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; R Language support in Emacs via Emacs Speak Statistics (ESS)

;;; ESS for R IDE

(use-package ess
  :ensure t
  :bind
  ("C-x M-e" . ess-request-a-process)
  ;; :map ess-mode-map ("<f10>" . ess-outline-minor-mode)) defined below to avoid error
  :init
  (setq ess-use-ido nil)                ; Do not use IDO
  (setq ess-plain-first-buffername nil) ; Have all iESS buffers numbered
  (setq ess-directory-function 'vc-root-dir)
  (setq ess-ask-for-ess-directory nil)
  (setq ess-style 'RStudio)             ; Collaborating with other RStudio style is important
  (setq ess-nuke-trailing-whitespace-p 'ask)
  :preface
  (defun ess-outline-minor-mode ()
    "Activate and configure `outline-minor-mode` for ESS[R]."
    (interactive)
    (if (null outline-minor-mode)
        (progn
          (setq-local outline-regexp "\\(^#\\{3,6\\}\\)\\|\\(^[a-zA-Z0-9_\.]+ ?<- ?function(.*{\\)")
          (setq-local outline-heading-alist
                      '(("###" . 1)
                        ("####" . 2)
                        ("#####" . 3)
                        ("######" . 4)
                        ("^[a-zA-Z0-9_\.]+ ?<- ?function(.*{" . 5)))
          (outline-minor-mode 1)
          (outline-hide-other)
          (message "Enabled `outline-minor-mode'"))
      (outline-minor-mode -1)
      (message "Disabled `outline-minor-mode'")))
  :config
  (with-eval-after-load 'ess-r-mode
    (define-key ess-r-mode-map (kbd "<f10>") #'ess-outline-minor-mode))
  :hook
  ((ess-mode-hook . (lambda ()
                      (flyspell-prog-mode)
                      (run-hooks 'prog-mode-hook)))
   (ess-mode-hook . prettify-symbols-mode)
   (ess-r-mode-hook . ess-outline-minor-mode)
   (ess-R-post-run-hook . (lambda () (ess-r-package-load)))
   (ess-r-post-run-hook . (lambda () (smartparens-mode 1)))))

;;; ESS smart equals

(use-package ess-smart-equals
  :ensure t
  :custom
  (ess-smart-equals-extra-ops '(brace percent))
  :after
  (:any ess-r-mode inferior-ess-r-mode ess-r-transcript-mode)
  :config
  (ess-smart-equals-activate))

;;; ESS view data

(use-package ess-view-data
  :ensure t
  :after ess-r-mode
  :init
  (with-eval-after-load 'ess-r-mode
    (define-key ess-r-mode-map (kbd "C-c C-. v") #'ess-view-data-print))
  (with-eval-after-load 'ess-rdired
    (define-key ess-rdired-mode-map (kbd "v") #'ess-view-data-print)))

;;; Support for RMarkdown

(use-package poly-R
  :ensure t
  :after
  (:any ess-r-mode inferior-ess-r-mode ess-r-transcript-mode)
  :mode
  ("\\.[rR]md\\'" . poly-gfm+r-mode)
  :custom
  (markdown-code-block-braces t)
  :bind
  (:map org-mode-map
    ("<f12>" . poly-org-mode)))

;;; R in org-mode

(with-eval-after-load 'org
  (add-to-list 'org-babel-load-languages '(R . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages))

;;; mp-r-ess.el ends here:
(provide 'mp-r-ess)
