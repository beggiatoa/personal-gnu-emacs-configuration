;;; mp-base-text-editor.el --- Text editor configuration -*- lexical-binding: t -*-

;; Copyright (C) 2020-2022 Marco Prevedello <marco.prevedello@outlook.it>

;; Author: Marco Prevedello <marco.prevedello@outlook.it>
;; Maintainer: Marco Prevedello <marco.prevedello@outlook.it>
;; Created: 2022-10-06
;; Version: 0.1.0
;; Homepage: https://gitlab.com/beggiatoa/personal-gnu-emacs-configuration.git

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Generic configurations for editing text with GNU Emacs.

;;; Text editing

(global-subword-mode +1)

;;; Text indentation

(setq-default tab-always-indent 'complete)
(setq-default tab-first-completion 'word-or-paren-or-punct) ; Emacs 27
(setq-default tab-width 4)

(setq-default fill-column 80)

(add-hook 'text-mode-hook #'(lambda nil (setq fill-column 70)))

;; Taken from emacs wiki. Credit to Stefan Monnier <foo at acm.org>
(defun unfill-paragraph (&optional region)
  "Takes a multi-line paragraph and makes it into a single line of text."
  (interactive (progn (barf-if-buffer-read-only) '(t)))
  (let ((fill-column (point-max))
        ;; This would override `fill-column' if it's an integer.
        (emacs-lisp-docstring-fill-column t))
    (fill-paragraph nil region)))

(define-key global-map "\M-Q" 'unfill-paragraph)

;;; Upcase-, downcase-, capitalize-dwim

(global-set-key [rebind upcase-word] 'upcase-dwim)
(global-set-key [rebind downcase-word] 'downcase-dwim)
(global-set-key [rebind capitalize-word] 'capitalize-dwim)

;;; Text auto-insertion

(auto-insert-mode)

;;;###autoload
(defun mp/insert-current-date (&optional arg)
  "Insert the current date as YYYY-MM-DD.
With optional prefix ARG (\\[universal-argument]) also append the
current time as HH:MM."
  (interactive "P")
  (let* ((date "%F")
         (time "%R %z")
         (format (if arg (format "%s %s" date time) date)))
    (when (use-region-p)
      (delete-region (region-beginning) (region-end)))
    (insert (format-time-string format))))

(define-key global-map (kbd "C-=") #'mp/insert-current-date)

;;; Spell checking
(use-package flyspell
  :blackout t
  :init
  (setq flyspell-default-dictionary "en_US")
  :hook
  (text-mode-hook . turn-on-flyspell)
  )

;; (use-package flycheck
;;   :ensure t)

;; (use-package flycheck-languagetool
;;   :ensure t
;;   :hook (text-mode . flycheck-languagetool-setup)
;;   :init
;;   (setq flycheck-languagetool-server-jar "~/.local/opt/LanguageTool-6.0/languagetool-server.jar"))

;;; mp-base-text-editor.el ends here:
(provide 'mp-base-text-editor)
