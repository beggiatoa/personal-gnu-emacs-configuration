;;;;; Outline minor mode
(use-package outline
  :straight (:type built-in)
  :init
  (setq outline-minor-mode-highlight 'override
    outline-minor-mode-cycle t)

  (defun prot-outline-narrow-to-subtree ()
    "Narrow to current Outline subtree."
    (interactive)
    (let ((start)
      (end)
      (point (point)))
      (when (and (prot-common-line-regexp-p 'empty)
         (not (eobp)))
    (forward-char 1))
      (when (or (outline-up-heading 1)
        (outline-back-to-heading))
    (setq start (point)))
      (if (outline-get-next-sibling)
      (forward-line -1)
    (goto-char (point-max)))
      (setq end (point))
      (narrow-to-region start end)
      (goto-char point)))

  (defcustom prot-outline-headings-per-mode
    '((emacs-lisp-mode . ";\\{3,\\}+ [^\n]"))
    "Alist of major modes with `outline-regexp' values."
    :type '(alist :key-type symbol :value-type string)
    :group 'outline)

  (defcustom prot-outline-major-modes-blocklist
    '(org-mode outline-mode markdown-mode)
    "Major modes where Outline-minor-mode should not be enabled."
    :type '(repeat symbol)
    :group 'outline)

  (defun prot/outline-minor-mode-safe ()
    "Test to set variable `outline-minor-mode' to non-nil."
    (interactive)
    (let* ((blocklist prot-outline-major-modes-blocklist)
       (mode major-mode)
       (headings (alist-get mode prot-outline-headings-per-mode)))
      (when (derived-mode-p (car (member mode blocklist)))
    (error "Don't use `outline-minor-mode' with `%s'" mode))
      (if (null outline-minor-mode)
      (progn
        (when (derived-mode-p mode)
          (setq-local outline-regexp headings))
        (outline-minor-mode 1)
        (outline-hide-other)
        (message "Enabled `outline-minor-mode'"))
    (outline-minor-mode -1)
    (message "Disabled `outline-minor-mode'"))))
  :bind
  (("<f10>" . prot/outline-minor-mode-safe)
   :map outline-minor-mode-map
   ("C-c C-n" . outline-next-visible-heading)
   ("C-c C-p" . outline-previous-visible-heading)
   ("C-c C-f" . outline-forward-same-level)
   ("C-c C-b" . outline-backward-same-level)
   ("C-c C-a" . outline-show-all)
   ("C-c C-h" . outline-hide-sublevels)
   ("C-c C-o" . outline-hide-other)
   ("C-c C-u" . outline-up-heading)
   ("C-x n s" . prot-outline-narrow-to-subtree)))

;;;;; Find cursor with pulse.el
(use-package prot-pulse
  :after key-chord
  :straight (:type built-in)
  :init
  (setq prot-pulse-pulse-command-list
    '(recenter-top-bottom
      move-to-window-line-top-bottom
      reposition-window
      bookmark-jump
      other-window
      scroll-up-command
      scroll-down-command))
  :config
  (prot-pulse-advice-commands-mode 1)
  (key-chord-define-global "jk" #'prot-pulse-pulse-line)
  (global-set-key (kbd "M-<escape>") #'prot-pulse-pulse-line))





;;;;; Org-noter
(use-package org-noter
  :init
  (setq org-noter-always-create-frame nil)
  (setq org-noter-hide-other t)
  (setq org-noter-default-notes-file-names "~/Org/inbox.org")
  (setq org-noter-notes-search-path "~/Resources")
  (setq org-noter-separate-notes-from-heading t))



;;;; Windows management
;;;;; Display buffer alist
(setq display-buffer-alist
      `(;; top side window
    ("\\*Dictionary.*"
     (display-buffer-in-side-window)
     (window-width . 66)
     (side . left)
     (slot . -1))
    ("\\*\\(Flymake\\|Package-Lint\\|vc-git :\\).*"
     (display-buffer-in-side-window)
     (window-height . 0.16)
     (side . top)
     (slot . 0))
    ("\\*Messages.*"
     (display-buffer-in-side-window)
     (window-height . 0.16)
     (side . top)
     (slot . 1))
    ("\\*\\(Backtrace\\|Warnings\\|Compile-Log\\)\\*"
     (display-buffer-in-side-window)
     (window-height . 0.16)
     (side . top)
     (slot . 2)
     (window-parameters . ((no-other-window . t))))
    ;; Bottom buffer (NOT side window)
    ("\\*\\(Output\\|Register Preview\\).*"
     (display-buffer-at-bottom))
    ("\\*.*\\(e?shell\\|v?term\\).*"
     (display-buffer-reuse-mode-window display-buffer-at-bottom)
     (window-height . 0.2))
    ;; bottom side window
    ;; left side window
    ("\\*Help.*"            ; See the hooks for `visual-line-mode'
     (display-buffer-in-side-window)
     (window-width . 70)
     (side . left)
     (slot . -1))
    ("\\*Faces\\*"
     (display-buffer-in-side-window)
     (window-width . 70)
     (side . left)
     (slot . 0))
    ;; right side window
    ;; below current window
    ("\\*Calendar.*"
     (display-buffer-reuse-mode-window display-buffer-below-selected)
     (window-height . shrink-window-if-larger-than-buffer))))

(setq window-combination-resize t
      even-window-sizes 'height-only
      window-sides-vertical nil
      switch-to-buffer-in-dedicated-window 'pop)

(add-hook 'help-mode-hook #'visual-line-mode)




;;;;; Transpose frames
(use-package transpose-frame
  :bind
  (("H-t" . flop-frame)
   ("C-x w t" . flop-frame)
   ("H-r" . rotate-frame-clockwise)
   ("C-x w r" . rotate-frame-clockwise)))

;;;; Working with text
;;;;; Generic defaults
(add-hook 'prog-mode-hook (lambda () (setq fill-column 72)))
(add-hook 'text-mode-hook (lambda () (setq fill-column 80)))

(setq sentence-end-double-space t
      sentence-end-without-period nil
      colon-double-space nil
      use-hard-newlines nil
      adaptive-fill-mode t
      delete-selection-mode t
      comment-empty-lines t
      comment-fill-column nil
      comment-multi-line t
      comment-style 'multi-line)

(setq-default tab-width 4
          tab-always-indent 'complete
          indent-tabs-mode nil)

(add-to-list 'auto-mode-alist
         '("\\(README\\|CHANGELOG\\|COPYING\\|LICENSE\\)$" . text-mode))

(global-set-key (kbd "C-:") #'comment-kill)

;;;;; DocView for docx, xlsx, etc
(use-package doc-view
  :straight (:type built-in)
  :init
  (setq doc-view-resolution 300))

;;;;; Flyspell
(use-package flyspell
  :straight (:type built-in)
  :blackout t
  :init
  (setq ispell-program-name "aspell"
    ispell-dictionary "en_US"
    ispell-silently-savep t
    flyspell-use-meta-tab nil)
  :config
  (add-to-list 'ispell-skip-region-alist '("#\\+begin_src". "#\\+end_src"))
  :hook
  ((prog-mode-hook . flyspell-prog-mode)
   (text-mode-hook . turn-on-flyspell))
  :bind
  (("M-<f7>" . flyspell-buffer)
   ("<f7>" . flyspell-auto-correct-word)
   (:map flyspell-mode-map
     ("C-;" . nil)
     ("C-." . nil))))

;;;;;; Flyspell-consult

(use-package flyspell-correct
  :after flyspell
  :bind (:map flyspell-mode-map ("<f7>" . flyspell-correct-wrapper)))

;;;;; Emacs Dictionaly
(use-package dictionary
  :straight (:type built-in)
  :init
  (setq dictionary-server "dict.org")
  (setq dictionary-use-single-buffer t)
  :bind
  ("C-c d" . dictionary-search))

;;;;; synonyms
(use-package synosaurus
  :init (setq synosaurus-choose-method 'default)
  :config (synosaurus-mode +1))

;;;;; Markdown
(use-package markdown-mode
  :mode ("/README\\(?:\\.md\\)?\\'" . gfm-mode)
  :init
  (setq markdown-enable-math t
    markdown-italic-underscore t
    markdown-asymmetric-header t
    markdown-gfm-additional-languages '("sh")
    markdown-make-gfm-checkboxes-buttons t))

;;;;;; Grip mode for markdown (and org) GitHub-flavoured previews
(use-package grip-mode
  :bind (:map markdown-mode-command-map
          ("g" . grip-mode)))

;;;;;; Org tables in markdown
(require 'org-table)

(defun cleanup-org-tables ()
  "Replace \"-+-\" with \"-|-\"."
  (save-excursion
    (goto-char (point-min))
    (while (search-forward "-+-" nil t) (replace-match "-|-"))))

(add-hook 'markdown-mode-hook 'orgtbl-mode)
(add-hook 'markdown-mode-hook
      (lambda()
        (add-hook 'after-save-hook 'cleanup-org-tables  nil 'make-it-local)))

;;;;; tab/comma-separated-values files
(use-package csv-mode)

;;;;; Presentation with org mode
(use-package mixed-pitch)

(use-package olivetti
  :init
  (setq olivetti-body-width 0.7)
  (setq olivetti-minimum-body-width 80)
  (setq olivetti-recall-visual-line-mode-entry-state t)
  :bind
  ("<f9>" . olivetti-mode))

(use-package org-tree-slide
  :preface
  (defvar mp-hide-org-meta-line-p nil)

  (defun mp//show-org-meta-line ()
    "Show org-meta-line lines."
    (setq mp-hide-org-meta-line-p nil)
    (set-face-attribute 'org-meta-line nil :foreground nil))

  (defun mp//hide-org-meta-line ()
    "Show org-meta-line lines."
    (setq mp-hide-org-meta-line-p t)
    (set-face-attribute 'org-meta-line nil
            :foreground (face-attribute 'default :background)))

  (defun mp/toggle-org-meta-line ()
    "Toggle org-meta-line lines."
    (interactive)
    (if mp-hide-org-meta-line-p
    mp//show-org-meta-line
      mp//hide-org-meta-line))

  (defvar mp-face-remapping-p nil)

  (defun mp//face-remapping ()
    "Remap certain faces"
    (setq mp-face-remapping-p t)
    (setq-local line-spacing 10)
    (setq-local face-remapping-alist '((default (:height 1.5) default)
                    ;                                       (header-line (:height 4.5))
                    ;                                       (org-document-title (:height 1.75) org-document-title)
                    ;                                       (org-code (:height 1.55) org-code)
                    ;                                       (org-verbatim (:height 1.55) org-verbatim)
                    ;                                       (org-block (:height 1.25) org-block)
                    ;                                       (org-block-begin-line (:height 0.7) org-block))
                       )
        org-format-latex-options (plist-put org-format-latex-options :scale 2.5)))

  (defun mp//face-remapping-reset ()
    "Reset face remapping"
    (setq mp-face-remapping-p nil)
    (setq-local line-spacing nil)
    (setq-local face-remapping-alist '((default variable-pitch default))
        org-format-latex-options (plist-put org-format-latex-options :scale 1.0)))

  (defvar mp-org-tree-slide-beutify-p nil)

  (defun mp//org-tree-slide-beutify ()
    "Personal configuration for org-tree-slide presentations"
    (setq mp-org-tree-slide-beutify-p t)
    (set-frame-parameter nil 'internal-border-width 20)
    (mp//face-remapping)
    (mp//hide-org-meta-line)
    (scroll-lock-mode +1)
    (olivetti-mode +1)
    (org-indent-mode +1)
    (org-redisplay-inline-images)
                    ;    (mixed-pitch-mode +1)
    (hide-mode-line-mode +1)
    (blink-cursor-mode -1))

  (defun mp//org-tree-slide-reset ()
    "Reset values set with `mp//org-tree-slide-play'."
    (setq mp-org-tree-slide-beutify-p nil)
    (set-frame-parameter nil 'internal-border-width 0)
    (mp//face-remapping-reset)
    (mp//show-org-meta-line)
    (scroll-lock-mode -1)
    (olivetti-mode -1)
    (org-indent-mode -1)
                    ;    (mixed-pitch-mode -1)
    (hide-mode-line-mode -1))

  :init
  (setq org-tree-slide-breadcrumbs nil)
  (setq org-tree-slide-header nil)
  (setq org-tree-slide-slide-in-effect nil)
  (setq org-tree-slide-heading-emphasis nil)
  (setq org-tree-slide-cursor-init t)
  (setq org-tree-slide-modeline-display nil)
  (setq org-tree-slide-skip-done nil)
  (setq org-tree-slide-skip-comments t)
  (setq org-tree-slide-fold-subtrees-skipped t)
  (setq org-tree-slide-skip-outline-level 8)
  (setq org-tree-slide-never-touch-face t)
  (setq org-tree-slide-activate-message
    (format "Presentation %s" (propertize "ON" 'face 'success)))
  (setq org-tree-slide-deactivate-message
    (format "Presentation %s" (propertize "OFF" 'face 'error)))
  :config
  (add-hook 'org-tree-slide-play-hook #'mp//org-tree-slide-beutify)
  (add-hook 'org-tree-slide-stop-hook #'mp//org-tree-slide-reset)
  :bind
  (:map org-mode-map
    ("<f8>" . org-tree-slide-mode))
  (:map org-tree-slide-mode-map
    ("<f11>" . org-tree-slide-content)
    ("<down>" . org-tree-slide-display-header-toggle)
    ("<right>" . org-tree-slide-move-next-tree)
    ("<left>" . org-tree-slide-move-previous-tree)))

(use-package hide-mode-line)

;;;;; Edit text from browser in Emacs
;; Note that the browser extension `Edit with Emacs' is required.
(use-package edit-server
  :commands edit-server-start
  :init
  (setq edit-server-new-frame t)
  (setq edit-server-default-major-mode 'org-mode)
  (if after-init-time
      (edit-server-start)
    (add-hook 'after-init-hook
          #'(lambda() (edit-server-start))))
  :config
  (setq edit-server-new-frame-alist
    '((name . "Edit with Emacs FRAME")
      (top . 200)
      (left . 200)
      (width . 80)
      (height . 25)
      (minibuffer . t)
      (menu-bar-lines . t)
      (window-system . x))))

;;;; Working with (La)TeX
(use-package tex
  :straight auctex
  :init
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil))

;;;; Working with e-mail
;; E-mails are fetch by isync/mbsync, which is configured with
;; ~/.mbsyncrc.  E-mails are sent using SMTP, which is configured below.
;; Outlook365 e-mails are fetch and sent via Davmail.  See the
;; ~/.davmail.properties file for configuration.
;;;;; Security settings

(use-package auth-source
  :straight (:type built-in)
  :init
  (setq auth-sources '("~/.authinfo.gpg")
        user-full-name "Marco Prevedello"
        user-mail-address "m.prevedello1@nuigalway.ie"))

(use-package mml-sec
  :straight (:type built-in)
  :init
  (setq mml-secure-openpgp-encrypt-to-self t ; original was nil
        mml-secure-openpgp-sign-with-sender t ; original was nil
        mml-secure-smime-encrypt-to-self t    ; original was nil
        mml-secure-smime-sign-with-sender t)) ; original was nil

;;;;; Message composition settings

(use-package message
  :straight (:type built-in)
  :init
  (setq message-mail-user-agent t      ; use `mail-user-agent'
        mail-signature "Marco Prevedello
PhD candidate
———
m.prevedello1@nuigalway.ie
+353 852566133
Room 105, Orbsen Building.
Department of Microbiology
National University of Ireland Galway"

        message-signature "Marco Prevedello\n"
        message-citation-line-format "On %Y-%m-%d, %R %z, %f wrote:\n"
        message-citation-line-function 'message-insert-formatted-citation-line
        message-confirm-send t
        message-kill-buffer-on-exit t
        message-wide-reply-confirm-recipients t)
  :config
  (add-to-list 'mm-body-charset-encoding-alist '(utf-8 . base64))

  (add-hook 'message-setup-hook #'message-sort-headers))

;;;;; SMTP settings

(use-package sendmail
  :straight (:type built-in)
  :init
  (setq send-mail-function 'smtpmail-send-it)
  (setq smtpmail-smtp-server "localhost")
  (setq smtpmail-smtp-service 1025))

;;;;; mu4e

(add-to-list 'load-path
             "/usr/share/emacs/site-lisp/mu4e")
(require 'mu4e)

(setq mail-user-agent 'mu4e-user-agent)
(setq user-mail-address "m.prevedello1@nuigalway.ie")
(setq mu4e-mu-home "~/.mu/davmail")
(setq mu4e-change-filenames-when-moving t)
(setq mu4e-attachment-dir "~/Inbox")
(setq mu4e-get-mail-command "mbsync -a")
(setq mu4e-completing-read-function 'completing-read)
(setq mu4e-compose-signature
      "Marco Prevedello
PhD candidate
——-
m.prevedello1@nuigalway.ie
+353 852566133
Room 105, Orbsen Building.
Department of Microbiology
National University of Ireland Galway")

(global-set-key (kbd "C-x m") #'mu4e)

(require 'mu4e-icalendar)
(mu4e-icalendar-setup)

;;;; Working with version controlled files
;;;;; diff-mode
(use-package diff-mode
  :straight (:type built-in)
  :init
  (setq diff-default-read-only t)
  (add-hook 'modus-themes-after-load-theme-hook
        (lambda ()
          (if (eq modus-themes-diffs 'bg-only)
          (setq diff-font-lock-syntax 'hunk-also)
        (setq diff-font-lock-syntax nil)))))

;;;;; magit
(use-package magit
  :init
  (setq magit-pull-or-fetch t)          ; `F' does both fetch and pull
  (setq magit-define-global-key-bindings nil)
  :config
  (require 'git-commit)
  (require 'magit-diff)
  (require 'magit-repos)

  (setq git-commit-summary-max-length 50
    git-commit-known-pseudo-headers '("Signed-off-by"
                      "Acked-by"
                      "Modified-by"
                      "Cc"
                      "Suggested-by"
                      "Reported-by"
                      "Tested-by"
                      "Reviewed-by")
    git-commit-style-convention-checks '(non-empty-second-line
                         overlong-summary-line)
    magit-diff-refine-hunk t
    magit-repository-directories '(("~/Projects" . 1)
                       ("~/Shared/Git" . 1)))
  :bind
  (("C-x g" . magit-status)
   ("C-c g" . magit-file-dispatch)
   ("C-x M-g" . magit-dispatch)))

;;;;; forge
(use-package forge
  :after magit
  :config
  (transient-insert-suffix 'forge-dispatch "r"
    '(";" "toggle closed" forge-toggle-closed-visibility)))

;;;;; Add org-link support
(use-package orgit)
(use-package orgit-forge)

;;;; Working with projects
;;;;; Ripgrep in Emacs
(use-package rg
  :bind
  (("C-x %" . rg-menu)
   ("M-n" . rg-next-file)
   ("M-p" . rg-prev-file))
  :config
  (rg-enable-menu))

;;;;; Emacs' Project (built-in)
(use-package project
  :straight (:type built-in)
  :preface
  (defun project-create-new-project (project-new-root)
    "Create a new project in the directory PROJECT-ROOT."
    (interactive
     (list (read-file-name "Project root (dir): " default-directory
               default-directory nil nil)))

    (setq project-new-root (expand-file-name project-new-root))

    (unless (file-directory-p project-new-root)
      (make-directory project-new-root t))

    (project--read-project-list)

    (if (member project-new-root project--list)
    (warn "%s is already a project" project-new-root)
      (make-directory (expand-file-name "documentation" project-new-root))
      (make-directory (expand-file-name "data/raw" project-new-root) t)
      (make-directory (expand-file-name "data/processed" project-new-root) t)
      (make-directory (expand-file-name "src" project-new-root))
      (find-file (expand-file-name "README.md" project-new-root))
      (project-switch-project project-new-root)))

  :init
  (setq project-list-file (expand-file-name "projects" mp-cache-dir))
  (setq project-switch-commands
    '((?f "File" project-find-file)
      (?s "Subdir" prot-project-find-subdir)
      (?g "Grep" project-find-regexp)
      (?d "Dired" project-dired)
      (?b "Buffer" project-switch-to-buffer)
      (?q "Query replace" project-query-replace-regexp)
      (?t "Tag switch" prot-project-retrieve-tag)
      (?m "Magit" prot-project-magit-status)
      (?v "VC dir" project-vc-dir)
      (?l "Log VC" prot-project-commit-log)
      (?e "Eshell" project-eshell)
      (?r "Remove" project-forget-project)
      (?n "New project" project-create-new-project)))
  :bind
  ("C-x p q" . project-query-replace-regexp)
  ("C-x p n" . project-create-new-project)) ; C-x p is `project-prefix-map'

(use-package prot-project
  :straight (:type built-in)
  :init
  (setq prot-project-project-roots '("~/Projects/" "~/Shared/Git/"))
  (setq prot-project-commit-log-limit 25)
  (setq prot-project-large-file-lines 1000)
  :hook (after-init-hook . prot-project-add-projects)
  :bind
  (("C-x p <delete>" . prot-project-remove-project)
   ("C-x p l" . prot-project-commit-log)
   ("C-x p m" . prot-project-magit-status)
   ("C-x p s" . prot-project-find-subdir)
   ("C-x p t" . prot-project-retrieve-tag)))

;;;;;; Extend project.el with project-x
(use-package project-x
  :straight (:type git :host github :repo "karthink/project-x")
  :init
  (setq project-x-window-list-file (expand-file-name "project-x-window-list" mp-cache-dir))
  (setq project-x-local-identifier ".project")
  (project-x-mode 1))

;;;; Working with source code
;;;;; Smartparens
(use-package smartparens
  :init
  (require 'smartparens-config)
  :bind
  ()
  :hook
  (prog-mode-hook . smartparens-mode))

;;;;; Working with Lisp
(use-package slime
  :init
  (setq inferior-lisp-program "sbcl"))

(use-package lispy)

;;;;; Emacs speeks statistics (ESS)
(use-package ess
  :mode (("\\.R\\'"  . ess-mode)
     ("\\.r\\'"  . ess-mode))
  :init
  (setq ess-plain-first-buffername nil) ; Have all iESS buffers numbered
  (setq ess-directory-function 'vc-root-dir)
  (setq ess-ask-for-ess-directory nil)
  (setq ess-style 'RRR)
  (setq ess-nuke-trailing-whitespace-p 'ask)

  (defun ess-outline-minor-mode ()
    "Activate and configure `outline-minor-mode` for ESS[R]."
    (interactive)
    (if (null outline-minor-mode)
    (progn
      (setq-local outline-regexp "\\(^#\\{3,6\\}\\)\\|\\(^[a-zA-Z0-9_\.]+ ?<- ?function(.*{\\)")
      (setq-local outline-heading-alist
              '(("###" . 1)
            ("####" . 2)
            ("#####" . 3)
            ("######" . 4)
            ("^[a-zA-Z0-9_\.]+ ?<- ?function(.*{" . 5)))
      (outline-minor-mode 1)
      (outline-hide-other)
      (message "Enabled `outline-minor-mode'"))
      (outline-minor-mode -1)
      (message "Disabled `outline-minor-mode'")))
  :config
  ;;  (add-to-list 'display-buffer-alist
  ;;               '("*R Dired"
  ;;                 (display-buffer-reuse-window display-buffer-in-side-window)
  ;;                 (side . right)
  ;;                 (slot . -1)
  ;;                 (window-width . 0.33)
  ;;                 (reusable-frames . nil)))
  ;;
  ;;  (add-to-list 'display-buffer-alist
  ;;               '("*R"
  ;;                 (display-buffer-reuse-window display-buffer-at-bottom)
  ;;                 (window-width . 0.35)
  ;;                 (reusable-frames . nil)))
  (require 'ess-r-mode)

  (define-key ess-r-mode-map (kbd "<f10>") #'ess-outline-minor-mode)

  :hook
  ((ess-mode-hook . (lambda ()
              (flyspell-prog-mode)
              (run-hooks 'prog-mode-hook)))
   (ess-mode-hook . prettify-symbols-mode)
   (ess-R-post-run-hook . (lambda () (smartparens-mode 1))))
  :bind
  ("C-x M-e" . ess-request-a-process))

;;;;;; Smart equals
(use-package ess-smart-equals
  :init
  (setq ess-smart-equals-extra-ops '(brace percent))
  :after
  (:any ess-r-mode inferior-ess-r-mode ess-r-transcript-mode)
  :config
  (ess-smart-equals-activate))

;;;;;; ESS view data
(use-package ess-view-data
  :bind
  (:map ess-mode-map
    ("C-c C-M-v" . ess-view-data-print))
  (:map inferior-ess-r-mode-map
    ("C-c C-M-v" . ess-view-data-print)))

;;;;;; RMarkdown
(use-package poly-R
  ;;  :after
  ;;  (:any ess-r-mode inferior-ess-r-mode ess-r-transcript-mode)
  :init
  (add-to-list 'auto-mode-alist
           '("\\.[rR]md\\'" . poly-gfm+r-mode))

  (setq markdown-code-block-braces t)
  :bind
  (:map org-mode-map
    ("<f12>" . poly-org-mode)))


;;;;; Language server protocol

(use-package lsp-mode
  :hook ((python-mode
          js-mode
          js-jsx-mode
          typescript-mode
          ) . lsp-deferred)
  :commands lsp
  :config
  (setq lsp-auto-guess-root t)
  (setq lsp-log-io nil)
  (setq lsp-restart 'auto-restart)
  (setq lsp-enable-symbol-highlighting nil)
  (setq lsp-enable-on-type-formatting nil)
  (setq lsp-signature-auto-activate nil)
  (setq lsp-signature-render-documentation nil)
  (setq lsp-eldoc-hook nil)
  (setq lsp-modeline-code-actions-enable nil)
  (setq lsp-modeline-diagnostics-enable nil)
  (setq lsp-headerline-breadcrumb-enable nil)
  (setq lsp-semantic-tokens-enable nil)
  (setq lsp-enable-folding nil)
  (setq lsp-enable-imenu nil)
  (setq lsp-enable-snippet nil)
  (setq read-process-output-max (* 1024 1024)) ;; 1MB
  (setq lsp-idle-delay 0.5))

(use-package lsp-ui
  :commands lsp-ui-mode
  :config
  (setq lsp-ui-doc-enable nil)
  (setq lsp-ui-doc-header t)
  (setq lsp-ui-doc-include-signature t)
  (setq lsp-ui-doc-border (face-foreground 'default))
  (setq lsp-ui-sideline-show-code-actions t)
  (setq lsp-ui-sideline-delay 0.05))

;;;;;; LSP Python

(use-package lsp-pyright
  :hook (python-mode . (lambda ()
                          (require 'lsp-pyright)))
  :init (when (executable-find "python3")
          (setq lsp-pyright-python-executable-cmd "python3")))

;;;;; Working with Julia
(use-package julia-snail
  :requires vterm
  :hook (julia-mode-hook . julia-snail-mode)
  :init
  (add-to-list 'display-buffer-alist
           '("\\*julia" (display-buffer-reuse-window
                 display-buffer-same-window)))
  (setq julia-snail-multimedia-buffer-autoswitch t))

(use-package lsp-julia
  :hook (ess-julia-mode-hook . lsp-mode)
  :config (setq lsp-julia-default-environment "~/.julia/environments/v1.7"))

;; (use-package julia-mode
;;   :mode ("\\.jl\\'"  . julia-mode)
;;   :init
;;   ;; Borrow matlab.el's fontification of math operators. From
;;   ;; <https://web.archive.org/web/20170326183805/https://ogbe.net/emacsconfig.html>
;;   (dolist (mode '(julia-mode ess-julia-mode))
;;     (font-lock-add-keywords
;;      mode
;;      `((,(let ((OR "\\|"))
;;            (concat "\\("  ; stolen `matlab.el' operators first
;;                    ;; `:` defines a symbol in Julia and must not be highlighted
;;                    ;; as an operator. The only operators that start with `:` are
;;                    ;; `:<` and `::`. This must be defined before `<`.
;;                    "[:<]:" OR
;;                    "[<>]=?" OR
;;                    "\\.[/*^']" OR
;;                    "===" OR
;;                    "==" OR
;;                    "=>" OR
;;                    "\\<xor\\>" OR
;;                    "[-+*\\/^&|$]=?" OR  ; this has to come before next (updating operators)
;;                    "[-^&|*+\\/~]" OR
;;                    ;; Julia variables and names can have `!`. Thus, `!` must be
;;                    ;; highlighted as a single operator only in some
;;                    ;; circumstances. However, full support can only be
;;                    ;; implemented by a full parser. Thus, here, we will handle
;;                    ;; only the simple cases.
;;                    "[[:space:]]!=?=?" OR "^!=?=?" OR
;;                    ;; The other math operators that starts with `!`.
;;                    ;; more extra julia operators follow
;;                    "[%$]" OR
;;                    ;; bitwise operators
;;                    ">>>" OR ">>" OR "<<" OR
;;                    ">>>=" OR ">>" OR "<<" OR
;;                    "\\)"))
;;         1 font-lock-type-face)))))
;;
;; (use-package julia-repl
;;   :config (julia-repl-set-terminal-backend 'vterm)
;;   :hook (julia-mode-hook . julia-repl-mode))
;;
;; (use-package eglot-jl
;;   :after eglot
;;   :preface
;;   (setq eglot-jl-language-server-project "~/.julia/environments/v1.7")
;;   :hook
;;   (julia-mode-hook . (lambda () (setq eglot-connect-timeout (max eglot-connect-timeout 60))))
;;   :config
;;   (eglot-jl-init))

;;;;; Org for literate programming
;;;;;; Configure org babel
(with-eval-after-load 'org
  (add-to-list 'org-structure-template-alist
           '("se" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist
           '("sr" . "src R"))
  (add-to-list 'org-structure-template-alist
           '("sp" . "src python"))
  (add-to-list 'org-structure-template-alist
           '("sc" . "src calc"))

  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (R . t)
     (julia . nil)                      ; BUG! Currently I cannot find a
                    ; working package
     (python . t)
     (calc . t)))

  (setq org-babel-python-command "python3"))

;;;;;; BUG! Poly org
(use-package poly-org
  :disabled t                           ; Makes org very slow.
  :init
  (defun poly-org-toggle ()
    "Toggle `poly-org-mode'"
    (interactive)
    (if poly-org-mode
    (poly-org-mode -1)
      (poly-org-mode +1)))
  :bind
  (:map org-mode-map
    ("<f12>" . poly-org-toggle)))

;;;; GTD and PKM with the org-mode ecosystem
(use-package org
  :straight (:type built-in)
;;;;;; BUG! org-file-name-concat undefined
  :preface
  (defalias 'org-file-name-concat #'file-name-concat)
;;;;; Org export backends
  :custom
  (org-export-backends
   '(ascii beamer html icalendar latex man md odt koma-letter))
;;;;; Org modules
  :init
  (setq org-modules '(ol-bbdb ol-bibtex org-crypt ol-docview
                  ol-eww ol-gnus org-habit org-id
                  ol-info org-inlinetask ol-irc
                  ol-mhe org-protocol ol-rmail
                  org-tempo ol-w3m ol-doi))
;;;;; Basic org-mode configuration

  (setq
   org-directory "~/Org"
   org-use-speed-commands t
   org-special-ctrl-a/e t
   org-special-ctrl-k nil
   org-ctrl-k-protect-subtree t
   org-catch-invisible-edits 'show-and-error
   org-insert-heading-respect-content t
   org-return-follows-link nil
   org-link-keep-stored-after-insertion t
   org-startup-folded 'overview
   org-list-demote-modify-bullet '(("+" . "-") ("-" . "*"))
   org-startup-shrink-all-tables t
   org-startup-indented nil
   org-adapt-indentation nil
   org-pretty-entities t
   org-pretty-entities-include-sub-superscripts t
   org-hide-emphasis-markers t
   org-hide-leading-stars nil
   org-startup-numerated nil
   org-startup-with-inline-images t
   org-image-actual-width '(300)
   org-enforce-todo-dependencies t
   org-log-done 'time
   org-log-done-with-time nil
   org-log-into-drawer t
   org-highest-priority ?A
   org-lowest-priority ?C
   org-default-priority ?B
   org-clock-persist 'history
   org-use-sub-superscripts '{}
   org-footnote-auto-adjust t
   org-num-skip-footnotes t
   org-id-link-to-org-use-id 'create-if-interactive-and-no-custom-id)

;;;;; Org todo

  (setq
   org-todo-keywords '((sequence "TODO(t)" "STRT(s!)" "|" "DONE(x)")
               (sequence "WAIT(w@)" "HOLD(h!)" "|" "REVW(R)" "DELG(d@)" "CANC(c@)")
               (sequence "BUG(b@)" "REPORTED(r!)" "|" "FIXED(f!)" "MERGED(m!)")
               (sequence "PROJ(p)" "|"))
   org-todo-keyword-faces '(("STRT" . (:foreground "#Ff8c00" :weight bold))
                ("PROJ" . (:foreground "#1e90ff" :underline t)) ; #8a2be2
                ("CANC" . (:foreground "#A9a9a9" :strike-through t))
                ("HOLD" . (:foreground "#A9a9a9"))
                ("WAIT" . (:foreground "#A9a9a9" :underline t))
                ("DELG" . (:underline t))
                ("REPORTED" . (:foreground "#Ff8c00" :weight bold)))
   org-hierarchical-todo-statistics nil)

  ;; (setq org-todo-state-tags-triggers '(("CANC" ("ARCHIVE" . t))))

;;;;; Org captures

  (setq org-capture-templates
;;;;;; Tasks
        '(("i" "Inbox" entry (file "inbox.org")
           "* TODO %?\n:PROPERTIES:\n:CREATED: %U\n:END:\n\n%i"
           :prepend t
           :empty-lines 1
           :clock-resume t)
;;;;;; Meetings
          ("m" "Meeting" entry (file+headline "agenda.org" "Future")
           "* %^{Who?|Gavin|Ciaran|Simon|Claribel|Raffaello|Kris} :meeting:\nSCHEDULED:%^T"
           :prepend t
           :empty-lines 1
           :immediate-finish t)
          ("M" "Ongoing meeting" entry (file+healine "agenda.org" "Past")
           "* %^{Who?|Gavin|Ciaran|Simon|Claribel|Raffaello|Kris} %T :meeting:\n:PROPERTIES:\n:During: %K\n:END:\n\n%?"
           :immediate-finish t
           :prepend t
           :empty-lines 1)
;;;;;; Notes
          ("n" "Notes..")
          ("nm" "Meeting note" entry (file "~/Resources/Slipbox/inbox.org")
           "* Meeting with %^{Who?|Gavin|Ciaran|Simon|Claribel|Raffaello|Kris} :meeting:\n:PROPERTIES:\n:CREATED: %U\n:During: %K\n:END:\n\n%?"
           :clock-in t
           :prepend t
           :empty-lines 1)
          ("nn" "Fleeting note" entry (file "~/Resources/Slipbox/inbox.org")
           "* %^{About?}\n:PROPERTIES:\n:CREATED: %U\n:During: %K\n:END:\n\n%?"
           :clock-keep t
           :prepend t
           :empty-lines 1)
;;;;;; listing lists
          ("l" "Lists..")
          ("lw" "Web page" entry (file+headline "lists.org" "Web pages")
           "* %^{Summary}%^g\nPROPERTIES:\n:CREATED: %U\n:During: %K\n:Author: %^{Author}\n:Link: %^{Link}\n:SuggestedBy: %^{Suggested by}\n:CATEGORY: %^{Category|Personal|Work|Learning|Reference}:END:\n"
           :clock-keep t
           :prepend t
           :empty-lines 1
           :immediate-finish t)
          ("lb" "Book" entry (file+headline "lists.org" "Books")
           "* %^{Title}\nPROPERTIES:\n:CREATED: %U\n:During: %K\n:Author: %^{Author}\n:SuggestedBy: %^{Suggested by}\n:GoodreadsLink: %^{Goodreads link}\n:PubYear: %^{Publication year}\n:END:\n"
           :clock-keep t
           :prepend t
           :empty-lines 1
           :immediate-finish t)
          ("lm" "Movie or TV Show" entry (file+headline "lists.org" "Movies & TV shows")
           "* %^{Title}\nPROPERTIES:\n:CREATED: %U\n:During: %K\n:Director: %^{Director}\n:SuggestedBy: %^{Suggested by}\n:IMDbLink: %^{IMDb link}\n:PubYear: %^{Release year}\n:END:\n"
           :clock-keep t
           :prepend t
           :empty-lines 1
           :immediate-finish t)
          ("lj" "Job offer" entry (file+headline "lists.org" "Jobs")
           "* %^{Title}\nPROPERTIES:\n:CREATED: %U\n:During: %K\n:Country: %^{Country}\n:SuggestedBy: %^{Suggested by}\n:Link: %^{Link}\n:Deadline: %^{Application deadline}\n:END:\n"
           :clock-keep t
           :prepend t
           :empty-lines 1
           :immediate-finish t)
          ))

;;;;; Org agenda

  (setq
   calendar-latitude 53.270962 
   calendar-longitude -9.062691
   org-agenda-files '("~/Org/inbox.org" "~/Org/agenda.org"
              "~/Org/areas.org" "~/Org/lists.org")
   org-agenda-start-on-weekday 1
   org-agenda-window-setup 'current-window
   org-agenda-hide-tags-regexp "."
   org-agenda-skip-scheduled-if-done t
   org-agenda-skip-deadline-if-done t
   org-agenda-skip-timestamp-if-done t
   org-agenda-custom-commands '(("p" "Projects"
                 ((todo "PROJ"
                    ((org-agenda-dim-blocked-tasks nil)
                     (org-agenda-tags-column -80)
                     (org-agenda-prefix-format "%-12:b %i\t %T\t")
                     (org-agenda-overriding-header "Projects\n")))))

                ("d" "Dashboard"
                 ((agenda nil
                      ((org-agenda-skip-function
                        '(org-agenda-skip-entry-if 'deadline))
                       (org-agenda-span 5)
                       (org-agenda-use-time-grid nil)
                       (org-deadline-warning-days 0)))
                  (todo "STRT"
                    ((org-agenda-skip-function
                      '(org-agenda-skip-entry-if 'deadline))
                     (org-agenda-prefix-format " %i %-12:c [%e] ")
                     (org-agenda-overriding-header "Started tasks\n")))
                  (agenda nil
                      ((org-agenda-entry-types '(:deadline))
                       (org-agenda-span 1)
                       (org-agenda-format-date "")
                       (org-deadline-warning-days 15)
                       (org-agenda-skip-function
                        '(org-agenda-skip-entry-if
                          'regexp
                          "\\* \\(?:CANC\\|DONE\\|HOLD\\|WAIT\\)"))
                       (org-agenda-overriding-header "Deadlines")))
                  (tags-todo "inbox"
                         ((org-agenda-prefix-format " %?-12t% s")
                          (org-agenda-overriding-header "Inbox\n")))
                  (todo "WAIT|HOLD"
                    ((org-agenda-skip-function
                      '(org-agenda-skip-entry-if 'deadline))
                     (org-agenda-prefix-format " %i %-12:c [%e] ")
                     (org-agenda-overriding-header "Task waiting for...\n")))
                  (tags "CLOSED>=\"<today>\""
                    ((org-agenda-overriding-header "Completed today\n"))))))
   initial-buffer-choice (lambda () (org-agenda nil "d")))

;;;;; Org attach

  (setq
   org-attach-id-dir "data/.attachments/org"
   org-attach-method 'mv
   org-attach-store-link-p t)

;;;;; Org refile

  (setq
   org-refile-targets '((nil . (:maxlevel . 4))
            (org-buffer-list . (:maxlevel . 2))
            ("areas.org" :regexp . "\\(?:\\(?:Note\\|Task\\)s\\)"))
   org-refile-use-outline-path 'file
   org-outline-path-complete-in-steps nil
   org-refile-allow-creating-parent-nodes 'confirm
   org-refile-use-cache t
   org-archive-location (concat
             (expand-file-name "archive.org" org-directory)
             "::datetree/"))

;;;;; Org LaTeX

  (setq
   org-preview-latex-image-directory (expand-file-name "org-ltximg/" mp-cache-dir))

  (with-eval-after-load 'org
    (plist-put org-format-latex-options :scale 1.5))

;;;;; Org blocks (source, quote, etc)

  (setq
   org-fontify-quote-and-verse-blocks t
   org-confirm-babel-evaluate nil
   org-src-window-setup 'current-window
   org-edit-src-persistent-message nil
   org-src-fontify-natively t
   org-src-preserve-indentation t
   org-src-tab-acts-natively t
   org-edit-src-content-indentation 0)

;;;;; Org-table spreadsheet

  (defun org-table-highlight-row ()
    (if (org-at-table-p)
    (hl-line-mode 1)
      (hl-line-mode -1)))

  (defun setup-table-highlighting ()
    (add-hook 'post-command-hook #'org-table-highlight-row nil t))

  (add-hook 'org-mode-hook #'setup-table-highlighting)
  (add-hook 'orgtbl-mode-hook #'setup-table-highlighting)

  (setq org-table-header-line-p t
    org-table-export-default-format "orgtbl-to-tsv")

;;;;; Org habits

  (setq
   org-habit-show-habits-only-for-today t
   org-habit-preceding-days 4
   org-habit-following-days 1
   org-habit-graph-column 72
   org-habit-today-glyph 46 ;8224
   org-habit-completed-glyph 32 ;10003
   org-habit-show-done-always-green t)

;;;;; Extra functions

  (defun org-capture-inbox ()
    (interactive)
    (call-interactively 'org-store-link)
    (org-capture nil "i"))

  (defun org-emphasize-dwim (&optional char)
    "Emphasize word or region."
    (interactive)
    (unless (region-active-p)
      (backward-word)
      (mark-word))
    (org-emphasize char))

  (defun org-summary-todo (n-done n-not-done)
    "Switch entry to DONE when all subentries are done, to TODO otherwise."
    (let (org-log-done org-log-states)   ; turn off logging
      (org-todo (if (= n-not-done 0) "DONE" "PROJ"))))

  :config
;;;;; Org config after load
  (org-load-modules-maybe)

  (org-clock-persistence-insinuate)

  (add-hook 'org-after-todo-statistics-hook 'org-summary-todo)
;;;;; Org bindings
  :bind (("C-c l" . org-store-link)
     ("C-c a" . org-agenda)
     ("C-c c" . org-capture)
     ("C-c i" . org-capture-inbox)
     ("C-c C-x C-o" . org-clock-out)
     :map org-mode-map
     ([remap org-emphasize] . org-emphasize-dwim)
     ("C-M-i" . completion-at-point)))

;;;; Org transclusion
(use-package org-transclusion
  :defer
  :after org
  :straight (:host github
           :repo "nobiot/org-transclusion"
           :branch "main"
           :files ("*.el"))
  :config
  (set-face-attribute
   'org-transclusion-fringe nil
   :foreground "orange"
   :background "orange")
  :bind
  ("C-c t" . org-transclusion-mode))

;;;; Citation features (org 9.5+)
;;;;; Bibliography files
(defvar mp-bibliography-files '("~/Resources/Bibliographies/main.bib"
                                "~/Resources/Bibliographies/microtransitions.bib"
                                "~/Resources/Bibliographies/gcl-nuig.bib")
  "A list of bib files to use as bibliographies.")

;;;;; RefTeX for general settings (not only Org)
(use-package reftex
  :straight (:type built-in)
  :init
  (setq reftex-default-bibliography mp-bibliography-files)
  (setq bibtex-completion-bibliography mp-bibliography-files
        bibtex-completion-library-path "~/Resources/Bibliographies/Files/"
        bibtex-completion-pdf-field "file"
        bibtex-completion-notes-path "~/Resources/Bibliographies/Notes"
        bibtex-completion-notes-extension ".org"
        ;; make sure to set this to ensure open commands work correctly
        bibtex-completion-additional-search-fields '(doi url keywords)
        ;; Set insert citekey with markdown citekeys for org-mode
        bibtex-completion-format-citation-functions
        '((org-mode      . bibtex-completion-format-citation-org-cite)
          (latex-mode    . bibtex-completion-format-citation-cite)
          (markdown-mode . bibtex-completion-format-citation-pandoc-citeproc)
          (default       . bibtex-completion-format-citation-pandoc-citeproc))
        bibtex-dialect 'biblatex))

;;;;; Org-Cite
(use-package oc
  :straight (:type built-in)
  :after org
  :config
  (setq org-cite-global-bibliography mp-bibliography-files))

;;;;;; Org-cite processors
(use-package oc-basic
  :straight (:type built-in)
  :after oc)

(use-package oc-biblatex
  :straight (:type built-in)
  :after oc)

(use-package oc-natbib
  :straight (:type built-in)
  :after oc)

(use-package oc-csl
  :straight (:type built-in)
  :after oc
  :init
  (setq org-cite-csl-styles-dir "~/Zotero/styles")
  (setq org-cite-export-processors '((beamer natbib)
                                     (latex biblatex)
                                     (t csl "chicago-author-date.csl"))))

;;;;; Citar (previously bibtex-actions) for completion
(use-package citar
  :custom
  (citar-library-paths '("~/Resources/Bibliographies/Files"))
  (citar-bibliography org-cite-global-bibliography)
  (org-cite-global-bibliography mp-bibliography-files)
  (org-cite-insert-processor 'citar)
  (org-cite-follow-processor 'citar)
  (org-cite-activate-processor 'citar)
  :init
  (setq citar-symbols
      `((file ,(all-the-icons-faicon "file-o" :face 'all-the-icons-green :v-adjust -0.1) . " ")
        (note ,(all-the-icons-material "speaker_notes" :face 'all-the-icons-blue :v-adjust -0.3) . " ")
        (link ,(all-the-icons-octicon "link" :face 'all-the-icons-orange :v-adjust 0.01) . " ")))
  (setq citar-symbol-separator "  ")
  :config
  (citar-filenotify-setup '(LaTeX-mode-hook org-mode-hook))
  :bind
  (("C-c b" . citar-insert-citation)
   :map minibuffer-local-map
   ("M-b" . citar-insert-preset)
   :map org-mode-map :package org
   ("C-c b" . org-cite-insert)))

;;;; Automatic hide/show Org overlays
;;;;; Org-fragtog for LaTeX fragments
(use-package org-fragtog
  :hook
  (org-mode-hook . org-fragtog-mode))

;;;;; Org-appear for emphasis markers
(use-package org-appear
  :straight (:type git :host github :repo "awth13/org-appear")
  :hook
  (org-mode-hook . org-appear-mode))

;;;; Zettlekasten with Org-roam
;;;;; Org-roam
(use-package org-roam
;;;;;; General settings
  :init
  (setq org-roam-v2-ack t)
  (setq org-roam-completion-everywhere t)
  (setq org-roam-db-gc-threshold most-positive-fixnum)
  (setq org-roam-directory (file-truename "~/Resources/Slipbox"))

  (unless (file-exists-p org-roam-directory)
    (make-directory org-roam-directory))

  (add-to-list 'display-buffer-alist
           '("\\*org-roam\\*"
         (display-buffer-in-direction)
         (direction . right)
         (window-width . 0.33)
         (window-height . fit-window-to-buffer)))
  :config
  (org-roam-setup)
;;;;;; Additional methods for `org-roam-display-template'
  (cl-defmethod org-roam-node-type ((node org-roam-node))
    "Return the TYPE of NODE."
    (condition-case nil
        (file-name-nondirectory
         (directory-file-name
          (file-name-directory
           (file-relative-name (org-roam-node-file node)
                               org-roam-directory))))
      (error "")))

  (cl-defmethod org-roam-node-backlinkscount ((node org-roam-node))
    (let* ((count (caar (org-roam-db-query
                         [:select (funcall count source)
                                  :from links
                                  :where (= dest $s1)
                                  :and (= type "id")]
                         (org-roam-node-id node)))))
      (format "[%d]" count)))

  (setq org-roam-node-display-template
        (concat "${type:15} ${title:*} "
                (propertize "${tags:10}" 'face 'org-tag)
                " ${backlinkscount:6}"))

;;;;;; Search notes with consult-ripgrep
  (defun mp/org-roam-rg-search ()
    "Search org-roam directory using consult-ripgrep. With live-preview."
    (interactive)
    ;; (let ((consult-ripgrep-args "rg --null --multiline --ignore-case --type org --line-buffered --color=always --max-columns=500 --no-heading --line-number . -e ARG OPTS"))
    (consult-ripgrep org-roam-directory))

;;;;;; Set org-roam-capture-templates
  (setq org-roam-capture-templates
        '(("o" "original" plain
           "%?"
           :if-new (file+head "original/${slug}.org"
                              ":PROPERTIES:
:CREATED: %u
:END:
#+title: ${title}
#+filetags: :original:
")
           :immediate-finish nil
           :unnarrowed t)

          ("r" "reference" plain "%?"
           :if-new
           (file+head "reference/${slug}.org"
                              ":PROPERTIES:
:CREATED: %u
:END:
#+title: ${title}
#+filetags: :reference:
")
           :immediate-finish t
           :unnarrowed t)

          ("p" "placeholder" plain "%?"
           :if-new
           (file+head "placeholders/${slug}.org"
                      ":PROPERTIES:
:CREATED: %u
:END:
#+title: ${title}
#+filetags: :placeholder:
")
           :immediate-finish t
           :unnarrowed t)))

;;;;;; Simple bibliographic reference without ORB
  (defun mp/org-roam-node-from-cite (keys-entries)
    "Create org-roam node from citar key entry."
    (interactive (list (citar-select-ref :multiple nil
                                         :rebuild-cache t)))
    (let ((title (citar--format-entry-no-widths
                  (cdr keys-entries)
                  "${author editor} :: ${title}")))
      (org-roam-capture- :templates
                         '(("r" "reference" plain "%?"
                            :if-new
                            (file+head "reference/${citekey}.org"
                                       ":PROPERTIES:
:ROAM_REFS: [cite:@${citekey}]
:CREATED: %u
:END:
#+title: ${title}
#+filetags: :reference:
")
                            :immediate-finish t
                            :unnarrowed t))
                         :info (list :citekey (car keys-entries))
                         :node (org-roam-node-create :title title)
                         :props '(:finalize find-file))))

;;;;;; Set new notes as drafts
  (defun mp/org-roam-tag-add-draft ()
    "Add tag \"draft\" to a org roam node."
    (org-roam-tag-add '("draft")))

;;;;;; hooks
  :hook
  ((after-init-hook . org-roam-db-autosync-mode)
   (org-roam-capture-new-node-hook . mp/org-roam-tag-add-draft))
;;;;;; keybindings
  :bind
  (("C-c nl" . org-roam-buffer-toggle)
   ("C-c nf" . org-roam-node-find)
   ("C-c ni" . org-roam-node-insert)
   ("C-c ng" . mp/org-roam-rg-search)
   ("C-c nr" . mp/org-roam-node-from-cite)
   :map org-mode-map
   ("C-c n a" . org-roam-alias-add)
   ("C-c n t" . org-roam-tag-add)))

;;;;; Org-roam-dailies for journaling

(use-package org-roam-dailies
  :after org-roam
  :straight (:type built-in)
  :init
  (setq org-roam-dailies-directory "dailies/")
  (setq org-roam-dailies-capture-templates
    '(("d" "default" entry
       "* %?"
       :target (file+head "%<%Y%m%d-%a>.org"
                  "#+title: %<%a %d/%m/%y>\n#+filetags: :journal:\n"))))
  :bind
  (("C-c nd" . org-roam-dailies-goto-today)
   ("C-c nn" . org-roam-dailies-goto-next-note)
   ("C-c np" . org-roam-dailies-goto-previous-note)
   ("C-c nD" . org-roam-dailies-goto-date)))

;;;;; Org roam UI for visualization of note-links

(use-package websocket
  :after org-roam-ui)

(use-package simple-httpd
  :after org-roam-ui)

(use-package org-roam-ui
  :straight
  (:host github :repo "org-roam/org-roam-ui" :branch "main" :files ("*.el" "out"))
  :after org-roam
  :init
  (setq org-roam-ui-browser-function #'browse-url-firefox
        browse-url-new-window-flag t
        org-roam-ui-sync-theme t
        org-roam-ui-follow t
        org-roam-ui-update-on-save t
        org-roam-ui-open-on-start t)
  :bind
  ("C-c nu" . org-roam-ui-open))

;;;; Org Download to paste screenshots in org-mode files
(use-package org-download
  :init
  (setq org-download-method 'attach) ; See the org-attach section
  (setq org-download-image-org-width 500)
  :hook
  ((dired-mode-hook . org-download-enable)
   (org-mode-hook . org-download-enable))
  :bind
  (:map org-mode-map
    ("C-c C-<print> C-<print>" . org-download-screenshot)
    ("C-c C-<print> C-y" . org-download-clipboard)))

;;;; Org Transclusion to visualize (section of) org files into org files
(use-package org-transclusion
  :disabled t
  :after org
  :straight (:host github
           :repo "nobiot/org-transclusion"
           :branch "main"
           :files ("*.el"))
  :init
  (setq org-transclusion-include-first-section nil)
  :bind
  (:map org-mode-map
    ("C-c t" . org-transclusion-mode)))
